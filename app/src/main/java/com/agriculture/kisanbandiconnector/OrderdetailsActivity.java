package com.agriculture.kisanbandiconnector;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class OrderdetailsActivity extends AppCompatActivity {

    TextView ordercode,name,paymenttype,gateway,totitems,totamount,sno,qty,itemname,itemprice,
    subtotal,deliveryamount,totalamount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderdetails);
        ordercode=findViewById(R.id.ordercode);
        name=findViewById(R.id.name);
        paymenttype=findViewById(R.id.paymenttype);
        gateway=findViewById(R.id.gateway);
        totitems=findViewById(R.id.totitems);
        totalamount=findViewById(R.id.totalamount);
        totamount=findViewById(R.id.totamount);
        sno=findViewById(R.id.sno);
        qty=findViewById(R.id.qty);
        itemname=findViewById(R.id.itemname);
        itemprice=findViewById(R.id.itemprice);
        subtotal=findViewById(R.id.subtotal);
        deliveryamount=findViewById(R.id.deliveryamount);
    }
}
