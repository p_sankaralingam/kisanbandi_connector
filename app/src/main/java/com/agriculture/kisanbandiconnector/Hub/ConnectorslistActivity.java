package com.agriculture.kisanbandiconnector.Hub;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.Adapter.ConnectorslistAdapter;
import com.agriculture.kisanbandiconnector.Models.Resbonse.ConnectorlistResponse;
import com.agriculture.kisanbandiconnector.R;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConnectorslistActivity extends AppCompatActivity {



    RecyclerView connectorslist;
    String connectorid="";


    LinearLayout listlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connectorslist);

        connectorslist=findViewById(R.id.connectorslist);
        connectorslist.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        connectorid= Sharedpreference.getStringValue(getApplicationContext(),"Connectorid");
        listlayout=findViewById(R.id.listlayout);

        getconnecotorslist(connectorid);
    }

    public void getconnecotorslist(final String connectorid) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<ConnectorlistResponse> call = service.getconnectorslist(connectorid);
        call.enqueue(new Callback<ConnectorlistResponse>() {
            @Override
            public void onResponse(Call<ConnectorlistResponse> call, Response<ConnectorlistResponse> response) {
                if (response.body().getStatus().equals("success")) {
                    if (response.body().getConnectorList().size() != 0) {
                        ConnectorslistAdapter adapter = new ConnectorslistAdapter(getApplicationContext(), response.body().getConnectorList());
                        connectorslist.setAdapter(adapter);
                    }
                }
                else {
                    connectorslist.setVisibility(View.GONE);
                    listlayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ConnectorlistResponse> call, Throwable t) {
            }
        });
    }
}
