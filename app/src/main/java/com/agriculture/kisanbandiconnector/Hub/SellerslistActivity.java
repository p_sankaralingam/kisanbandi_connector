package com.agriculture.kisanbandiconnector.Hub;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.Adapter.SellerslistAdapter;
import com.agriculture.kisanbandiconnector.Models.Resbonse.SellerlistResponse;
import com.agriculture.kisanbandiconnector.R;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SellerslistActivity extends AppCompatActivity {
    RecyclerView  sellerslist;
    String connectorid="";
    LinearLayout listlayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sellerslist);
        sellerslist=findViewById(R.id.sellerslist);
        listlayout=findViewById(R.id.listlayout);
        sellerslist.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        connectorid= Sharedpreference.getStringValue(getApplicationContext(),"Connectorid");
        getsellerslist(connectorid);
    }
    public void getsellerslist(final String connectorid) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<SellerlistResponse> call = service.getsellerlist(connectorid);
        call.enqueue(new Callback<SellerlistResponse>() {
            @Override
            public void onResponse(Call<SellerlistResponse> call, Response<SellerlistResponse> response) {
                if (response.body().getStatus().equals("success")) {
                    Toast.makeText(getApplicationContext(),String.valueOf(response.body().getSellerList().size()),Toast.LENGTH_LONG).show();
                    if (response.body().getSellerList().size() != 0) {
                        listlayout.setVisibility(View.GONE);
                        SellerslistAdapter adapter = new SellerslistAdapter(getApplicationContext(), response.body().getSellerList());
                        sellerslist.setAdapter(adapter);
                    }
                }
                else
                {
                    sellerslist.setVisibility(View.GONE);
                    listlayout.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onFailure(Call<SellerlistResponse> call, Throwable t) {

            }
        });
    }
}
