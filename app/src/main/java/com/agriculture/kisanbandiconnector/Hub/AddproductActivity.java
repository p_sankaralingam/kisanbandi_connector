package com.agriculture.kisanbandiconnector.Hub;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.agriculture.kisanbandiconnector.Models.Resbonse.CategoryResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.ImageuploadResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.MeasurementResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.ProductlistResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.SelleridResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.StatusResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.SubcategoryResponse;
import com.agriculture.kisanbandiconnector.R;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.Constants;
import com.agriculture.kisanbandiconnector.Utils.ProgressRequestBody;
import com.agriculture.kisanbandiconnector.Utils.SearchableSpinner;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;
import com.agriculture.kisanbandiconnector.Utils.UploadCallBacks;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import pub.devrel.easypermissions.AfterPermissionGranted;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.agriculture.kisanbandiconnector.Utils.Constants.READ_REQUEST_CODE;

public class AddproductActivity extends AppCompatActivity implements UploadCallBacks {
    ArrayList<String> CategoryList = new ArrayList<>();
    ArrayList<String> CategoryIdlist = new ArrayList<>();
    ArrayList<String> SellerList = new ArrayList<>();
    ArrayList<String> SellerIdlist = new ArrayList<>();
    ArrayList<String> CommissionList = new ArrayList<>();
    ArrayList<String> SubcategoryIdlist = new ArrayList<>();
    ArrayList<String> Subcategorylist = new ArrayList<>();
    ArrayList<String> MeasurementIdlist = new ArrayList<>();
    ArrayList<String> Measurelist = new ArrayList<>();
    SearchableSpinner category, sub_category, measurement;
    String str_category_id = "", str_subcategory_id = "", str_measurement_id = "";
    int str_seller_id;
    String User_id = "";
    EditText productname, qty, price, desc;
    ImageView product_image;
    ArrayList<String> Gradelist = new ArrayList<>();
    SearchableSpinner grade, seller;
    String Image_url = "";
    String str_grade_name = "";
    String str_commission = "";
    private static final int REQUEST_VIDEO_CAPTURE = 300;
    private static final int MY_PERMISSIONS_REQUEST_CODE = 123;
    boolean isImageAdded = false;
    String _video, mCurrentPhotoPath;
    String filepath1, pathToStoredVideo, outputFileAbsolutePath;
    private Uri fileUri, uri;
    ProgressDialog progressDialog;
    String Connector_id = "";
    boolean edit = false;
    String str_product_id = "";
    ProgressDialog dialog,dialog1,dialog2;
    ProductlistResponse.ProductListDetail data;
    String str_image_url="";
    TextView title;
    Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addproduct);
        Gson gson = new Gson();
        if(getIntent().getExtras()!=null) {
            data = gson.fromJson(getIntent().getStringExtra("data"), ProductlistResponse.ProductListDetail.class);
            edit = getIntent().getBooleanExtra("edit", false);
            str_product_id = data.getProdId();
            str_image_url=data.getProdImageurl();
        }
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // do you work now
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                            showSettingsDialog();
                        }
                    }
                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();

        title=findViewById(R.id.title);
        category = findViewById(R.id.category);
        category.setTitle("Select Category");
        sub_category = findViewById(R.id.subcategory);
        measurement = findViewById(R.id.measurement);
        grade = findViewById(R.id.grade);
        seller = findViewById(R.id.seller);
        sub_category.setTitle("Select SubCategory");
        measurement.setTitle("Select Measurement");
        grade.setTitle("Select Grade");
        seller.setTitle("Select Seller");
        productname = findViewById(R.id.product_name);
        qty = findViewById(R.id.qty);
        price = findViewById(R.id.price);
        desc = findViewById(R.id.desc);
        product_image = findViewById(R.id.image);
        submit=findViewById(R.id.submit);
        product_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
        User_id = Sharedpreference.getStringValue(AddproductActivity.this, "User_id");
        Connector_id = Sharedpreference.getStringValue(getApplicationContext(), "Connectorid");
        getSellerlist(Connector_id);
        getcategory();
        Measurement(User_id);
        Gradelist.add("A");
        Gradelist.add("B");
        Gradelist.add("C");
        if (edit) {
            productname.setText(data.getProdName());
            qty.setText(data.getProdQuantity());
            price.setText(data.getProdPrice());
            desc.setText(data.getProdDetail());
            title.setText("Edit Product");
            submit.setText("SUBMIT");
            Glide.with(this).load(data.getProdImageurl()).override(150, 150).into(product_image);
          //  category.setSelectionM(2);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, Gradelist);
        grade.setAdapter(adapter);
        if(edit)
        {
            String str_grade=data.getGrade();
            if (str_grade.length() != 0) {
                int modelname = checking(str_grade, Gradelist);
                if (modelname != -1)
                    grade.setSelectionM(modelname);
                str_grade_name=str_grade;
            }
        }
        grade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                str_grade_name = adapterView.getSelectedItem().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(edit)
                {
                    if (category.getSelectedItemPosition() == -1) {
                        TextView spinner1 = (TextView) category.getSelectedView();
                        spinner1.setError("Select Category");
                    } else if (sub_category.getSelectedItemPosition() == -1) {
                        TextView spinner2 = (TextView) sub_category.getSelectedView();
                        spinner2.setError("Select Sub Category");
                    } else if (productname.getText().length() == 0) {
                        productname.setError("Enter Product name");
                    } else if (qty.getText().length() == 0) {
                        qty.setError("Enter Quantity");
                    } else if (measurement.getSelectedItemPosition() == -1) {
                        TextView spinner3 = (TextView) measurement.getSelectedView();
                        spinner3.setError("Select Sub Category");
                    } else if (price.getText().length() == 0) {
                        price.setError("Enter Price");
                    } else if (grade.getSelectedItemPosition() == -1) {
                        TextView spinner = (TextView) grade.getSelectedView();
                        spinner.setError("Select Grade");
                    } else if (desc.getText().length() == 0) {
                        desc.setError("Enter Description");

                    } else {
                        String str_product_name = productname.getText().toString();
                        String str_quantity = qty.getText().toString();
                        String str_price = price.getText().toString();
                        String str_desc = desc.getText().toString();
                        Editproduct(Connector_id, str_category_id, str_subcategory_id, str_product_name, str_quantity, str_measurement_id, str_price, str_commission, str_grade_name, str_desc, filepath1);
                    }
                }
                else
                {
                    if (category.getSelectedItemPosition() == -1) {
                        TextView spinner1 = (TextView) category.getSelectedView();
                        spinner1.setError("Select Category");
                    } else if (sub_category.getSelectedItemPosition() == -1) {
                        TextView spinner2 = (TextView) sub_category.getSelectedView();
                        spinner2.setError("Select Sub Category");
                    } else if (productname.getText().length() == 0) {
                        productname.setError("Enter Product name");
                    } else if (qty.getText().length() == 0) {
                        qty.setError("Enter Quantity");
                    } else if (measurement.getSelectedItemPosition() == -1) {
                        TextView spinner3 = (TextView) measurement.getSelectedView();
                        spinner3.setError("Select Sub Category");
                    } else if (price.getText().length() == 0) {
                        price.setError("Enter Price");
                    } else if (grade.getSelectedItemPosition() == -1) {
                        TextView spinner = (TextView) grade.getSelectedView();
                        spinner.setError("Select Grade");
                    } else if (desc.getText().length() == 0) {
                        desc.setError("Enter Description");
                    } else if (filepath1.length() == 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(AddproductActivity.this);
                        builder.setTitle("You must Upload Product Image");
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.setCancelable(true);
                        alert.show();
                    } else {
                        String str_product_name = productname.getText().toString();
                        String str_quantity = qty.getText().toString();
                        String str_price = price.getText().toString();
                        String str_desc = desc.getText().toString();
                        Inserthub(Connector_id, str_category_id, str_subcategory_id, str_product_name, str_quantity, str_measurement_id, str_price, str_commission, str_grade_name, str_desc, filepath1);
                    }
                }

            }
        });
    }
    public void getSellerlist(String connector_id) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<SelleridResponse> call = service.getSellerid(connector_id);
        call.enqueue(new Callback<SelleridResponse>() {
            @Override
            public void onResponse(Call<SelleridResponse> call, Response<SelleridResponse> response) {
                if (response.body().getStatus().equals("success")) {
                    for (int i = 0; i < response.body().getProductListDetails().size(); i++) {
                        SellerList.add(response.body().getProductListDetails().get(i).getFirstName());
                        SellerIdlist.add(response.body().getProductListDetails().get(i).getSellersId());
                    }
                    final ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, SellerList);
                    seller.setAdapter(adapter);
                    if(edit)
                    {
                        String mainc_id=data.getSeller_id();
                        if (mainc_id.length() != 0) {
                            int modelname = checking(mainc_id, SellerIdlist);
                            if (modelname != -1)
                                seller.setSelectionM(modelname);
                            str_seller_id= Integer.parseInt(mainc_id);
                        }
                    }
                    seller.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            str_seller_id = Integer.parseInt(SellerIdlist.get(adapterView.getSelectedItemPosition()));
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });
                }
            }
            @Override
            public void onFailure(Call<SelleridResponse> call, Throwable t) {
            }
        });
    }

    private void getcategory() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<CategoryResponse> call = service.getcategoruydetails();
        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                if (response.body().getStatus().equals("success")) {
                    for (int i = 0; i < response.body().getCategoryDetails().size(); i++) {
                        CategoryList.add(response.body().getCategoryDetails().get(i).getMaincName());
                        CategoryIdlist.add(response.body().getCategoryDetails().get(i).getMaincId());
                        CommissionList.add(response.body().getCategoryDetails().get(i).getCommisionAmount());
                    }
                    ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, CategoryList);
                    category.setAdapter(adapter1);
                    if(edit)
                    {
                        String mainc_id=data.getMaincId();
                        if (mainc_id.length() != 0) {
                            int modelname = checking(mainc_id, CategoryIdlist);
                            if (modelname != -1)
                                category.setSelectionM(modelname);
                            str_category_id=mainc_id;
                        }
                    }
                    category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            str_category_id = CategoryIdlist.get(adapterView.getSelectedItemPosition());
                            str_commission = CommissionList.get(adapterView.getSelectedItemPosition());
                            subcategory(str_category_id);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                }
            }
            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
            }
        });
    }
    public int checking(String naame, ArrayList<String> brandNamelist) {
        int j = -1;
        for (int i = 0; i < brandNamelist.size(); i++) {
            String name = brandNamelist.get(i);
            if (name.equals(naame)) {
                j = i;
            }
        }
        return j;
    }
    private void subcategory(String str_category_id) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<SubcategoryResponse> call = service.getsubcategory(str_category_id);
        call.enqueue(new Callback<SubcategoryResponse>() {
            @Override
            public void onResponse(Call<SubcategoryResponse> call, Response<SubcategoryResponse> response) {
                if (response.body().getStatus().equals("success")) {
                    for (int i = 0; i < response.body().getSubCategoryDetails().size(); i++) {
                        Subcategorylist.add(response.body().getSubCategoryDetails().get(i).getSubcName());
                        SubcategoryIdlist.add(response.body().getSubCategoryDetails().get(i).getMaincId());
                    }
                    final ArrayAdapter<String> adapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_spinner_item, Subcategorylist);
                    sub_category.setAdapter(adapter);
                    if(edit)
                    {
                        String mainc_id=data.getSubcId();
                        if (mainc_id.length() != 0) {
                            int modelname = checking(mainc_id, SubcategoryIdlist);
                            if (modelname != -1)
                                sub_category.setSelectionM(modelname);
                            str_subcategory_id=mainc_id;
                        }
                    }
                    sub_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            str_subcategory_id = SubcategoryIdlist.get(adapterView.getSelectedItemPosition());
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                }
            }
            @Override
            public void onFailure(Call<SubcategoryResponse> call, Throwable t) {
            }
        });
    }

    private void Measurement(String str_connector_id) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<MeasurementResponse> call = service.getmeasurement(str_connector_id);
        call.enqueue(new Callback<MeasurementResponse>() {
            @Override
            public void onResponse(Call<MeasurementResponse> call, Response<MeasurementResponse> response) {
                if (response.body().getStatus().equals("success")) {
                    for (int i = 0; i < response.body().getMeasurementDetails().size(); i++) {
                        Measurelist.add(response.body().getMeasurementDetails().get(i).getMeasurementName());
                        MeasurementIdlist.add(response.body().getMeasurementDetails().get(i).getMeasurementId());
                    }
                    final ArrayAdapter<String> adapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_spinner_item, Measurelist);
                    measurement.setAdapter(adapter);
                    if(edit)
                    {
                        String mainc_id=data.getMeasurementId();
                        if (mainc_id.length() != 0) {
                            int modelname = checking(mainc_id, MeasurementIdlist);
                            if (modelname != -1)
                                measurement.setSelectionM(modelname);
                            str_measurement_id=mainc_id;
                        }
                    }
                    measurement.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            str_measurement_id = MeasurementIdlist.get(adapterView.getSelectedItemPosition());
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<MeasurementResponse> call, Throwable t) {
            }
        });
    }

    private void Inserthub(String Connector_id, String str_category_id, String str_subcategory_id, String str_product_name, String str_quantity, String str_measurement_id, String str_price, String str_commission, String str_grade_name, String str_desc, String str_imageurl) {
        progressDialog = new ProgressDialog(AddproductActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading please wait...");
        progressDialog.setMax(100);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.show();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        RequestBody str_userid = RequestBody.create(MultipartBody.FORM, Connector_id);
        RequestBody category_id = RequestBody.create(MultipartBody.FORM, str_category_id);
        RequestBody subcategory_id = RequestBody.create(MultipartBody.FORM, str_subcategory_id);
        RequestBody product_name = RequestBody.create(MultipartBody.FORM, str_product_name);
        RequestBody quantity = RequestBody.create(MultipartBody.FORM, str_quantity);
        RequestBody measurement_id = RequestBody.create(MultipartBody.FORM, str_measurement_id);
        RequestBody price = RequestBody.create(MultipartBody.FORM, str_price);
        RequestBody commission = RequestBody.create(MultipartBody.FORM, str_commission);
        RequestBody grade_name = RequestBody.create(MultipartBody.FORM, str_grade_name);
        RequestBody desc = RequestBody.create(MultipartBody.FORM, str_desc);
        File file1 = new File(filepath1);
        ProgressRequestBody reqFile1 = new ProgressRequestBody(file1, this);
        MultipartBody.Part image = MultipartBody.Part.createFormData("prod_imageurl", file1.getName(), reqFile1);
        Call<StatusResponse> call = service.insertproduct(str_userid, category_id, subcategory_id, product_name, measurement_id, quantity, price, commission, grade_name, desc, str_seller_id, image);
        call.enqueue(new Callback<StatusResponse>() {
            @Override
            public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                progressDialog.dismiss();
                if (response.body().getStatus().equals("success")) {
                    AddproductActivity.this.onBackPressed();
                }
            }
            @Override
            public void onFailure(Call<StatusResponse> call, Throwable t) {
            }
        });
    }
    @AfterPermissionGranted(READ_REQUEST_CODE)
    public void selectImage() {
        String title = "Open Photo";
        CharSequence[] itemlist = {"Take a Photo", "Pick from Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(AddproductActivity.this);
        builder.setTitle(title);
        builder.setItems(itemlist, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:// Take Photo
                        // Do Take Photo task here
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, Constants.CAMERA_REQUEST);
                        /* if (EasyPermissions.hasPermissions(RegisterActivity.this.getApplicationContext(), Manifest.permission.CAMERA)) {
                         *//*  Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, Constants.CAMERA_REQUEST);*//*
                        } else {
                            EasyPermissions.requestPermissions(RegisterActivity.this, getString(R.string.permission_read_file), Constants.CAMERA_REQUEST, Manifest.permission.CAMERA);
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, Constants.CAMERA_REQUEST);
                        }*/
                        break;
                    case 1:// Choose Existing Photo
                        // Do Pick Photo task here
                        Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                        openGalleryIntent.setType("image/*");
                        startActivityForResult(openGalleryIntent, Constants.REQUEST_GALLERY_CODE);
                        /*if (EasyPermissions.hasPermissions(RegisterActivity.this.getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                            openGalleryIntent.setType("image/*");
                            startActivityForResult(openGalleryIntent, Constants.REQUEST_GALLERY_CODE);
                        } else {
                            EasyPermissions.requestPermissions(RegisterActivity.this, getString(R.string.permission_read_file), Constants.READ_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
                            Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                            openGalleryIntent.setType("image/*");
                            startActivityForResult(openGalleryIntent, Constants.REQUEST_GALLERY_CODE);
                        }*/
                        break;
                    default:
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.setCancelable(true);
        alert.show();
    }
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        /* super.onRequestPermissionsResult(requestCode, permissions, grantResults);*/
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CODE: {
                // When request is cancelled, the results array are empty
                if ((grantResults.length > 0) &&
                                (grantResults[0]
                                        + grantResults[1]
                                        == PackageManager.PERMISSION_GRANTED
                                )
                ) {
                    // Permissions are granted
                    /*Toast.makeText(RegisterActivity.this,"Permissions granted.",Toast.LENGTH_SHORT).show();*/
                } else {
                    // Permissions are denied
                    Toast.makeText(AddproductActivity.this, "Permissions denied.", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (Constants.REQUEST_GALLERY_CODE == requestCode && resultCode == Activity.RESULT_OK && data != null) {
            uri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = AddproductActivity.this.getContentResolver().query(uri, filePathColumn, null, null, null);
            if (cursor == null)
                return;
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            filepath1 = cursor.getString(columnIndex);
            isImageAdded = true;
            if(edit)
            {
                Addimage();
            }
//            progressBar.setVisibility(View.VISIBLE);
            /*  uploadVideo(fileUri,video_Url);*/
            /*invokeUploadImageService(uri,filepath1,video_Url);*/
            Glide.with(this).load(uri).into(product_image);
        }
        if (requestCode == Constants.CAMERA_REQUEST) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                File photoFile = null;
                try {
                    photoFile = createImageFile(bitmap);
                } catch (IOException ex) {
                    // Error occurred while creating the File
                    ex.printStackTrace();
                }
                // Continue only if the File was successfully created
                if (photoFile != null)
                    uri = Uri.fromFile(photoFile);
              /*  invokeUploadImageService(uri,filepath1,video_Url);
         invokeUploadImageService(uri,filepath1,video_Url);*/
                Glide.with(this).load(uri).into(product_image);
                filepath1 = String.valueOf(photoFile);
                isImageAdded = true;

                if(edit)
                {
                    Addimage();
                }
            }
        }
    }

    private File createImageFile(Bitmap bitmap) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(imageFileName,  /* prefix */".jpg",/* suffix */AddproductActivity.this.getCacheDir() /* directory */);

        /* FileOutputStream fileOutputStream = new FileOutputStream(image);*/
        OutputStream outputStream = new FileOutputStream(image);
        /*bitmap = ((BitmapDrawable) kyc.getDrawable()).getBitmap();//newlly added*/
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        outputStream.flush();
        outputStream.close();

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.e("Getpath", "Cool" + mCurrentPhotoPath);
        return image;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onProgressUpdate(int i) {
        progressDialog.setProgress(i);
    }


    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddproductActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 100);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.delete, menu);
        MenuItem item1 = menu.findItem(R.id.menu_delete);
        if (edit) {
            item1.setVisible(true);
        } else {
            item1.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_delete) {
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(AddproductActivity.this);
            alertDialogBuilder.setMessage("Are you sure want to delete the product?");
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setPositiveButton("yes",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            Deleteitem();
                        }
                    });
            alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            androidx.appcompat.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

    private void Deleteitem() {
        dialog = new ProgressDialog(AddproductActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage("Loading......");
        dialog.show();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<StatusResponse> call = service.deletehubproduct(Connector_id, str_product_id);
        call.enqueue(new Callback<StatusResponse>() {
            @Override
            public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                dialog.dismiss();
                if (response.body().getStatus().equals("success")) {
                    AddproductActivity.this.onBackPressed();
                }
            }
            @Override
            public void onFailure(Call<StatusResponse> call, Throwable t) {
            }
        });
    }
    private void Addimage() {
        dialog1 = new ProgressDialog(AddproductActivity.this);
        dialog1.setCancelable(false);
        dialog1.setMessage("Loading......");
        dialog1.show();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        File file1 = new File(filepath1);
        ProgressRequestBody reqFile1 = new ProgressRequestBody(file1, this);
        MultipartBody.Part image = MultipartBody.Part.createFormData("prod_imageurl", file1.getName(), reqFile1);
        Call<ImageuploadResponse> call = service.imageupload(image);
        call.enqueue(new Callback<ImageuploadResponse>() {
            @Override
            public void onResponse(Call<ImageuploadResponse> call, Response<ImageuploadResponse> response) {
                dialog1.dismiss();
                if (response.body().getStatus().equals("success")) {
//                    AddproductActivity.this.onBackPressed();


                }
            }
            @Override
            public void onFailure(Call<ImageuploadResponse> call, Throwable t) {
            }
        });
    }
    private void Editproduct(String connector_id, String str_category_id, String str_subcategory_id, String str_product_name, String str_quantity, String str_measurement_id, String str_price, String str_commission, String str_grade_name, String str_desc, String filepath1) {
        dialog2 = new ProgressDialog(AddproductActivity.this);
        dialog2.setCancelable(false);
        dialog2.setMessage("Loading......");
        dialog2.show();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<StatusResponse> call = service.edithubproduct(str_product_id,String.valueOf(str_seller_id),str_category_id,str_subcategory_id,str_product_name,str_desc,str_price,str_commission,str_measurement_id,str_quantity,str_image_url,str_grade_name);
        call.enqueue(new Callback<StatusResponse>() {
            @Override
            public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                dialog2.dismiss();
                if (response.body().getStatus().equals("success")) {
//                    AddproductActivity.this.onBackPressed();
                    AddproductActivity.this.onBackPressed();
                }
            }
            @Override
            public void onFailure(Call<StatusResponse> call, Throwable t) {

            }
        });
    }

}
