package com.agriculture.kisanbandiconnector.Hub;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.agriculture.kisanbandiconnector.Adapter.ProductlistAdapter;
import com.agriculture.kisanbandiconnector.Models.Resbonse.ProductlistResponse;
import com.agriculture.kisanbandiconnector.R;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProductsActivity extends AppCompatActivity {

    RecyclerView product;
    String Connectorid="";
    SwipeRefreshLayout layout;
    LinearLayout listlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        product=findViewById(R.id.product);
        Connectorid = Sharedpreference.getStringValue(getApplicationContext(),"Connectorid");
        product.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
//        layout=view.findViewById(R.id.swipe);
        listlayout=findViewById(R.id.listlayout);
        productlist(Connectorid);
        findViewById(R.id.add_new_product).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), AddproductActivity.class));
            }
        });
//        layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                productlist(Connectorid);
//            }
//        });
    }

    private void productlist(String connectorid) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<ProductlistResponse> call=service.getproductdetails(connectorid);
        call.enqueue(new Callback<ProductlistResponse>() {
            @Override
            public void onResponse(Call<ProductlistResponse> call, Response<ProductlistResponse> response) {
                //  layout.setRefreshing(false);
                if (response.body().getStatus().equals("success")){
                    if(response.body().getProductListDetails().size()==0)
                    {
                        product.setVisibility(View.GONE);
                        //  listlayout.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        //  listlayout.setVisibility(View.GONE);
                        ProductlistAdapter adapter=new ProductlistAdapter(getApplicationContext(),response.body().getProductListDetails());
                        product.setAdapter(adapter);
                    }
                }
            }
            @Override
            public void onFailure(Call<ProductlistResponse> call, Throwable t) {
            }
        });
    }
}
