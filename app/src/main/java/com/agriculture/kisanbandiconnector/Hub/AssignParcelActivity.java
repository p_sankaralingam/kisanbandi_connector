package com.agriculture.kisanbandiconnector.Hub;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.Adapter.HubassignorderlistAdapter;
import com.agriculture.kisanbandiconnector.Models.Resbonse.HuborderlistResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.StatusResponse;
import com.agriculture.kisanbandiconnector.R;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AssignParcelActivity extends AppCompatActivity {
    RecyclerView assignlist;
    String Connectorid="";
    HubassignorderlistAdapter adapter;
    String str_connector_id="";
    String user_id="";

    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_parcel);
        str_connector_id=getIntent().getStringExtra("id");
        user_id=getIntent().getStringExtra("userid");
        assignlist=findViewById(R.id.assignlist);
        assignlist.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        Connectorid= Sharedpreference.getStringValue(getApplicationContext(),"Connectorid");
        getallparcellist(Connectorid);
        findViewById(R.id.assign).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                        if (adapter.getSelected().size() > 0) {
                            StringBuilder stringBuilder = new StringBuilder();
                            ArrayList<String>data=new ArrayList<>();
                            for (int i = 0; i < adapter.getSelected().size(); i++) {
                                stringBuilder.append(adapter.getSelected().get(i).getOrderId());
                                data.add(adapter.getSelected().get(i).getOrderId());
                                stringBuilder.append(",");
                            }
                            String result = TextUtils.join(",",data);
                            Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
                            Log.d("data",result);
                            assignparcel(result,str_connector_id,user_id,Connectorid);
                        } else {
                            //showToast("No Selection");
                           // assignparcel()
                            Toast.makeText(getApplicationContext(),"No Parcel Selected",Toast.LENGTH_LONG).show();
                            }
            }
        });
    }
    private void getallparcellist(String connectorid) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<HuborderlistResponse> call=service.gethuborderlist(connectorid);
        call.enqueue(new Callback<HuborderlistResponse>() {
            @Override
            public void onResponse(Call<HuborderlistResponse> call, Response<HuborderlistResponse> response) {
                if (response.body().getStatus().equals("success")){
                     adapter=new HubassignorderlistAdapter(getApplicationContext(),response.body().getOrderList());
                     assignlist.setAdapter(adapter);
                }
            }
            @Override
            public void onFailure(Call<HuborderlistResponse> call, Throwable t) {
            }
        });
    }

    private void assignparcel(String Order_id, String connectorid, String connector_user_id, String hubid) {
        dialog=new ProgressDialog(AssignParcelActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage("Parcel Assigning To connector");
        dialog.show();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<StatusResponse> call=service.addassignparcel(Order_id,connectorid,connector_user_id,hubid);
        call.enqueue(new Callback<StatusResponse>() {
            @Override
            public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
              dialog.dismiss();
                if (response.body().getStatus().equals("success")){
                    AssignParcelActivity.this.onBackPressed();
                }
            }
            @Override
            public void onFailure(Call<StatusResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
