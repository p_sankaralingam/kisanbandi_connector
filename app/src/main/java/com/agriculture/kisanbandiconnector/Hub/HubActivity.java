package com.agriculture.kisanbandiconnector.Hub;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;

import com.agriculture.kisanbandiconnector.Adapter.PageAdapter;
import com.agriculture.kisanbandiconnector.Fragments.KBCListFragment;
import com.agriculture.kisanbandiconnector.Fragments.OrdersFragment;
import com.agriculture.kisanbandiconnector.Fragments.ProductListFragment;
import com.agriculture.kisanbandiconnector.R;
import com.agriculture.kisanbandiconnector.Utils.ForceUpdateChecker;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;
import com.google.android.material.tabs.TabLayout;

public class HubActivity extends AppCompatActivity implements ForceUpdateChecker.OnUpdateNeededListener {
    ViewPager viewPager;
    TabLayout tabLayout;
    LocationManager locationManager;
    private static  final int REQUEST_LOCATION=1;
    String Lat = "", Lang = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hub);
        ForceUpdateChecker.with(HubActivity.this).onUpdateNeeded(this).check();
        locationManager=(LocationManager) HubActivity.this.getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            //Write Function To enable gps
            OnGPS();
        }
        else
        {
            //GPS is already On then
            getLocation();
        }
        viewPager = findViewById(R.id.dashboard_viewpager);
        tabLayout = findViewById(R.id.dashboard_tab);
        setUpVierPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }
    public void setUpVierPager(ViewPager viewPager){
        PageAdapter adapter = new PageAdapter(getSupportFragmentManager());
        adapter.addFragment(new OrdersFragment(),"Orders");
        adapter.addFragment(new KBCListFragment(),"Connectors");
        adapter.addFragment(new ProductListFragment(),"Products");
        viewPager.setAdapter(adapter);
    }

    private void OnGPS() {
        final androidx.appcompat.app.AlertDialog.Builder builder= new androidx.appcompat.app.AlertDialog.Builder(HubActivity.this);
        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });
        final androidx.appcompat.app.AlertDialog alertDialog=builder.create();
        alertDialog.show();
    }

    private void getLocation() {
        //Check Permissions again
        if (ActivityCompat.checkSelfPermission(HubActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(HubActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) !=PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(HubActivity .this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }
        else
        {
            Location LocationGps= locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location LocationNetwork=locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location LocationPassive=locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            if (LocationGps !=null)
            {
                double lat=LocationGps.getLatitude();
                double longi=LocationGps.getLongitude();
                Lat=String.valueOf(lat);
                Lang=String.valueOf(longi);
                Sharedpreference.storeStringValue(getApplicationContext(),"lat",Lat);
                Sharedpreference.storeStringValue(getApplicationContext(),"lang",Lang);
                Log.d("Locationdata",Lat);
                Log.d("Locationdata",Lang);
            }
            else if (LocationNetwork !=null)
            {
                double lat=LocationNetwork.getLatitude();
                double longi=LocationNetwork.getLongitude();
                Lat=String.valueOf(lat);
                Lang=String.valueOf(longi);
                Sharedpreference.storeStringValue(getApplicationContext(),"lat",Lat);
                Sharedpreference.storeStringValue(getApplicationContext(),"lang",Lang);
                Log.d("Locationdata",Lat);
                Log.d("Locationdata",Lang);
            }
            else if (LocationPassive !=null)
            {
                double lat=LocationPassive.getLatitude();
                double longi=LocationPassive.getLongitude();
                Lat=String.valueOf(lat);
                Lang=String.valueOf(longi);
                Sharedpreference.storeStringValue(getApplicationContext(),"lat",Lat);
                Sharedpreference.storeStringValue(getApplicationContext(),"lang",Lang);
                Log.d("Locationdata",Lat);
                Log.d("Locationdata",Lang);
            }
            else
            {
                Toast.makeText(HubActivity.this, "Can't Get Your Location", Toast.LENGTH_SHORT).show();
            }
            //Thats All Run Your App
        }
    }

    @Override
    public void onBackPressed() {
        android.app.AlertDialog.Builder alertDialog=new android.app.AlertDialog.Builder(this);
        alertDialog.setTitle("Confirm Exit..!!!");
        alertDialog.setMessage("Are you sure want to exit");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        android.app.AlertDialog alertDialog1=alertDialog.create();
        alertDialog1.show();
    }
    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onUpdateNeeded(final String updateUrl, final Boolean forupdate) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please, update app to new version get new features.")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(forupdate)
                                {
                                    finish();
                                }
                                else {
                                    dialog.dismiss();
                                }
                            }
                        }).create();
        dialog.show();
    }
}