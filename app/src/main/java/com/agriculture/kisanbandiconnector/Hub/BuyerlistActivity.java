package com.agriculture.kisanbandiconnector.Hub;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.Adapter.BuyerslistAdapter;
import com.agriculture.kisanbandiconnector.Models.Resbonse.BuyerlistResponse;
import com.agriculture.kisanbandiconnector.R;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BuyerlistActivity extends AppCompatActivity {


    RecyclerView buyerslist;
    String connectorid="";
    LinearLayout nodata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyerlist);
        buyerslist=findViewById(R.id.buyerslist);
        buyerslist.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        connectorid= Sharedpreference.getStringValue(getApplicationContext(),"Connectorid");
        nodata=findViewById(R.id.nodata);
        getbuyerlist(connectorid);
    }
    public void getbuyerlist(final String connectorid) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<BuyerlistResponse> call = service.getbuyerlist(connectorid);
        call.enqueue(new Callback<BuyerlistResponse>() {
            @Override
            public void onResponse(Call<BuyerlistResponse> call, Response<BuyerlistResponse> response) {
                if (response.body().getStatus().equals("success")) {
                    if (response.body().getBuyerList().size() != 0) {
                        nodata.setVisibility(View.GONE);
                        BuyerslistAdapter adapter = new BuyerslistAdapter(getApplicationContext(), response.body().getBuyerList());
                        buyerslist.setAdapter(adapter);
                    }
                }
                else {
                    buyerslist.setVisibility(View.GONE);
                    nodata.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onFailure(Call<BuyerlistResponse> call, Throwable t) {

            }
        });
    }
}
