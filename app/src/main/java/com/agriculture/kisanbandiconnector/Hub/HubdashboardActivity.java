package com.agriculture.kisanbandiconnector.Hub;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.agriculture.kisanbandiconnector.LoginActivity;
import com.agriculture.kisanbandiconnector.Models.Resbonse.HubDashboardResponse;
import com.agriculture.kisanbandiconnector.NotificationActivity;
import com.agriculture.kisanbandiconnector.ProfileActivity;
import com.agriculture.kisanbandiconnector.R;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HubdashboardActivity extends AppCompatActivity {
    CardView availableorders,productlist,connectorslist,logout,buyerslist,sellerslist;
    ImageView profile;
    String User_id="";
    String Connector_id="";
    ImageView notification;
    ProgressDialog dialog;
    TextView availableorderscount,productscount,connectors_count,sellerscount,buyerscount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hubdashboard);
        productscount=findViewById(R.id.products);
        availableorderscount=findViewById(R.id.availableorderscount);
        connectors_count=findViewById(R.id.connectors_count);
        sellerscount=findViewById(R.id.sellerscount);
        buyerscount=findViewById(R.id.buyerscount);
        availableorders=findViewById(R.id.availableorders);
        productlist=findViewById(R.id.productlist);
        profile=findViewById(R.id.profile);
        connectorslist=findViewById(R.id.connectorslist);
        logout=findViewById(R.id.logout);
        buyerslist=findViewById(R.id.buyerslist);
        notification=findViewById(R.id.notification);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), NotificationActivity.class));
            }
        });
        User_id = Sharedpreference.getStringValue(getApplicationContext(), "User_id");
        Connector_id = Sharedpreference.getStringValue(getApplicationContext(), "Connectorid");
        Log.d("userid:",User_id);
        Log.d("userid",Connector_id);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
            }
        });
        availableorders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),AvailableOrdersListActivity.class));
            }
        });

        getallcounts(Connector_id);
        sellerslist=findViewById(R.id.seller_list);
        productlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),ProductsActivity.class)); }
        });
        connectorslist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),ConnectorslistActivity.class));
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(HubdashboardActivity.this);
                alertDialogBuilder.setMessage("Are you sure want to logout?");
                alertDialogBuilder.setCancelable(false);
                alertDialogBuilder.setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                Sharedpreference.ClearPreference(getApplicationContext());
                                Intent intent2 = new Intent(getApplicationContext(), LoginActivity.class);
                                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent2);
                            }
                        });
                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
        buyerslist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),BuyerlistActivity.class));
            }
        });
        sellerslist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),SellerslistActivity.class));
            }
        });
    }
    @Override
    public void onBackPressed() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(this);
        alertDialog.setTitle("Confirm Exit..!!!");
        alertDialog.setMessage("Are you sure want to exit");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        android.app.AlertDialog alertDialog1 = alertDialog.create();
        alertDialog1.show();
    }
    public void getallcounts(String connector_id)
    {
        dialog=new ProgressDialog(HubdashboardActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage("Loading....");
        dialog.show();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<HubDashboardResponse> call=service.getdashboardcount(connector_id);
        call.enqueue(new Callback<HubDashboardResponse>() {
            @Override
            public void onResponse(Call<HubDashboardResponse> call, Response<HubDashboardResponse> response) {
                dialog.dismiss();
                if (response.body().getStatus().equals("success")){
//                    AssignParcelActivity.this.onBackPressed();
                    availableorderscount.setText(String.valueOf(response.body().getCount().getHubOrder()));
                    productscount.setText(String.valueOf(response.body().getCount().getHubProduct()));
                    connectors_count.setText(String.valueOf(response.body().getCount().getConnectorCount()));
                    sellerscount.setText(String.valueOf(response.body().getCount().getSellerList()));
                    buyerscount.setText(String.valueOf(response.body().getCount().getBuyerList()));
                }
            }
            @Override
            public void onFailure(Call<HubDashboardResponse> call, Throwable t) {
            }
        });
    }
}
