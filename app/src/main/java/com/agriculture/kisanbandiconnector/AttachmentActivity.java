package com.agriculture.kisanbandiconnector;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.abedelazizshe.lightcompressorlibrary.CompressionListener;
import com.abedelazizshe.lightcompressorlibrary.VideoCompressor;
import com.abedelazizshe.lightcompressorlibrary.VideoQuality;
import com.agriculture.kisanbandiconnector.Models.Resbonse.AttachmentsResbonse;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.Constants;
import com.agriculture.kisanbandiconnector.Utils.ProgressRequestBody;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;
import com.agriculture.kisanbandiconnector.Utils.UploadCallBacks;
import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.agriculture.kisanbandiconnector.Utils.Constants.READ_REQUEST_CODE;

public class AttachmentActivity extends AppCompatActivity implements UploadCallBacks {
    private static final String TAG = AttachmentActivity.class.getSimpleName();
    LinearLayout img_layout, video_layout;
    ImageView kyc;
    EditText bank_name, account_no, account_ifsc;
    TextView image_error, video_error;
    Button record_video, register;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    private VideoView displayRecordedVideo;
    String filepath1, pathToStoredVideo = "No Video", User_id, outputFileAbsolutePath;
    private Uri fileUri, uri;
    private static final int REQUEST_VIDEO_CAPTURE = 300;
    private static final int MY_PERMISSIONS_REQUEST_CODE = 123;
    String _video, mCurrentPhotoPath;
    boolean isImageAdded = false;
    boolean isVideoAdded = false;
    boolean approve = false;
    String my_video;
    String filePathEnvironment;
    private Handler handler = new Handler();
    ProgressDialog progressDialog1;
    ProgressDialog newdialog;
    File file = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attachment);
        img_layout = findViewById(R.id.img_layout);
        video_layout = findViewById(R.id.layout_video);
        bank_name = findViewById(R.id.connector_Account_name);
        account_no = findViewById(R.id.connector_Account_number);
        account_ifsc = findViewById(R.id.connector_Account_ifsc);
        SharedPreferences sharedPreferences = getSharedPreferences("Approve", Context.MODE_PRIVATE);
        approve = sharedPreferences.getBoolean("Approval", false);
        kyc = findViewById(R.id.connector_kyc2);
        record_video = findViewById(R.id.btnRecordVideo2);
        register = findViewById(R.id.Attachment_submit);
        image_error = findViewById(R.id.imag_error);
        video_error = findViewById(R.id.video_error);
        displayRecordedVideo = findViewById(R.id.video_display2);

        kyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
               /* Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                openGalleryIntent.setType("image/*");
                startActivityForResult(openGalleryIntent, Constants.REQUEST_GALLERY_CODE);*/
                image_error.setVisibility(View.GONE);
            }
        });
        User_id = Sharedpreference.getStringValue(AttachmentActivity.this, "User_id");
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == register) {
                    boolean error = false;
                    if (bank_name.getText().toString().isEmpty()) {
                        error = true;
                        bank_name.setError("Enter Bank Name");
                    }
                    if (account_no.getText().toString().isEmpty()) {
                        error = true;
                        account_no.setError("Enter Account no");
                    }
                    if (account_ifsc.getText().toString().isEmpty()) {
                        error = true;
                        account_ifsc.setError("Enter ifsc code");
                    }
                    if (filepath1 == null) {
                        error = true;
                        image_error.setVisibility(View.VISIBLE);
                    }
                    if (file == null) {
                        error = true;
                        video_error.setVisibility(View.VISIBLE);
                    }
                    if (!error) {
                        getAttachment(User_id, filepath1, file.getPath(), account_no.getText().toString(),
                                account_ifsc.getText().toString(), bank_name.getText().toString());
                    }
                }
            }
        });

        if (!isDeviceSupportCamera()) {
            Toast.makeText(getApplicationContext(), "Sorry! Your device doesn't support camera", Toast.LENGTH_LONG).show();
            finish();
        }

        record_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayRecordedVideo.setVisibility(View.VISIBLE);
                video_error.setVisibility(View.GONE);
                Intent videoCaptureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                videoCaptureIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 180);

                videoCaptureIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
                long maxVideoSize = 20 * 1024 * 1024; // 10 MB
                videoCaptureIntent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, maxVideoSize);
                videoCaptureIntent.putExtra(MediaStore.Video.Thumbnails.HEIGHT, 180);
                videoCaptureIntent.putExtra(MediaStore.Video.Thumbnails.WIDTH, 120);
                videoCaptureIntent.putExtra(MediaStore.EXTRA_FINISH_ON_COMPLETION, true);
                if (videoCaptureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(videoCaptureIntent, REQUEST_VIDEO_CAPTURE);
                    _video = "video".toLowerCase();
                }
            }
        });
        ContextCompat.checkSelfPermission(AttachmentActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        ContextCompat.checkSelfPermission(AttachmentActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        ContextCompat.checkSelfPermission(AttachmentActivity.this,Manifest.permission.CAMERA);
        // Do something, when permissions not granted
        if (ActivityCompat.shouldShowRequestPermissionRationale((AttachmentActivity.this), Manifest.permission.ACCESS_FINE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale((AttachmentActivity.this), Manifest.permission.READ_EXTERNAL_STORAGE)|| ActivityCompat.shouldShowRequestPermissionRationale((AttachmentActivity.this), Manifest.permission.CAMERA)) {
            // If we should give explanation of requested permissions
            // Show an alert dialog here with request explanation
            AlertDialog.Builder builder = new AlertDialog.Builder(AttachmentActivity.this);
            builder.setMessage("Camera , Location and  Write External" + " Storage permissions are required to do the task.");
            builder.setTitle("Please grant those permissions");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    ActivityCompat.requestPermissions(
                            AttachmentActivity.this,
                            new String[]{
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.CAMERA
                            },
                            MY_PERMISSIONS_REQUEST_CODE
                    );
                }
            });
            builder.setNeutralButton("Cancel", null);
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            // Directly request for required permissions, without explanation
            ActivityCompat.requestPermissions(
                    AttachmentActivity.this,
                    new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST_CODE
            );
        }
    }

    private boolean isDeviceSupportCamera() {
        if (getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }

    @AfterPermissionGranted(READ_REQUEST_CODE)
    public void selectImage() {
        String title = "Open Photo";
        CharSequence[] itemlist = {"Take a Photo", "Pick from Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(AttachmentActivity.this);
        builder.setTitle(title);
        builder.setItems(itemlist, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:// Take Photo
                        // Do Take Photo task here
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, Constants.CAMERA_REQUEST);
                        /* if (EasyPermissions.hasPermissions(RegisterActivity.this.getApplicationContext(), Manifest.permission.CAMERA)) {
                         *//*  Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, Constants.CAMERA_REQUEST);*//*
                        } else {
                            EasyPermissions.requestPermissions(RegisterActivity.this, getString(R.string.permission_read_file), Constants.CAMERA_REQUEST, Manifest.permission.CAMERA);
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, Constants.CAMERA_REQUEST);
                        }*/
                        break;
                    case 1:// Choose Existing Photo
                        // Do Pick Photo task here
                        Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                        openGalleryIntent.setType("image/*");
                        startActivityForResult(openGalleryIntent, Constants.REQUEST_GALLERY_CODE);
                        /*if (EasyPermissions.hasPermissions(RegisterActivity.this.getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                            openGalleryIntent.setType("image/*");
                            startActivityForResult(openGalleryIntent, Constants.REQUEST_GALLERY_CODE);
                        } else {
                            EasyPermissions.requestPermissions(RegisterActivity.this, getString(R.string.permission_read_file), Constants.READ_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
                            Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                            openGalleryIntent.setType("image/*");
                            startActivityForResult(openGalleryIntent, Constants.REQUEST_GALLERY_CODE);
                        }*/
                        break;

                    default:
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.setCancelable(true);
        alert.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_VIDEO_CAPTURE) {
            fileUri = data.getData();
            if (EasyPermissions.hasPermissions(AttachmentActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // displayRecordedVideo.setVideoURI(fileUri);
                pathToStoredVideo = getRealPathFromURIPath(fileUri, AttachmentActivity.this);
                Log.d(TAG, "Recorded Video Path " + pathToStoredVideo);
                isVideoAdded = true;
                compressvideo(pathToStoredVideo);
               /*    uploadVideo(fileUri,video_Url);
             invokeUploadImageService(uri,filepath1,video_Url);
                //Store the video to your server
                newRegister(name.getText().toString(),Last_name,email.getText().toString(),mobile.getText().toString(),aadhar.getText().toString(),Video_Url,Doc_Url,Address,
                        Landmark,State_name,District_name,pincode.getText().toString(),Country,Device_id);*/

            } else {
                EasyPermissions.requestPermissions(AttachmentActivity.this, "read_file", READ_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
            }


        }
        if (Constants.REQUEST_GALLERY_CODE == requestCode && resultCode == Activity.RESULT_OK && data != null) {
            uri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = AttachmentActivity.this.getContentResolver().query(uri, filePathColumn, null, null, null);
            if (cursor == null)
                return;
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            filepath1 = cursor.getString(columnIndex);
            isImageAdded = true;
//            progressBar.setVisibility(View.VISIBLE);
            /*  uploadVideo(fileUri,video_Url);*/
            /*invokeUploadImageService(uri,filepath1,video_Url);*/
            Glide.with(this).load(uri).into(kyc);
        }
        if (requestCode == Constants.CAMERA_REQUEST) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                File photoFile = null;
                try {
                    photoFile = createImageFile(bitmap);
                } catch (IOException ex) {
                    // Error occurred while creating the File
                    ex.printStackTrace();
                }
                // Continue only if the File was successfully created
                if (photoFile != null)
                    uri = Uri.fromFile(photoFile);
              /*  invokeUploadImageService(uri,filepath1,video_Url);
         invokeUploadImageService(uri,filepath1,video_Url);*/
                Glide.with(this).load(uri).into(kyc);
                filepath1 = String.valueOf(photoFile);
                isImageAdded = true;
            }
        }
    }

    private File createImageFile(Bitmap bitmap) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName,  /* prefix */".jpg",/* suffix */AttachmentActivity.this.getCacheDir() /* directory */);
        /* FileOutputStream fileOutputStream = new FileOutputStream(image);*/
        OutputStream outputStream = new FileOutputStream(image);
        /*bitmap = ((BitmapDrawable) kyc.getDrawable()).getBitmap();//newlly added*/
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        outputStream.flush();
        outputStream.close();
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.e("Getpath", "Cool" + mCurrentPhotoPath);
        return image;
    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        /* super.onRequestPermissionsResult(requestCode, permissions, grantResults);*/
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CODE: {
                // When request is cancelled, the results array are empty
                if (
                        (grantResults.length > 0) && (grantResults[0] + grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    // Permissions are granted
                    /*Toast.makeText(RegisterActivity.this,"Permissions granted.",Toast.LENGTH_SHORT).show();*/
                } else {
                    // Permissions are denied
                    Toast.makeText(AttachmentActivity.this, "Permissions denied.", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    public void getAttachment(String user_id, String doc_url, String video_url, String account_no, String ifsc_code, String bankname) {
        progressDialog1 = new ProgressDialog(AttachmentActivity.this);
        progressDialog1.setCancelable(false);
        progressDialog1.setMessage("Loading please wait...");
        progressDialog1.setMax(100);
        progressDialog1.setIndeterminate(false);
        progressDialog1.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog1.show();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);

        RequestBody User_id = RequestBody.create(MultipartBody.FORM, user_id);
        File photofile = new File(String.valueOf(doc_url));
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), photofile);
        MultipartBody.Part image = MultipartBody.Part.createFormData("doc_url", photofile.getName(), reqFile);

//        File file1 = new File(String.valueOf(video_url));
      //  RequestBody reqFile1 = RequestBody.create(MediaType.parse("video/*"), file);
        ProgressRequestBody reqFile1=new ProgressRequestBody(file,this);
        MultipartBody.Part video = MultipartBody.Part.createFormData("video_url", file.getName(), reqFile1);
        RequestBody Account_no = RequestBody.create(MultipartBody.FORM, account_no);
        RequestBody Ifsc = RequestBody.create(MultipartBody.FORM, ifsc_code);
        RequestBody Bank = RequestBody.create(MultipartBody.FORM, bankname);
        Call<AttachmentsResbonse> call = service.getAttachments(User_id, image, video, Account_no, Ifsc, Bank);
        call.enqueue(new Callback<AttachmentsResbonse>() {
            @Override
            public void onResponse(Call<AttachmentsResbonse> call, Response<AttachmentsResbonse> response) {
                progressDialog1.dismiss();
                if (response.body().getStatus().equals("success")) {
                    Intent intent = new Intent(AttachmentActivity.this, DeliveryModeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                       /* SharedPreferences sharedPreferences=getSharedPreferences("Attach", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor=sharedPreferences.edit();
                        editor.commit();
                        editor.putBoolean("Attachment",true);
                        editor.apply();*/
                    Sharedpreference.storeBooleanValue(AttachmentActivity.this, "Attachment", true);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<AttachmentsResbonse> call, Throwable t) {

            }
        });
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }


    }

    public void compressvideo(String pathToStoredVideo) {
        try {
            file = File.createTempFile("temp", ".mp4", this.getCacheDir());
        } catch (IOException e) {
            e.printStackTrace();
        }
        VideoCompressor.start(pathToStoredVideo, file.getPath(), new CompressionListener() {
            @Override
            public void onStart() {
                // Compression start
                newdialog = new ProgressDialog(AttachmentActivity.this);
                newdialog.setCancelable(false);
                newdialog.setMessage("Compressing please wait...");
                newdialog.setMax(100);
                newdialog.setIndeterminate(false);
                newdialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                newdialog.show();
            }

            @Override
            public void onSuccess() {
                newdialog.dismiss();
                long size = file.length();

                Log.d("videofilesize", String.valueOf(size/1024));
                displayRecordedVideo.setVideoURI(Uri.parse(file.getAbsolutePath()));
                Toast.makeText(getApplicationContext(), String.valueOf(size), Toast.LENGTH_LONG).show();
                // On Compression success
            }

            @Override
            public void onFailure(String failureMessage) {
                // On Failure
            }

            @Override
            public void onProgress(final float v) {
                // Update UI with progress value
                runOnUiThread(new Runnable() {
                    public void run() {
//                        progress.setText(progressPercent + "%");
                        newdialog.setProgress((int) v);
                    }
                });
            }

            @Override
            public void onCancelled() {
                // On Cancelled
            }
        }, VideoQuality.MEDIUM, false, false);
    }


    @Override
    public void onProgressUpdate(int percentage) {
        progressDialog1.setProgress(percentage);
    }
}
