package com.agriculture.kisanbandiconnector;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.agriculture.kisanbandiconnector.Models.Resbonse.StatusResponse;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.Constants;
import com.agriculture.kisanbandiconnector.Utils.ProgressRequestBody;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;
import com.agriculture.kisanbandiconnector.Utils.UploadCallBacks;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import pub.devrel.easypermissions.AfterPermissionGranted;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.agriculture.kisanbandiconnector.Utils.Constants.READ_REQUEST_CODE;

public class FinalDeliveryAcceptActivity extends AppCompatActivity implements UploadCallBacks {
    ImageView image;
    Button submit;
    TextInputEditText date, time;
    DatePickerDialog picker;
    TimePickerDialog timePickerDialog;
    Uri uri;
    String filepath1;
    String _video, mCurrentPhotoPath;
    boolean submited = false;
    private static final int MY_PERMISSIONS_REQUEST_CODE = 123;
    String Connectorid = "";
    String str_order_id;
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Connectorid = Sharedpreference.getStringValue(getApplicationContext(), "Connectorid");
        setContentView(R.layout.activity_final_delivery_accept);
        image = findViewById(R.id.image);
        submit = findViewById(R.id.submit);
        date = findViewById(R.id.date);
        time = findViewById(R.id.time);
        str_order_id = getIntent().getStringExtra("orderid");
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(FinalDeliveryAcceptActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                date.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
                picker.getDatePicker().setMinDate(System.currentTimeMillis());
            }
        });
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar cldr = Calendar.getInstance();
                int hour = cldr.get(Calendar.HOUR_OF_DAY);
                int minutes = cldr.get(Calendar.MINUTE);
                timePickerDialog = new TimePickerDialog(FinalDeliveryAcceptActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int i, int i1) {
                        String formattedTime = "";
                        String sHour = "00";
                        // converting hour to tow digit if its between 0 to 9. (e.g. 7 to 07)
                        if (i < 10)
                            sHour = "0" + i;
                        else
                            sHour = String.valueOf(i);
                        if (i1 < 10)
                            formattedTime = "0" + i1;
                        else
                            formattedTime = String.valueOf(i1);
                        time.setText(sHour + ":" + formattedTime);
                    }
                }, hour, minutes, false);
                timePickerDialog.show();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submited = true;
                String str_date = date.getText().toString();
                String str_time = time.getText().toString();
                adddeliverydetails(Connectorid, str_order_id, str_date, str_time, filepath1);
            }
        });
        ContextCompat.checkSelfPermission(FinalDeliveryAcceptActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        ContextCompat.checkSelfPermission(FinalDeliveryAcceptActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        ContextCompat.checkSelfPermission(FinalDeliveryAcceptActivity.this, Manifest.permission.CAMERA);
        // Do something, when permissions not granted
        if (ActivityCompat.shouldShowRequestPermissionRationale((FinalDeliveryAcceptActivity.this), Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale((this), Manifest.permission.CAMERA)) {
            // If we should give explanation of requested permissions
            // Show an alert dialog here with request explanation
            AlertDialog.Builder builder = new AlertDialog.Builder(FinalDeliveryAcceptActivity.this);
            builder.setMessage("Camera , Write External" + " Storage permissions are required to do the task.");
            builder.setTitle("Please grant those permissions");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    ActivityCompat.requestPermissions(
                            FinalDeliveryAcceptActivity.this,
                            new String[]{
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.CAMERA
                            },
                            MY_PERMISSIONS_REQUEST_CODE
                    );
                }
            });
            builder.setNeutralButton("Cancel", null);
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            // Directly request for required permissions, without explanation
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST_CODE
            );
        }
    }
    @AfterPermissionGranted(READ_REQUEST_CODE)
    public void selectImage() {
        String title = "Open Photo";
        CharSequence[] itemlist = {"Take a Photo", "Pick from Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(FinalDeliveryAcceptActivity.this);
        builder.setTitle(title);
        builder.setItems(itemlist, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:// Take Photo
                        // Do Take Photo task here
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, Constants.CAMERA_REQUEST);
                        /* if (EasyPermissions.hasPermissions(RegisterActivity.this.getApplicationContext(), Manifest.permission.CAMERA)) {
                         *//*  Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, Constants.CAMERA_REQUEST);*//*
                        } else {
                            EasyPermissions.requestPermissions(RegisterActivity.this, getString(R.string.permission_read_file), Constants.CAMERA_REQUEST, Manifest.permission.CAMERA);
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, Constants.CAMERA_REQUEST);
                        }*/
                        break;
                    case 1:// Choose Existing Photo
                        // Do Pick Photo task here
                        Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                        openGalleryIntent.setType("image/*");
                        startActivityForResult(openGalleryIntent, Constants.REQUEST_GALLERY_CODE);
                        /*if (EasyPermissions.hasPermissions(RegisterActivity.this.getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                            openGalleryIntent.setType("image/*");
                            startActivityForResult(openGalleryIntent, Constants.REQUEST_GALLERY_CODE);
                        } else {
                            EasyPermissions.requestPermissions(RegisterActivity.this, getString(R.string.permission_read_file), Constants.READ_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
                            Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                            openGalleryIntent.setType("image/*");
                            startActivityForResult(openGalleryIntent, Constants.REQUEST_GALLERY_CODE);
                        }*/
                        break;
                    default:
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.setCancelable(true);
        alert.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Constants.REQUEST_GALLERY_CODE == requestCode && resultCode == Activity.RESULT_OK && data != null) {
            uri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = this.getContentResolver().query(uri, filePathColumn, null, null, null);
            if (cursor == null)
                return;
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            filepath1 = cursor.getString(columnIndex);
//            isImageAdded = true;
//            progressBar.setVisibility(View.VISIBLE);
            /*  uploadVideo(fileUri,video_Url);*/
            /*invokeUploadImageService(uri,filepath1,video_Url);*/
            Glide.with(this).load(uri).into(image);
        }
        if (requestCode == Constants.CAMERA_REQUEST) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                File photoFile = null;
                try {
                    photoFile = createImageFile(bitmap);
                } catch (IOException ex) {
                    // Error occurred while creating the File
                    ex.printStackTrace();
                }
                // Continue only if the File was successfully created
                if (photoFile != null)
                    uri = Uri.fromFile(photoFile);
              /*  invokeUploadImageService(uri,filepath1,video_Url);
         invokeUploadImageService(uri,filepath1,video_Url);*/
                Glide.with(this).load(uri).into(image);
                filepath1 = String.valueOf(photoFile);
//                isImageAdded = true;
            }
        }
    }

    private File createImageFile(Bitmap bitmap) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName,  /* prefix */".jpg",/* suffix */this.getCacheDir() /* directory */);
        /* FileOutputStream fileOutputStream = new FileOutputStream(image);*/
        OutputStream outputStream = new FileOutputStream(image);
        /*bitmap = ((BitmapDrawable) kyc.getDrawable()).getBitmap();//newlly added*/
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, outputStream);
        outputStream.flush();
        outputStream.close();
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.e("Getpath", "Cool" + mCurrentPhotoPath);
        return image;
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        /* super.onRequestPermissionsResult(requestCode, permissions, grantResults);*/
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CODE: {
                // When request is cancelled, the results array are empty
                if (
                        (grantResults.length > 0) && (grantResults[0] + grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    // Permissions are granted
                    /*Toast.makeText(RegisterActivity.this,"Permissions granted.",Toast.LENGTH_SHORT).show();*/
                } else {
                    // Permissions are denied
                    Toast.makeText(this, "Permissions denied.", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }
    @Override
    public void onBackPressed() {
        if (submited) {
            Intent intent = new Intent(getApplicationContext(), DeliveryModeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            super.onBackPressed();
        }
    }
    private void adddeliverydetails(String connectorid1, String orderid, String str_date, String time, String filepath1) {
        dialog = new ProgressDialog(FinalDeliveryAcceptActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage("Loading......");
        dialog.show();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        RequestBody c_id = RequestBody.create(MultipartBody.FORM, connectorid1);
        RequestBody s_date = RequestBody.create(MultipartBody.FORM, str_date);
        RequestBody str_time = RequestBody.create(MultipartBody.FORM, time);
        RequestBody str_order_id = RequestBody.create(MultipartBody.FORM, orderid);
        File file = new File(filepath1);
        ProgressRequestBody reqFile1 = new ProgressRequestBody(file, this);
        MultipartBody.Part image = MultipartBody.Part.createFormData("prod_imageurl", file.getName(), reqFile1);
        Call<StatusResponse> call = service.finaldeliveryupload(c_id, image, s_date, str_time, str_order_id);
        call.enqueue(new Callback<StatusResponse>() {
            @Override
            public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                //  layout.setRefreshing(false);
                dialog.dismiss();
                if (response.body() != null) {
                    if (response.body().getStatus().equals("success")) {
                        final Dialog dialog = new Dialog(FinalDeliveryAcceptActivity.this);
                        dialog.setCancelable(true);
                        dialog.setContentView(R.layout.parcel_accept);
                        Button home = dialog.findViewById(R.id.home);
                        Button mydeliveries = dialog.findViewById(R.id.mydeliveries);
                        home.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(getApplicationContext(), DeliveryModeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        });
                        mydeliveries.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(getApplicationContext(), MyorderActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.putExtra("noback", true);
                                startActivity(intent);
                            }
                        });
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.show();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"Try Again with another ImageLater",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<StatusResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onProgressUpdate(int i) {

    }
}
