package com.agriculture.kisanbandiconnector;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.agriculture.kisanbandiconnector.Models.Resbonse.LoginResbonse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.StatusResponse;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApprovalActivity extends AppCompatActivity {
    Boolean approve=false;
    String Device_id="",User_type="",User_id,Password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approval);
        final Button start=findViewById(R.id.next);
        Device_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        User_type="KBC";
        User_id= Sharedpreference.getStringValue(ApprovalActivity.this,"User_id");
        Password=Sharedpreference.getStringValue(ApprovalActivity.this,"Password");
       /* start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newlogin(User_id,Password,Device_id,User_type);
            }
        });*/
        boolean tokenupdate= Sharedpreference.getBoolen(getApplicationContext(),"updatetoken");
        String token=Sharedpreference.getStringValue(getApplicationContext(),"token");
        if(tokenupdate)
        {
            updateFCMToken(token,User_id,"KBC",Device_id);
        }
    }
    public void newlogin(final String User_id, String user_password, String device_id, String user_type){
        final ProgressDialog progressDialog1 = new ProgressDialog(ApprovalActivity.this);
        progressDialog1.setCancelable(false);
        progressDialog1.setMessage("Loading please wait...");
        progressDialog1.show();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<LoginResbonse> call=service.getlogin(User_id,user_password,device_id,user_type);
        call.enqueue(new Callback<LoginResbonse>() {
            @Override
            public void onResponse(Call<LoginResbonse> call, Response<LoginResbonse> response) {
                progressDialog1.dismiss();
                if (response.body().getStatus().equals("success")){
                   if (response.body().getConnectorstatus().equals("success")){
                        Intent intent=new Intent(ApprovalActivity.this, DeliveryModeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                       Sharedpreference.storeBooleanValue(ApprovalActivity.this,"Approval",true);
                        startActivity(intent);
                    } else {
                       Toast.makeText(getApplicationContext(),"Still Not Approved",Toast.LENGTH_SHORT).show();
                   }
                }

            }

            @Override
            public void onFailure(Call<LoginResbonse> call, Throwable t) {

            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        newlogin(User_id,Password,Device_id,User_type);
    }

    private void updateFCMToken(String token, String session_id, String kbb, String s) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);


        Call<StatusResponse> call=service.updateFcmToken(session_id,token,kbb,s);
        call.enqueue(new Callback<StatusResponse>() {
            @Override
            public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                if (response.body().getStatus().equals("success")) {
                }
            }

            @Override
            public void onFailure(Call<StatusResponse> call, Throwable t) {

            }
        });




    }
}
