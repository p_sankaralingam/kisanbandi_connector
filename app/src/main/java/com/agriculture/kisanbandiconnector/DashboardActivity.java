package com.agriculture.kisanbandiconnector;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.Adapter.HublistAdapter;
import com.agriculture.kisanbandiconnector.Models.Resbonse.HublistResponse;
import com.agriculture.kisanbandiconnector.Services.Api;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DashboardActivity extends AppCompatActivity {

    RecyclerView Hublist;
    String str_from_lat="",str_from_long="",str_to_lat="",str_to_long="",str_vehicle_type="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        str_from_lat=getIntent().getStringExtra("fromlat");
        str_from_long=getIntent().getStringExtra("fromlong");
        str_to_lat=getIntent().getStringExtra("tolat");
        str_to_long=getIntent().getStringExtra("tolong");
        str_vehicle_type=getIntent().getStringExtra("vehicletype");
        Hublist=findViewById(R.id.hublist);
        Hublist.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        getAllhublist(str_from_lat,str_from_long,str_to_lat,str_to_long,str_vehicle_type);
    }

    private void getAllhublist(final String str_from_lat, final String str_from_long, final String str_to_lat, final String str_to_long, String str_vehicle_type) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        OkHttpClient.Builder oktHttpClient = new OkHttpClient.Builder()
//                .addInterceptor(new NetworkConnectionInterceptor(mContext));
//        // Adding NetworkConnectionInterceptor with okHttpClientBuilder.
//        oktHttpClient.addInterceptor(logging);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<HublistResponse> call=service.getallhubdetails(str_from_lat,str_to_lat,str_from_long,str_to_long,str_vehicle_type);
        call.enqueue(new Callback<HublistResponse>() {
            @Override
            public void onResponse(Call<HublistResponse> call, Response<HublistResponse> response) {
                if (response.body().getStatus().equals("success")){
                    HublistAdapter adapter=new HublistAdapter(getApplicationContext(),response.body().getDeliveryDetails(),str_from_lat,str_from_long,str_to_lat,str_to_long);
                    Hublist.setAdapter(adapter);
                }
            }
            @Override
            public void onFailure(Call<HublistResponse> call, Throwable t) {

            }
        });
    }




}
