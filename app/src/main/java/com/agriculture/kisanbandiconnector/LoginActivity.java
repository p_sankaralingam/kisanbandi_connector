package com.agriculture.kisanbandiconnector;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.agriculture.kisanbandiconnector.Hub.HubdashboardActivity;
import com.agriculture.kisanbandiconnector.Models.Resbonse.ForgetPasswordResbonse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.LoginResbonse;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;
import com.basgeekball.awesomevalidation.AwesomeValidation;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    Button login,forget,forget2;
    EditText user_id,password;
    String Device_id="",User_type="",Connector_id,Email="";
    TextView txt;
    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;
    Boolean register=false;
    private long mLastClickTime = 0;
    private AwesomeValidation awesomeValidation;
    LinearLayout password_layout;
    ImageView show_psw;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        login=findViewById(R.id.new_login);
        user_id=findViewById(R.id.user_name);
        password=findViewById(R.id.user_password);
        forget=findViewById(R.id.forget_password);
        password_layout=findViewById(R.id.password_layout);
        forget2=findViewById(R.id.forget_password2);
        show_psw=findViewById(R.id.show_password);
        SharedPreferences sharedPreferences=getSharedPreferences("Data", Context.MODE_PRIVATE);
        register = sharedPreferences.getBoolean("Register", false);
        SharedPreferences sharedPreferences1 = getSharedPreferences("myKey", MODE_PRIVATE);
        Email = sharedPreferences1.getString("value","");
        user_id.setText(Email);
        Device_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        User_type="KBC";
        if (isConnected()){
        }
        else {
            AlertDialog.Builder builder =new AlertDialog.Builder(LoginActivity.this);
            builder.setTitle("No Internet Connection");
            builder.setIcon(R.drawable.ic_no_internet);
            builder.setMessage("You need to have mobile data or wifi");
            builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            builder.show();
        }
        show_psw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(password.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
                    show_psw.setImageResource(R.drawable.ic_hide_password);

                    //Show Password
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                else{
                    show_psw.setImageResource(R.drawable.ic_show_password);

                    //Hide Password
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user_id.getText().length()==0){
                    Toast.makeText(getApplicationContext(),"Please enter user_Id",Toast.LENGTH_SHORT).show();
                }
                else if (password.getText().length()==0){
                    Toast.makeText(LoginActivity.this, "Please enter password", Toast.LENGTH_SHORT).show();
                }
                else {
                        forget.setEnabled(true);
                        newlogin(user_id.getText().toString(),password.getText().toString(),Device_id,User_type);
                }
            }
        });
        forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                password.setText("");
              /*  password_layout.setVisibility(View.GONE);
                login.setVisibility(View.GONE);
                forget.setVisibility(View.GONE);
                forget2.setVisibility(View.VISIBLE);*/
                if (user_id.getText().length()==0){
                    Toast.makeText(getApplicationContext(),"Enter User ID",Toast.LENGTH_SHORT).show();
                }else {
                    /*Dialog();*/
                    getForgetPassword(user_id.getText().toString());
                }
            }
        });

        /*forget2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user_id.getText().length()==0){
                    geterror();
                    txt.setText("Enter User ID");
                }else {
                    *//*Dialog();*//*
                    getForgetPassword(user_id.getText().toString());
                }
            }
        });*/
    }

    public void newlogin(final String User_id, String user_password, String device_id, String user_type){
        final ProgressDialog progressDialog1 = new ProgressDialog(LoginActivity.this);
        progressDialog1.setCancelable(false);
        progressDialog1.setMessage("Loading please wait...");
        progressDialog1.show();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);

        Call<LoginResbonse> call=service.getlogin(User_id,user_password,device_id,user_type);
        call.enqueue(new Callback<LoginResbonse>() {
            @Override
            public void onResponse(Call<LoginResbonse> call, Response<LoginResbonse> response) {
                progressDialog1.dismiss();
                if (response.body().getStatus().equals("success")){
                    Sharedpreference.storeBooleanValue(LoginActivity.this,"Login",true);
                    Sharedpreference.storeStringValue(LoginActivity.this,"User_id",user_id.getText().toString());
                    Sharedpreference.storeStringValue(LoginActivity.this,"Password",password.getText().toString());
                    if (response.body().getConnectorstatus().equals("attachement")){
                        Intent intent=new Intent(LoginActivity.this,DeliveryModeActivity.class);
                        Sharedpreference.storeStringValue(LoginActivity.this,"User_id",response.body().getUserId());
                        Sharedpreference.storeStringValue(LoginActivity.this,"Connectorid",response.body().getConnectorId());
                        Sharedpreference.storeBooleanValue(LoginActivity.this,"Success",true);
                        Sharedpreference.storeBooleanValue(LoginActivity.this,"Attachment",true);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }else if (response.body().getConnectorstatus().equals("approval")){
                        Intent intent=new Intent(LoginActivity.this,ApprovalActivity.class);
                        Sharedpreference.storeStringValue(LoginActivity.this,"User_id",response.body().getUserId());
                    /*    Sharedpreference.storeBooleanValue(LoginActivity.this,"Approval",true);*/
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }else if (response.body().getConnectorstatus().equals("success")){
                        Sharedpreference.storeStringValue(LoginActivity.this,"User_id",response.body().getUserId());
                            Sharedpreference.storeStringValue(LoginActivity.this,"Connectorid",response.body().getConnectorId());
                     if(response.body().getHub_status().equalsIgnoreCase("1"))
                     {
                         Intent intent=new Intent(LoginActivity.this, HubdashboardActivity.class);
                         Sharedpreference.storeBooleanValue(LoginActivity.this,"Success",true);
                         /*   Sharedpreference.storeBooleanValue(LoginActivity.this,"Approved",true);*/

                         Sharedpreference.storeBooleanValue(LoginActivity.this,"hubstatus",true);
                         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                         startActivity(intent);
                     }
                     else
                     {
                         Intent intent=new Intent(LoginActivity.this, DeliveryModeActivity.class);
                         Sharedpreference.storeBooleanValue(LoginActivity.this,"Success",true);
                         /*   Sharedpreference.storeBooleanValue(LoginActivity.this,"Approved",true);*/
                         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                         startActivity(intent);
                     }
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(),"Wrong userName & Password",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResbonse> call, Throwable t) {
            }
        });
    }
    public void Dialog(){
        final AlertDialog.Builder  alertDialog=new AlertDialog.Builder(LoginActivity.this);
        alertDialog.setTitle("Reset Password");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Are you Sure Reset Password")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        password_layout.setVisibility(View.VISIBLE);
                        login.setVisibility(View.VISIBLE);
                        getForgetPassword(user_id.getText().toString());
                    }
                })
                .setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        user_id.setText("");
                        dialog.dismiss();
                        password_layout.setVisibility(View.VISIBLE);
                        login.setVisibility(View.VISIBLE);
                    }
                });

        AlertDialog alertdialog = alertDialog.create();
        alertdialog.show();
    }
    public void getForgetPassword(String email){
        final ProgressDialog progressDialog1 = new ProgressDialog(LoginActivity.this);
        progressDialog1.setCancelable(false);
        progressDialog1.setMessage("Loading Please wait...");
        progressDialog1.show();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<ForgetPasswordResbonse> call=service.getPassword(email);
        call.enqueue(new Callback<ForgetPasswordResbonse>() {
            @Override
            public void onResponse(Call<ForgetPasswordResbonse> call, final Response<ForgetPasswordResbonse> response) {
                progressDialog1.dismiss();
                if (response.body().getResult().equals("success")){
                    final AlertDialog.Builder  alertDialog=new AlertDialog.Builder(LoginActivity.this);
                    alertDialog.setTitle("Reset Password");
                    alertDialog.setCancelable(false);
                    alertDialog.setMessage("Password  Link is Send To:"+response.body().getEmail())
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                         user_id.setText(response.body().getEmail());
                                }
                            });
                    AlertDialog alertdialog = alertDialog.create();
                    alertdialog.show();
                }
                if (response.body().getResult().equals("fail")){
                    Toast.makeText(getApplicationContext(),"User Not Found",Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getApplicationContext(),"Fail to login",Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ForgetPasswordResbonse> call, Throwable t) {
            }
        });
    }
    public void geterror(){
        LayoutInflater inflater = getLayoutInflater();
        View layout=inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.toast_layout));
        txt= layout.findViewById(R.id.error_toast_txt);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.BOTTOM, 0, 100);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }
    private boolean isConnected(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public void showAlertDialog(int layout){
        dialogBuilder=new AlertDialog.Builder(LoginActivity.this);
        View layoutview=getLayoutInflater().inflate(layout,null);
        Button btn=layoutview.findViewById(R.id.done);
        dialogBuilder.setView(layoutview);
        alertDialog=dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        alertDialog.setCancelable(false);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }
}
