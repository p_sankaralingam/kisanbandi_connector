package com.agriculture.kisanbandiconnector.Fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.Adapter.ConnectorslistAdapter;
import com.agriculture.kisanbandiconnector.Models.Resbonse.ConnectorlistResponse;
import com.agriculture.kisanbandiconnector.R;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class KBCListFragment extends Fragment {


    public KBCListFragment() {
        // Required empty public constructor
    }


    RecyclerView connectorslist;
    String connectorid="";


    LinearLayout listlayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_kbclist, container, false);
        connectorslist=view.findViewById(R.id.connectorslist);
        connectorslist.setLayoutManager(new LinearLayoutManager(getActivity()));
        connectorid= Sharedpreference.getStringValue(getActivity(),"Connectorid");
        listlayout=view.findViewById(R.id.listlayout);

        getconnecotorslist(connectorid);
        return view;
    }


    public void getconnecotorslist(final String connectorid) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<ConnectorlistResponse> call = service.getconnectorslist(connectorid);
        call.enqueue(new Callback<ConnectorlistResponse>() {
            @Override
            public void onResponse(Call<ConnectorlistResponse> call, Response<ConnectorlistResponse> response) {
                if (response.body().getStatus().equals("success")) {
                    if (response.body().getConnectorList().size() != 0) {
                        ConnectorslistAdapter adapter = new ConnectorslistAdapter(getActivity(), response.body().getConnectorList());
                        connectorslist.setAdapter(adapter);
                    } else {
                        connectorslist.setVisibility(View.GONE);
                        listlayout.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ConnectorlistResponse> call, Throwable t) {
            }
        });
    }

}
