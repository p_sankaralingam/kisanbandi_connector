package com.agriculture.kisanbandiconnector.Fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.Adapter.MyorderslistAdapter;
import com.agriculture.kisanbandiconnector.Models.Resbonse.DeliveryDetailsResponse;
import com.agriculture.kisanbandiconnector.R;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class RejectedOrders extends Fragment {


    public RejectedOrders() {
        // Required empty public constructor
    }

    RecyclerView rejectedorder;
    String User_id="";

    LinearLayout emptylayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_rejected_orders, container,                                                                                                                           false);
        rejectedorder=view.findViewById(R.id.rejected);
        rejectedorder.setLayoutManager(new LinearLayoutManager(getActivity()));
        emptylayout=view.findViewById(R.id.emptylayout);
        User_id = Sharedpreference.getStringValue(getActivity(), "User_id");
        getrejected(User_id);
        return view;
    }
    public void getrejected(String userid) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<DeliveryDetailsResponse> call = service.getrejecteddelivery(userid);
        call.enqueue(new Callback<DeliveryDetailsResponse>() {
            @Override
            public void onResponse(Call<DeliveryDetailsResponse> call, Response<DeliveryDetailsResponse> response) {
                if (response.body().getStatus().equals("success")) {

                    if(response.body().getDeliveryDetails().size()!=0)
                    {
                        emptylayout.setVisibility(View.GONE);
                        MyorderslistAdapter adapter = new MyorderslistAdapter(getActivity(), response.body().getDeliveryDetails(), "reject");
                        rejectedorder.setAdapter(adapter);
                    }
                    else
                    {
                        emptylayout.setVisibility(View.VISIBLE);
                        rejectedorder.setVisibility(View.GONE);
                    }

                }
            }
            @Override
            public void onFailure(Call<DeliveryDetailsResponse> call, Throwable t) {
            }
        });

    }
}
