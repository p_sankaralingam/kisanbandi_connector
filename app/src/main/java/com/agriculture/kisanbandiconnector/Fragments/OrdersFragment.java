package com.agriculture.kisanbandiconnector.Fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.Adapter.HuborderlistAdapter;
import com.agriculture.kisanbandiconnector.Interface.Rejectinterface;
import com.agriculture.kisanbandiconnector.Models.Resbonse.HuborderlistResponse;
import com.agriculture.kisanbandiconnector.R;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrdersFragment extends Fragment implements Rejectinterface {


    public OrdersFragment() {
        // Required empty public constructor
    }


    RecyclerView orderslist;
    String str_lat="",str_lang="";
    String Connectorid="";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_orders, container, false);
        orderslist=view.findViewById(R.id.orderlist);
        orderslist.setLayoutManager(new LinearLayoutManager(getActivity()));
        Connectorid= Sharedpreference.getStringValue(getActivity(),"Connectorid");
        str_lat= Sharedpreference.getStringValue(getActivity(),"lat");
        str_lang=Sharedpreference.getStringValue(getActivity(),"lang");
//        getallparcellist("");
        getallparcellist(Connectorid);
        return view;
    }


    private void getallparcellist(String connectorid) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<HuborderlistResponse> call=service.gethuborderlist(connectorid);
        call.enqueue(new Callback<HuborderlistResponse>() {
            @Override
            public void onResponse(Call<HuborderlistResponse> call, Response<HuborderlistResponse> response) {
                if (response.body().getStatus().equals("success")){
                    HuborderlistAdapter adapter=new HuborderlistAdapter(getActivity(),response.body().getOrderList());
                    orderslist.setAdapter(adapter);
                }
            }
            @Override
            public void onFailure(Call<HuborderlistResponse> call, Throwable t) {
            }
        });
    }

    @Override
    public void raject(Boolean reject) {

    }
}
