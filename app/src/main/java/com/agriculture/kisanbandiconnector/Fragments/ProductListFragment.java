package com.agriculture.kisanbandiconnector.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.agriculture.kisanbandiconnector.Adapter.ProductlistAdapter;
import com.agriculture.kisanbandiconnector.Hub.AddproductActivity;
import com.agriculture.kisanbandiconnector.Models.Resbonse.ProductlistResponse;
import com.agriculture.kisanbandiconnector.R;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductListFragment extends Fragment {

    public ProductListFragment() {
        // Required empty public constructor
    }
    RecyclerView product;
    String Connectorid="";
    SwipeRefreshLayout layout;
    LinearLayout listlayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_product_list, container, false);
        product=view.findViewById(R.id.product);
        Connectorid = Sharedpreference.getStringValue(getActivity(),"Connectorid");
        product.setLayoutManager(new LinearLayoutManager(getActivity()));
//        layout=view.findViewById(R.id.swipe);
        listlayout=view.findViewById(R.id.listlayout);
        productlist(Connectorid);
        view.findViewById(R.id.add_new_product).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), AddproductActivity.class));
            }
        });
//        layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                productlist(Connectorid);
//            }
//        });
        return  view;
    }


    private void productlist(String connectorid) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<ProductlistResponse> call=service.getproductdetails(connectorid);
        call.enqueue(new Callback<ProductlistResponse>() {
            @Override
            public void onResponse(Call<ProductlistResponse> call, Response<ProductlistResponse> response) {
              //  layout.setRefreshing(false);
                if (response.body().getStatus().equals("success")){
                    if(response.body().getProductListDetails().size()==0)
                    {
                        product.setVisibility(View.GONE);
                      //  listlayout.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                      //  listlayout.setVisibility(View.GONE);
                        ProductlistAdapter adapter=new ProductlistAdapter(getActivity(),response.body().getProductListDetails());
                        product.setAdapter(adapter);
                    }
                }
            }
            @Override
            public void onFailure(Call<ProductlistResponse> call, Throwable t) {
            }
        });
    }
    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);

        if (isVisible()) {
            // we check that the fragment is becoming visible
            productlist(Connectorid);
        }

    }
}
