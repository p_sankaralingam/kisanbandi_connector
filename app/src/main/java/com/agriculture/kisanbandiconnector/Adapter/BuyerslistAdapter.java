package com.agriculture.kisanbandiconnector.Adapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.agriculture.kisanbandiconnector.Models.Resbonse.BuyerlistResponse;
import com.agriculture.kisanbandiconnector.R;
import java.util.ArrayList;
import java.util.List;
public class BuyerslistAdapter extends RecyclerView.Adapter<BuyerslistAdapter.ViewHolder> {
    Context context;
    List<BuyerlistResponse.BuyerList>BuyerList=new ArrayList<>();
    public BuyerslistAdapter(Context activity, List<BuyerlistResponse.BuyerList> productListDetails) {
        this.context=activity;
        this.BuyerList=productListDetails;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(context).inflate(R.layout.buyers_list_row,viewGroup,false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.connectorid.setText(BuyerList.get(i).getBuyerId());
        viewHolder.connectorname.setText(BuyerList.get(i).getFullName());
        viewHolder.mobile.setText(BuyerList.get(i).getPhonenumber());
//        viewHolder.location.setText(BuyerList.get(i).get);
//        viewHolder.vehicle.setText(Connectorslist.get(i).get());
    }
    @Override
    public int getItemCount() {
        return BuyerList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView connectorid,connectorname,mobile,vehicle,status,assignorder,location;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            connectorid=itemView.findViewById(R.id.connectorid);
            connectorname=itemView.findViewById(R.id.connectorname);
            mobile=itemView.findViewById(R.id.mobile);
            location=itemView.findViewById(R.id.location);
        }
    }
}
