package com.agriculture.kisanbandiconnector.Adapter;


import android.content.Intent;
import android.graphics.drawable.PictureDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.DeliveryModeActivity;
import com.agriculture.kisanbandiconnector.LanguageSelectionActivity;
import com.agriculture.kisanbandiconnector.Models.Resbonse.LangaugeResponse;
import com.agriculture.kisanbandiconnector.R;
import com.agriculture.kisanbandiconnector.SplashScreenActivity;
import com.agriculture.kisanbandiconnector.Utils.LanguageCall;
import com.agriculture.kisanbandiconnector.Utils.LocaleManager;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;
import com.bumptech.glide.RequestBuilder;

import java.util.ArrayList;
import java.util.List;


public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.ViewHolder> {
    private RequestBuilder<PictureDrawable> requestBuilder;
    LanguageSelectionActivity context;
    List<LangaugeResponse.LanguageDetail> LanguageList = new ArrayList<>();
    Boolean profile = false;
    LanguageCall call;

    public LanguageAdapter(List<LangaugeResponse.LanguageDetail> languageDetails, Boolean profile) {

    }

    public LanguageAdapter(LanguageSelectionActivity languageSelectionActivity, List<LangaugeResponse.LanguageDetail> languageDetails, Boolean profile) {
        this.context = languageSelectionActivity;
        this.LanguageList = languageDetails;
        this.profile = profile;

        this.call=languageSelectionActivity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.language_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        viewHolder.name.setText(LanguageList.get(i).getLanguageName());
        boolean hubstatus = Sharedpreference.getBoolen(context, "hubstatus");

//        if (profile) {
//
//            if (hubstatus) {
//                viewHolder.name.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        //  viewHolder.name.setChecked(true);
//                        //   Sharedpreference.storeBooleanValue(context,"language",true);
//                        Intent intent = new Intent(context, HubdashboardActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        context.startActivity(intent);
//                    }
//
//
//                });
//            } else {
//                viewHolder.name.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        //  viewHolder.name.setChecked(true);
//                        //   Sharedpreference.storeBooleanValue(context,"language",true);
////                        Intent intent=new Intent(context, DeliveryModeActivity.class);
////                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
////                        context.startActivity(intent);
//
//                        setNewLocale(context, LocaleManager.KANNADA);
//                    }
//
//
//                });
//            }
//        } else {
//            viewHolder.name.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    // viewHolder.name.setChecked(true);
//                    //  Sharedpreference.storeBooleanValue(context,"language",true);
//                    Intent intent = new Intent(context, MainActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    context.startActivity(intent);
//                }
//            });
//        }


        viewHolder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  viewHolder.name.setChecked(true);
                //   Sharedpreference.storeBooleanValue(context,"language",true);
//                        Intent intent=new Intent(context, DeliveryModeActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        context.startActivity(intent);

//                call.call(true);

                if(viewHolder.name.getText().equals("Kannada"))
                {
                    Sharedpreference.storeStringValue(context,"languagename","kn");
                }
                else
                {
                    Sharedpreference.storeStringValue(context,"languagename","en");
                }
                Intent intent =new Intent(context, SplashScreenActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);

//                setNewLocale(context, LocaleManager.KANNADA);
            }


        });
    }

    private void setNewLocale(LanguageSelectionActivity mContext, @LocaleManager.LocaleDef String language) {
        LocaleManager.setNewLocale(mContext, language);
        Intent intent = new Intent(mContext, DeliveryModeActivity.class);
        mContext.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public int getItemCount() {
        return LanguageList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RadioButton name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
        }
    }
}
