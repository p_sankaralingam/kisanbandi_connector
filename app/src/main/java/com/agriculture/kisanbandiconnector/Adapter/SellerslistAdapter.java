package com.agriculture.kisanbandiconnector.Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.Models.Resbonse.SellerlistResponse;
import com.agriculture.kisanbandiconnector.R;

import java.util.ArrayList;
import java.util.List;

public class SellerslistAdapter extends RecyclerView.Adapter<SellerslistAdapter.ViewHolder> {
    Context context;
    List<SellerlistResponse.SellerList>Connectorslist=new ArrayList<>();
    public SellerslistAdapter(Context activity, List<SellerlistResponse.SellerList> productListDetails) {
        this.context=activity;
        this.Connectorslist=productListDetails;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(context).inflate(R.layout.connectors_list_row,viewGroup,false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.connectorid.setText(Connectorslist.get(i).getSellersId());
        viewHolder.connectorname.setText(Connectorslist.get(i).getFirstName());
        viewHolder.mobile.setText(Connectorslist.get(i).getPhonenumber());
//        viewHolder.vehicle.setText(Connectorslist.get(i).get());
    }
    @Override
    public int getItemCount() {
        return Connectorslist.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView connectorid,connectorname,mobile,vehicle,status,assignorder;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            connectorid=itemView.findViewById(R.id.connectorid);
            connectorname=itemView.findViewById(R.id.connectorname);
            mobile=itemView.findViewById(R.id.mobile);
            vehicle=itemView.findViewById(R.id.vehicles);
            status=itemView.findViewById(R.id.status);
            assignorder=itemView.findViewById(R.id.assignorder);
        }
    }
}
