package com.agriculture.kisanbandiconnector.Adapter;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.Hub.AssignParcelActivity;
import com.agriculture.kisanbandiconnector.Models.Resbonse.ConnectorlistResponse;
import com.agriculture.kisanbandiconnector.R;

import java.util.ArrayList;
import java.util.List;


public class ConnectorslistAdapter extends RecyclerView.Adapter<ConnectorslistAdapter.ViewHolder> {

    Context context;
    List<ConnectorlistResponse.ConnectorList>Connectorslist=new ArrayList<>();
    public ConnectorslistAdapter(Context activity, List<ConnectorlistResponse.ConnectorList> productListDetails) {
        this.context=activity;
        this.Connectorslist=productListDetails;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(context).inflate(R.layout.connectors_list_row,viewGroup,false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        viewHolder.connectorid.setText(Connectorslist.get(i).getConnectorId());
        viewHolder.connectorname.setText(Connectorslist.get(i).getFirstName());
        viewHolder.mobile.setText(Connectorslist.get(i).getPhonenumber());
        viewHolder.vehicle.setText(Connectorslist.get(i).getVehicleType());

        viewHolder.assignorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(context, AssignParcelActivity.class);
                intent.putExtra("id", Connectorslist.get(i).getConnectorId());
                intent.putExtra("userid",Connectorslist.get(i).getUserId());

                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }
    @Override
    public int getItemCount() {
        return Connectorslist.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView connectorid,connectorname,mobile,vehicle,status,assignorder;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            connectorid=itemView.findViewById(R.id.connectorid);
            connectorname=itemView.findViewById(R.id.connectorname);
            mobile=itemView.findViewById(R.id.mobile);
            vehicle=itemView.findViewById(R.id.vehicles);
            status=itemView.findViewById(R.id.status);
            assignorder=itemView.findViewById(R.id.assignorder);
        }
    }
}
