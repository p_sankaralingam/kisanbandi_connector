package com.agriculture.kisanbandiconnector.Adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.PictureDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.Interface.Rejectinterface;
import com.agriculture.kisanbandiconnector.Models.Resbonse.HuborderlistResponse;
import com.agriculture.kisanbandiconnector.ParceldetailsActivity;
import com.agriculture.kisanbandiconnector.R;
import com.bumptech.glide.RequestBuilder;

import java.util.ArrayList;
import java.util.List;


public class HuborderlistAdapter extends RecyclerView.Adapter<HuborderlistAdapter.ViewHolder> {
    private RequestBuilder<PictureDrawable> requestBuilder;
    Context context;
    List<HuborderlistResponse.OrderList>Parcellist=new ArrayList<>();
    String str_from_lat="",str_from_long="",str_to_lat="",str_to_long="";
    Rejectinterface rejectinterface;

    public HuborderlistAdapter(Context activity, List<HuborderlistResponse.OrderList> orderList) {
        this.context=activity;
        this.Parcellist=orderList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(context).inflate(R.layout.parcellist_row,viewGroup,false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
            viewHolder.orderid.setText("Order Id :"+Parcellist.get(i).getOrderId());
            viewHolder.address.setText("Address\n"+Parcellist.get(i).getAddress());
            viewHolder.orderscount.setText("No.of.Items : "+Parcellist.get(i).getProductQuantity());
            viewHolder.amount.setText("Total : "+Parcellist.get(i).getGrandTotal());
           // viewHolder.date.setText(Parcellist.get(i).get);
//            viewHolder.call.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                }
//            });
            viewHolder.accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, ParceldetailsActivity.class);
//                    intent.putExtra("");
                    intent.putExtra("orderid",Parcellist.get(i).getOrderId());
                    intent.putExtra("name",Parcellist.get(i).getFullName());
                    intent.putExtra("address",Parcellist.get(i).getAddress());
                    intent.putExtra("paymentmode",Parcellist.get(i).getPaymentMode());
                    intent.putExtra("noofitems",Parcellist.get(i).getProductQuantity());
                    intent.putExtra("total",Parcellist.get(i).getGrandTotal());
                    intent.putExtra("email",Parcellist.get(i).getEmail());
                    intent.putExtra("phoneno",Parcellist.get(i).getPhonenumber());
                    intent.putExtra("lat",Parcellist.get(i).getOrderLat());
                    intent.putExtra("long",Parcellist.get(i).getOrderLong());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });

//            viewHolder.reject.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    rejectinterface.raject(true);
//                }
//            });
    }
    @Override
    public int getItemCount() {
        return Parcellist.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,address,orderscount,orderid,amount,date;
        ImageView call,map;
        TextView reject,accept;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            orderid=itemView.findViewById(R.id.orderid);
            address=itemView.findViewById(R.id.address);
            orderscount=itemView.findViewById(R.id.orderitem);
            amount=itemView.findViewById(R.id.amount);
            date=itemView.findViewById(R.id.date);
            reject=itemView.findViewById(R.id.reject);
            accept=itemView.findViewById(R.id.accept);

        }
    }
}
