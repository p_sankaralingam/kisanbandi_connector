package com.agriculture.kisanbandiconnector.Adapter;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.Hub.AddproductActivity;
import com.agriculture.kisanbandiconnector.Models.Resbonse.ProductlistResponse;
import com.agriculture.kisanbandiconnector.R;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;


public class ProductlistAdapter extends RecyclerView.Adapter<ProductlistAdapter.ViewHolder> {

    Context context;
    List<ProductlistResponse.ProductListDetail>Productlist=new ArrayList<>();



    public ProductlistAdapter(Context activity, List<ProductlistResponse.ProductListDetail> productListDetails) {
        this.context=activity;
        this.Productlist=productListDetails;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(context).inflate(R.layout.product_list_row,viewGroup,false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
            viewHolder.name.setText(Productlist.get(i).getProdName());
            viewHolder.price.setText(Productlist.get(i).getProdPrice());
            viewHolder.qty.setText(Productlist.get(i).getProdQuantity());
            viewHolder.desc.setText(Productlist.get(i).getProdDetail());
            Glide.with(context).load(Productlist.get(i).getProdImageurl()).into(viewHolder.image);
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProductlistResponse.ProductListDetail  data=Productlist.get(i);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(data);
                    Intent intent=new Intent(context, AddproductActivity.class);
//                    intent.putExtra("name",Productlist.get(i).getProdName());
//                    intent.putExtra("price",Productlist.get(i).getProdPrice());
//                    intent.putExtra("qty",Productlist.get(i).getProdQuantity());
//                    intent.putExtra("")
                    intent.putExtra("data",myJson);
                    intent.putExtra("edit",true);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
    }
    @Override
    public int getItemCount() {
        return Productlist.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,price,qty,desc;
        ImageView image,map;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            qty=itemView.findViewById(R.id.qty);
            price=itemView.findViewById(R.id.price);
            desc=itemView.findViewById(R.id.desc);
            image=itemView.findViewById(R.id.image);
        }
    }
}
