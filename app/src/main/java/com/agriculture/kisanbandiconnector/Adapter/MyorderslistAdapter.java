package com.agriculture.kisanbandiconnector.Adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.PictureDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.Models.Resbonse.DeliveryDetailsResponse;
import com.agriculture.kisanbandiconnector.ParceldetailsActivity;
import com.agriculture.kisanbandiconnector.R;
import com.bumptech.glide.RequestBuilder;

import java.util.ArrayList;
import java.util.List;


public class MyorderslistAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private RequestBuilder<PictureDrawable> requestBuilder;
    Context context;
    String type="";
    List<DeliveryDetailsResponse.DeliveryDetail>Parcellist=new ArrayList<>();
    public MyorderslistAdapter(FragmentActivity activity, List<DeliveryDetailsResponse.DeliveryDetail> deliveryDetails, String current) {
        this.context=activity;
        this.Parcellist=deliveryDetails;
        this.type=current;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view;
          if(viewType==0)
          {
               view= LayoutInflater.from(context).inflate(R.layout.completed_delivery_details,viewGroup,false);
               return new ViewHolder(view);
          }
          else if(viewType==1)
          {
               view= LayoutInflater.from(context).inflate(R.layout.current_delivery_details,viewGroup,false);
               return new PendingViewHolder(view);
          }
          else
          {
               view= LayoutInflater.from(context).inflate(R.layout.myorder_order_list_row,viewGroup,false);
               return new CompletedViewHolder(view);
          }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {

        if(viewHolder.getItemViewType()==0)
        {
            ViewHolder vaultItemHolder = (ViewHolder) viewHolder;
            vaultItemHolder.orderid.setText("Order Id :"+Parcellist.get(i).getOrderId());
            vaultItemHolder.address.setText("Address\n"+Parcellist.get(i).getAddress());
            vaultItemHolder.orderscount.setText("No.of.Items : "+Parcellist.get(i).getProdQuantity());
            vaultItemHolder.amount.setText("Total : "+Parcellist.get(i).getProdGrandTotal());

            vaultItemHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, ParceldetailsActivity.class);
                    intent.putExtra("orderid",Parcellist.get(i).getOrderId());
                    intent.putExtra("type","details");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }
        else if(viewHolder.getItemViewType()==1)
        {
            PendingViewHolder pendingViewHolder=(PendingViewHolder)viewHolder;
            pendingViewHolder.orderid.setText("Order Id :"+Parcellist.get(i).getOrderId());
            pendingViewHolder.address.setText("Address\n"+Parcellist.get(i).getAddress());
            pendingViewHolder.orderscount.setText("No.of.Items : "+Parcellist.get(i).getProdQuantity());
            pendingViewHolder.amount.setText("Total : "+Parcellist.get(i).getProdGrandTotal());

            pendingViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, ParceldetailsActivity.class);
                    intent.putExtra("orderid",Parcellist.get(i).getOrderId());
                    intent.putExtra("type","details");

                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }
        else
        {
            CompletedViewHolder completedViewHolder=(CompletedViewHolder)viewHolder;
            completedViewHolder.orderid.setText("Order Id :"+Parcellist.get(i).getOrderId());
            completedViewHolder.address.setText("Address\n"+Parcellist.get(i).getAddress());
            completedViewHolder.orderscount.setText("No.of.Items : "+Parcellist.get(i).getProdQuantity());
            completedViewHolder.amount.setText("Total : "+Parcellist.get(i).getProdGrandTotal());
            completedViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, ParceldetailsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("orderid",Parcellist.get(i).getOrderId());
                    intent.putExtra("type","details");
                    context.startActivity(intent);
                }
            });
        }
    }
    @Override
    public int getItemViewType(int position) {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous
        int dd=0;
        if(type.equalsIgnoreCase("completed"))
        {
            dd=0;
        }
        else if(type.equalsIgnoreCase("current"))
        {
            dd=1;
        }
        else
        {
            dd=2;
        }
        return dd;
    }

    @Override
    public int getItemCount() {
        return Parcellist.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,address,orderscount,orderid,amount,date;
        ImageView call,map;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            orderid=itemView.findViewById(R.id.orderid);
            address=itemView.findViewById(R.id.address);
            orderscount=itemView.findViewById(R.id.orderitem);
            amount=itemView.findViewById(R.id.amount);
            date=itemView.findViewById(R.id.date);
        }
    }
    public class PendingViewHolder extends RecyclerView.ViewHolder {
        TextView name,address,orderscount,orderid,amount,date;
        ImageView call,map;
        public PendingViewHolder(@NonNull View itemView) {
            super(itemView);
            orderid=itemView.findViewById(R.id.orderid);
            address=itemView.findViewById(R.id.address);
            orderscount=itemView.findViewById(R.id.orderitem);
            amount=itemView.findViewById(R.id.amount);
            date=itemView.findViewById(R.id.date);
        }
    }
    public class CompletedViewHolder extends RecyclerView.ViewHolder {
        TextView name,address,orderscount,orderid,amount,date;
        ImageView call,map;
        public CompletedViewHolder(@NonNull View itemView) {
            super(itemView);
            orderid=itemView.findViewById(R.id.orderid);
            address=itemView.findViewById(R.id.address);
            orderscount=itemView.findViewById(R.id.orderitem);
            amount=itemView.findViewById(R.id.amount);
            date=itemView.findViewById(R.id.date);
        }
    }
}
