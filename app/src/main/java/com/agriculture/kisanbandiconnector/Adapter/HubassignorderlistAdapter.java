package com.agriculture.kisanbandiconnector.Adapter;


import android.content.Context;
import android.graphics.drawable.PictureDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.Interface.Rejectinterface;
import com.agriculture.kisanbandiconnector.Models.Resbonse.HuborderlistResponse;
import com.agriculture.kisanbandiconnector.R;
import com.bumptech.glide.RequestBuilder;

import java.util.ArrayList;
import java.util.List;


public class HubassignorderlistAdapter extends RecyclerView.Adapter<HubassignorderlistAdapter.ViewHolder> {
    private RequestBuilder<PictureDrawable> requestBuilder;
    Context context;
    List<HuborderlistResponse.OrderList>Parcellist=new ArrayList<>();
    String str_from_lat="",str_from_long="",str_to_lat="",str_to_long="";
    Rejectinterface rejectinterface;
     int checkedPosition = 0;
    public HubassignorderlistAdapter(Context activity, List<HuborderlistResponse.OrderList> orderList) {
        this.context=activity;
        this.Parcellist=orderList;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(context).inflate(R.layout.parcelassignllist_row,viewGroup,false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
            viewHolder.orderid.setText("Order Id :"+Parcellist.get(i).getOrderId());
            viewHolder.address.setText("Address\n"+Parcellist.get(i).getAddress());
            viewHolder.orderscount.setText("No.of.Items : "+Parcellist.get(i).getProductQuantity());
            viewHolder.amount.setText("Total : "+Parcellist.get(i).getGrandTotal());
           // viewHolder.date.setText(Parcellist.get(i).get);
//            viewHolder.call.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                }
//            });
        viewHolder.bind(Parcellist.get(i));
//            viewHolder.reject.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    rejectinterface.raject(true);
//                }
//            });
    }



    @Override
    public int getItemCount() {
        return Parcellist.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,address,orderscount,orderid,amount,date;
        ImageView call,map;
        TextView reject,accept;
        ImageView selection;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            orderid=itemView.findViewById(R.id.orderid);
            address=itemView.findViewById(R.id.address);
            orderscount=itemView.findViewById(R.id.orderitem);
            amount=itemView.findViewById(R.id.amount);
            date=itemView.findViewById(R.id.date);
            selection=itemView.findViewById(R.id.selection);
        }
           void bind(final HuborderlistResponse.OrderList data) {
               selection.setVisibility(data.isChecked() ? View.VISIBLE : View.GONE);


               itemView.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View view) {
                       data.setChecked(!data.isChecked());
                       selection.setVisibility(data.isChecked() ? View.VISIBLE : View.GONE);
                   }
               });
           }
//            textView.setText(employee.getName())
        }
        public ArrayList<HuborderlistResponse.OrderList> getSelected() {
            ArrayList<HuborderlistResponse.OrderList> selected = new ArrayList<>();
            for (int i = 0; i < Parcellist.size(); i++) {
                if (Parcellist.get(i).isChecked()) {
                    selected.add(Parcellist.get(i));
                }
            }
            return selected;
        }
}
