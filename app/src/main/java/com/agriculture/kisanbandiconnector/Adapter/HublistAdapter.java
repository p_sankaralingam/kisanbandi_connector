package com.agriculture.kisanbandiconnector.Adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.PictureDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.Models.Resbonse.HublistResponse;
import com.agriculture.kisanbandiconnector.ParcellistActivity;
import com.agriculture.kisanbandiconnector.R;
import com.bumptech.glide.RequestBuilder;

import java.util.ArrayList;
import java.util.List;


public class HublistAdapter extends RecyclerView.Adapter<HublistAdapter.ViewHolder> {
    private RequestBuilder<PictureDrawable> requestBuilder;
    Context context;
    List<HublistResponse.DeliveryDetail>Hublist=new ArrayList<>();

    String str_from_lat="",str_from_long="",str_to_lat="",str_to_long="";
    public HublistAdapter(Context applicationContext, List<HublistResponse.DeliveryDetail> languageDetails, String str_from_lat, String str_from_long, String str_to_lat, String str_to_long) {
        this.context=applicationContext;
        this.Hublist=languageDetails;
        this.str_from_lat=str_from_lat;
        this.str_from_long=str_from_long;
        this.str_to_lat=str_to_lat;
        this.str_to_long=str_to_long;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(context).inflate(R.layout.hublist_row,viewGroup,false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
            viewHolder.name.setText("Name :"+Hublist.get(i).getFirstName()+""+Hublist.get(i).getLastName());
            viewHolder.address.setText("Address \n"+Hublist.get(i).getAddress());
            viewHolder.orderscount.setText("No.of.Items:"+Hublist.get(i).getOrderCount());

            viewHolder.call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, ParcellistActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("sellerid",Hublist.get(i).getSellerId());
                    intent.putExtra("fromlat",str_from_lat);
                    intent.putExtra("fromlong",str_from_long);
                    intent.putExtra("tolat",str_to_lat);
                    intent.putExtra("tolong",str_to_long);
                    context.startActivity(intent);
                }
            });
    }
    @Override
    public int getItemCount() {
        return Hublist.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,address,orderscount;

        ImageView call,map;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.hubname);
            address=itemView.findViewById(R.id.address);
            orderscount=itemView.findViewById(R.id.orderscount);
            call=itemView.findViewById(R.id.call);
            map=itemView.findViewById(R.id.location);

        }
    }
}
