package com.agriculture.kisanbandiconnector;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.agriculture.kisanbandiconnector.Adapter.PageAdapter;
import com.agriculture.kisanbandiconnector.Fragments.CompletedOrders;
import com.agriculture.kisanbandiconnector.Fragments.CurrentOrder;
import com.agriculture.kisanbandiconnector.Fragments.RejectedOrders;
import com.google.android.material.tabs.TabLayout;

public class MyorderActivity extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager;
    boolean goback = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myorder);
        viewPager = findViewById(R.id.my_orders_viewpager);
        setupViewPager(viewPager);
        tabLayout = findViewById(R.id.my_orders_tab);
        tabLayout.setupWithViewPager(viewPager);
        goback = getIntent().getBooleanExtra("noback", false);
    }
    private void setupViewPager(ViewPager viewPager) {
        PageAdapter adapter = new PageAdapter(getSupportFragmentManager());
        adapter.addFragment(new CurrentOrder(), "Current");
        adapter.addFragment(new CompletedOrders(), "Delivered");
        adapter.addFragment(new RejectedOrders(), "Canceled");
        viewPager.setAdapter(adapter);
    }
    @Override
    public void onBackPressed() {
        if (goback) {
            Intent intent = new Intent(getApplicationContext(), DeliveryModeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            super.onBackPressed();
        }
    }
}
