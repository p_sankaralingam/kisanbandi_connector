package com.agriculture.kisanbandiconnector;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

public class NogpsActivity extends AppCompatActivity {

    AlertDialog.Builder builder;
    AppCompatButton retry;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nogps);

        builder = new AlertDialog.Builder(this);
        retry=findViewById(R.id.bt_retry);

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                boolean sta = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                //   Toast.makeText(getApplicationContext(),String.valueOf(sta),Toast.LENGTH_SHORT).show();
                if (!sta) {
                    builder.setMessage("Please Turn On GPS")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            });
                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    //Setting the title manually
                    alert.setTitle("No Location Found");
                    alert.show();
                }
                else
                {
                    Intent intent = new Intent(NogpsActivity.this,DeliveryModeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            }
        });
    }
}
