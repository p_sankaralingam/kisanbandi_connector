package com.agriculture.kisanbandiconnector;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.agriculture.kisanbandiconnector.Models.Resbonse.NotificationlistResponse;

import java.util.List;

class NotificationlistAdapter  extends RecyclerView.Adapter<NotificationlistAdapter.ViewHolder> {
    Context context;
    List<NotificationlistResponse.ConnectorNotification>Notificationlist;
    public NotificationlistAdapter(Context applicationContext, List<NotificationlistResponse.ConnectorNotification> notficationlist) {
        this.context=applicationContext;
        this.Notificationlist=notficationlist;
    }
    @NonNull
    @Override
    public NotificationlistAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.notification_row,parent,false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull NotificationlistAdapter.ViewHolder holder, int position) {
        holder.name.setText(String.valueOf(Notificationlist.get(position).getTitle()));
        holder.msg.setText(String.valueOf(Notificationlist.get(position).getContent()));
    }
    @Override
    public int getItemCount() {
        return Notificationlist.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,msg;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            msg=itemView.findViewById(R.id.msg);
        }
    }
}
