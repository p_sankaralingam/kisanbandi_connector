package com.agriculture.kisanbandiconnector.Utils;

import android.app.Application;

import com.agriculture.kisanbandiconnector.BuildConfig;

import timber.log.Timber;

public class FFmpegExample extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
