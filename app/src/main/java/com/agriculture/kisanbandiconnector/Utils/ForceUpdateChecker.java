package com.agriculture.kisanbandiconnector.Utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.agriculture.kisanbandiconnector.Models.Resbonse.VersionResbonse;
import com.agriculture.kisanbandiconnector.Services.Api;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ForceUpdateChecker {
    private static final String TAG = ForceUpdateChecker.class.getSimpleName();
    public static final String KEY_UPDATE_REQUIRED = "force_update";
    public static final String KEY_CURRENT_VERSION = "current_version";
    public static final String KEY_UPDATE_URL = "app_url";
    private OnUpdateNeededListener onUpdateNeededListener;
    private Context context;
    String str_version="";
    Boolean forupdate=false;
    String currentVersion="";
    public interface OnUpdateNeededListener {
        void onUpdateNeeded(String updateUrl, Boolean forupdate);
    }
    public static Builder with(@NonNull Context context) {
        return new Builder(context);
    }
    public ForceUpdateChecker(@NonNull Context context, OnUpdateNeededListener onUpdateNeededListener) {
        this.context = context;
        this.onUpdateNeededListener = onUpdateNeededListener;
    }
    public void check() {
//        final FirebaseRemoteConfig remoteConfig = FirebaseRemoteConfig.getInstance();
//
//        if (remoteConfig.getBoolean(KEY_UPDATE_REQUIRED)) {
        String currentVersion = getappversion();
        final String appVersion = getAppVersion(context);
        final String updateUrl ="https://play.google.com/store/apps/details?id="+context.getPackageName();
        Log.d("currentversion",currentVersion+" another  "+appVersion);
        Log.d("appversion",appVersion);
        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(str_version.length()!=0 && appVersion.length()!=0)
                    if (!TextUtils.equals(str_version, appVersion)
                            && onUpdateNeededListener != null) {
                        onUpdateNeededListener.onUpdateNeeded(updateUrl, forupdate);
                    }
            }
        },1000);
//            if (!TextUtils.equals(currentVersion, appVersion)
//                    && onUpdateNeededListener != null) {
//                onUpdateNeededListener.onUpdateNeeded(updateUrl,forupdate);
//            }
        //}
    }
    public  String getAppVersion(Context context) {
        String result = "";
        try {
            result = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0)
                    .versionName;
            result = result.replaceAll("[a-zA-Z]|-", "");
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }
        Log.d("result",result);
        return result;
    }
    public static class Builder {
        private Context context;
        private OnUpdateNeededListener onUpdateNeededListener;
        public Builder(Context context) {
            this.context = context;
        }
        public Builder onUpdateNeeded(OnUpdateNeededListener onUpdateNeededListener) {
            this.onUpdateNeededListener = onUpdateNeededListener;
            return this;
        }
        public ForceUpdateChecker build() {
            return new ForceUpdateChecker(context, onUpdateNeededListener);
        }
        public ForceUpdateChecker check() {
            ForceUpdateChecker forceUpdateChecker = build();
            forceUpdateChecker.check();
            return forceUpdateChecker;
        }
    }
    public String getappversion()
    {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<VersionResbonse> call=service.getversion();
        call.enqueue(new Callback<VersionResbonse>() {
            @Override
            public void onResponse(Call<VersionResbonse> call, Response<VersionResbonse> response) {
                Log.d("data",response.body().toString());
                str_version=response.body().getState().get(0).getVersion();
//                if(response.body().getVersion().equals("true"))
//                {
//                    forupdate=true;
//                }
//                else
//                {
//                    forupdate=false;
//                }
            }
            @Override
            public void onFailure(Call<VersionResbonse> call, Throwable t) {
            }
        });
        return str_version;
    }
}
