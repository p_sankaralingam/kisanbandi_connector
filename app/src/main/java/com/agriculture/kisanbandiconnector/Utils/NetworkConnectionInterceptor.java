package com.agriculture.kisanbandiconnector.Utils;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.agriculture.kisanbandiconnector.NointernetActivity;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class NetworkConnectionInterceptor implements Interceptor {
 
    private Context mContext;
    public NetworkConnectionInterceptor(Context context) {
        mContext = context;
    }
 
    @Override
    public Response intercept(Chain chain) throws IOException {
        if (!isConnected()) {
                mContext.startActivity(new Intent(mContext, NointernetActivity.class));
        }
        Request.Builder builder = chain.request().newBuilder();
        return chain.proceed(builder.build());
    }
   private boolean isConnected(){
      ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
       NetworkInfo netInfo = null;
       if (connectivityManager != null) {
           netInfo = connectivityManager.getActiveNetworkInfo();
       }
       return (netInfo != null && netInfo.isConnected());
   }
}