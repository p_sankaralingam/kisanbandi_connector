package com.agriculture.kisanbandiconnector.Utils;

public class Constants {
    private static final String BASE_URL = "https://www.kisanbandi.com/test/kisanbandiseller/KisanbandiConnector/";

    public static final int REQUEST_GALLERY_CODE = 3;
    public static final int READ_REQUEST_CODE = 2;
    public static final int FINISH_PARENT_ACTIVITY_CODE = 0;
    public static final int CAMERA_REQUEST = 4;
}
