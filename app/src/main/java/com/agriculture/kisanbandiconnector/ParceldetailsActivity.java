package com.agriculture.kisanbandiconnector;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.agriculture.kisanbandiconnector.Models.Resbonse.DeliveryDetailsResponse;
import com.agriculture.kisanbandiconnector.Services.Api;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ParceldetailsActivity extends AppCompatActivity {

    String str_orderid="",str_name="",str_address="",str_paymentmode="",str_noofitems="",str_total="",str_email="",str_phoneno="";
    TextView orderid,name,address,paymentmode,noofitems,total,mobileno1,emailid;
    TextView location,call;
    String str_type="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parceldetails);
        str_orderid=getIntent().getStringExtra("orderid");
        str_type=getIntent().getStringExtra("type");
        str_name=getIntent().getStringExtra("name");
        str_address=getIntent().getStringExtra("address");
        str_paymentmode=getIntent().getStringExtra("paymentmode");
        str_noofitems=getIntent().getStringExtra("noofitems");
        str_total=getIntent().getStringExtra("total");
        str_email=getIntent().getStringExtra("email");
        str_phoneno=getIntent().getStringExtra("phoneno");
        orderid=findViewById(R.id.orderid);
        name=findViewById(R.id.name);
        address=findViewById(R.id.address);
        paymentmode=findViewById(R.id.paymentmode);
        noofitems=findViewById(R.id.noofitems);
        total=findViewById(R.id.total);
        emailid=findViewById(R.id.mailid);
        mobileno1=findViewById(R.id.mobileno);
        orderid.setText(str_orderid);
        name.setText(str_name);
        address.setText(str_address);
        paymentmode.setText(str_paymentmode);
        noofitems.setText(str_noofitems);
        total.setText(str_total);
        emailid.setText(str_email);
        mobileno1.setText(str_phoneno);
        location=findViewById(R.id.location);
        call=findViewById(R.id.parcelaccept);
        final String latitude=getIntent().getStringExtra("lat");
        final String longitude=getIntent().getStringExtra("long");
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                        Uri.Builder builder = new Uri.Builder();
                        builder.scheme("https")
                                .authority("www.google.com")
                                .appendPath("maps")
                                .appendPath("dir")
                                .appendPath("")
                                .appendQueryParameter("api", "1")
                                .appendQueryParameter("destination", latitude + "," + longitude);
                        String url = builder.build().toString();
                        Log.d("Directions", url);
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                try {
//                    if(mobileno1.getText().length()!=0)
//                    {
//                        Intent intent = new Intent(Intent.ACTION_DIAL);
//                        intent.setData(Uri.parse("tel:" + mobileno1.getText().toString()));
//                        startActivity(intent);
//                    }
//                } catch (Exception e) {
//                }

               Intent intent= new Intent(getApplicationContext(),FinalDeliveryAcceptActivity.class);
                intent.putExtra("orderid",str_orderid);
                startActivity(intent);
            }
        });
    }


    public void parceldetails(String userid) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<DeliveryDetailsResponse> call = service.getrejecteddelivery(userid);
        call.enqueue(new Callback<DeliveryDetailsResponse>() {
            @Override
            public void onResponse(Call<DeliveryDetailsResponse> call, Response<DeliveryDetailsResponse> response) {
                if (response.body().getStatus().equals("success")) {

                }
            }
            @Override
            public void onFailure(Call<DeliveryDetailsResponse> call, Throwable t) {


            }
        });

    }


}
