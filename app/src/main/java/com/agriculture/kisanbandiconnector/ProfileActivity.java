package com.agriculture.kisanbandiconnector;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.agriculture.kisanbandiconnector.Models.Resbonse.ProfiledataResponse;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProfileActivity extends AppCompatActivity {

    EditText connector_id,name,email,mobile,language;
    TextView bank_details;
    Button add_bank_details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        String Connectorid= Sharedpreference.getStringValue(getApplicationContext(),"Connectorid");
        String userid=Sharedpreference.getStringValue(getApplicationContext(),"User_id");
        connector_id=findViewById(R.id.connectorid);
        name=findViewById(R.id.name);
        email=findViewById(R.id.email);
        mobile=findViewById(R.id.mobile);
        language=findViewById(R.id.language);
        bank_details=findViewById(R.id.bank_details);
        add_bank_details=findViewById(R.id.add_bank_details);
        getprofiledata(Connectorid,userid);
        findViewById(R.id.add_bank_details).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),AttachmentActivity.class));
            }
        });
        findViewById(R.id.change_language).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),LanguageSelectionActivity.class);
                intent.putExtra("profile",true);
                startActivity(intent);
            }
        });
    }


    public void getprofiledata(String connectorid, String userid){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<ProfiledataResponse> call=service.getprofiledata(connectorid,userid);
        call.enqueue(new Callback<ProfiledataResponse>() {
            @Override
            public void onResponse(Call<ProfiledataResponse> call, Response<ProfiledataResponse> response) {
                if (response.body().getStatus().equals("success")){
                    connector_id.setText(response.body().getConnectorDetails().get(0).getConnectorId());
                    name.setText(response.body().getConnectorDetails().get(0).getFirstName());
                    email.setText(response.body().getConnectorDetails().get(0).getEmail());
                    mobile.setText(response.body().getConnectorDetails().get(0).getPhonenumber());
                    language.setText("English");
                    String str_mob=response.body().getConnectorDetails().get(0).getAccountNo();
                    if(str_mob.length()!=0)
                    {
                        add_bank_details.setVisibility(View.GONE);
                        String acno=response.body().getConnectorDetails().get(0).getAccountNo();
                        String ifsc=response.body().getConnectorDetails().get(0).getIfscCode();
                        String bank=response.body().getConnectorDetails().get(0).getBankName();
                        bank_details.setText("Account No: "+acno+"\nBank Name :"+bank+"\nIFSC Code : "+ifsc);
                    }else
                    {
                        bank_details.setVisibility(View.GONE);
                        add_bank_details.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ProfiledataResponse> call, Throwable t) {

            }
        });

    }
}
