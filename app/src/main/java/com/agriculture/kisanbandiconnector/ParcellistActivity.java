package com.agriculture.kisanbandiconnector;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.Adapter.ParcellistAdapter;
import com.agriculture.kisanbandiconnector.Interface.Rejectinterface;
import com.agriculture.kisanbandiconnector.Models.Resbonse.ParcelDataResponse;
import com.agriculture.kisanbandiconnector.Services.Api;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ParcellistActivity extends AppCompatActivity implements Rejectinterface {
    String str_from_lat="",str_from_long="",str_to_lat="",str_to_long="",str_seller_id="";
    RecyclerView parcellist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parcellist);
        str_from_lat=getIntent().getStringExtra("fromlat");
        str_from_long=getIntent().getStringExtra("fromlong");
        str_to_lat=getIntent().getStringExtra("tolat");
        str_to_long=getIntent().getStringExtra("tolong");
        str_seller_id=getIntent().getStringExtra("sellerid");
        parcellist=findViewById(R.id.parcellist);
        parcellist.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        getallparcellist(str_from_lat,str_from_long,str_to_lat,str_to_long,str_seller_id);
    }
    private void getallparcellist(final String str_from_lat, final String str_from_long, final String str_to_lat, final String str_to_long, String str_vehicle_type) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<ParcelDataResponse> call=service.getparcellist(str_from_lat,str_to_lat,str_from_long,str_to_long,str_vehicle_type);
        call.enqueue(new Callback<ParcelDataResponse>() {
            @Override
            public void onResponse(Call<ParcelDataResponse> call, Response<ParcelDataResponse> response) {
                if (response.body().getStatus().equals("success")){
                    ParcellistAdapter adapter=new ParcellistAdapter(ParcellistActivity.this,response.body().getDeliveryDetails(),ParcellistActivity.this,null);
                    parcellist.setAdapter(adapter);
                }
            }
            @Override
            public void onFailure(Call<ParcelDataResponse> call, Throwable t) {
            }
        });
    }

    @Override
    public void raject(Boolean reject) {
//        if(reject)
//        {
            Toast.makeText(getApplicationContext(),"removed",Toast.LENGTH_LONG).show();
              //  ParcellistActivity.this.onBackPressed();
        //}
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
