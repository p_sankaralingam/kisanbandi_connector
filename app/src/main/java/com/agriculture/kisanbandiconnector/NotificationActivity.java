package com.agriculture.kisanbandiconnector;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.Models.Resbonse.NotificationlistResponse;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NotificationActivity extends AppCompatActivity {
    String str_title="",str_message="";
    TextView title,message;
    ArrayList<String>Notficationlist=new ArrayList();
    RecyclerView notiicationlist;
    String Connectorid="";
    LinearLayout nodata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        title=findViewById(R.id.title);
        message=findViewById(R.id.content);
        notiicationlist=findViewById(R.id.recyclerview);
        notiicationlist.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
//        if(getIntent().getExtras()!=null)
//        {
//            str_title=getIntent().getStringExtra("title");
//            str_message=getIntent().getStringExtra("content");
//            title.setText(str_title);
//            messa ge.setText(str_message);
//        }
        Connectorid= Sharedpreference.getStringValue(getApplicationContext(),"Connectorid");
        nodata=findViewById(R.id.nodata);
        getNotficationlist(Connectorid);
    }
    private void getNotficationlist(String user_id) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<NotificationlistResponse> call=service.getnotificationlist(user_id);
        call.enqueue(new Callback<NotificationlistResponse>() {
            @Override
            public void onResponse(Call<NotificationlistResponse> call, Response<NotificationlistResponse> response) {
                if (response.body().getStatus().equals("success")){
                        NotificationlistAdapter adapter=new NotificationlistAdapter(getApplicationContext(),response.body().getConnectorNotification());
                        notiicationlist.setAdapter(adapter);
                }
                else
                {
                    if(response.body().getConnectorNotification().size()==0)
                    {
                        notiicationlist.setVisibility(View.GONE);
                        nodata.setVisibility(View.VISIBLE);
                    }
                }
            }
            @Override
            public void onFailure(Call<NotificationlistResponse> call, Throwable t) {
            }
        });
    }
}
