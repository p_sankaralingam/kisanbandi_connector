package com.agriculture.kisanbandiconnector;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.agriculture.kisanbandiconnector.Utils.ConnectivityReceiver;

public class NointernetActivity extends AppCompatActivity {
    AppCompatButton retry;
    AlertDialog.Builder builder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nointernet);

        builder = new AlertDialog.Builder(this);
        retry=findViewById(R.id.bt_retry);

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final boolean isConnected= ConnectivityReceiver.isConnected();
                if (!isConnected) {
                    //Setting message manually and performing action on button click
                    builder.setMessage("Please Connect the Internet")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                                    startActivity(intent);
                                }
                            });
                    //Creating dialog box
                    AlertDialog alert = builder.create();
                    //Setting the title manually
                    alert.setTitle("No Internet Connection");
                    alert.show();
                }
                else
                {
                    Intent intent = new Intent(NointernetActivity.this, DeliveryModeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            }
        });
    }
}
