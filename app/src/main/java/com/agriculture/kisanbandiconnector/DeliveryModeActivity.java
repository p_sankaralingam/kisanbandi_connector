package com.agriculture.kisanbandiconnector;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import com.agriculture.kisanbandiconnector.Models.Resbonse.DashboardcountResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.StatusResponse;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.ConnectivityReceiver;
import com.agriculture.kisanbandiconnector.Utils.ContextUtils;
import com.agriculture.kisanbandiconnector.Utils.ForceUpdateChecker;
import com.agriculture.kisanbandiconnector.Utils.LocaleManager;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DeliveryModeActivity extends AppCompatActivity implements ForceUpdateChecker.OnUpdateNeededListener {
    LinearLayout location_layout, from_to_layout, kelometer_layout;
    SwitchCompat delivery_mode;
    EditText from, to, from1, to1;
    TextView txt1, txt2;
    RadioGroup location_group;
    RadioButton any_radio, particular_radio, radioButton;
    String Location_type;
    Button submit;
    RadioGroup vehiclegroup1, vehiclegroup2;
    RadioGroup vehicle_group1, vehicle_group2;
    RadioButton vehicle_radio;
    String str_from_lat = "";
    String str_from_long = "";
    String str_to_lat = "";
    String str_to_long = "";
    String str_vehicle_type = "";
    LatLng fromlatlong;
    LatLng tolatlong;
    boolean isChecking = false;
    int mCheckedId;
    String user_id = "";
    String device_id;
    String Connectorid = "";
    ProgressDialog dialog, deliverydialog;
    SwitchCompat delivery_mode_switch;
    TextView completedorders,currentorders;

    @Override
    protected void attachBaseContext(Context newBase) {
            String lan=Sharedpreference.getLanguageStringValue(newBase,"languagename");


        Locale localeToSwitchTo = new Locale(lan);
        ContextWrapper localeUpdatedContext = ContextUtils.updateLocale(newBase, localeToSwitchTo);
        super.attachBaseContext(localeUpdatedContext);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deliverymode);
        if (!isLocationEnabled()) {
            startActivity(new Intent(getApplicationContext(), NogpsActivity.class));
        }
        from = findViewById(R.id.from_location);
        to = findViewById(R.id.to_location);
        txt1 = findViewById(R.id.text_view1);
        txt2 = findViewById(R.id.text_view2);
        location_group = findViewById(R.id.location_group);
        any_radio = findViewById(R.id.any);
        particular_radio = findViewById(R.id.particular);
        from_to_layout = findViewById(R.id.from_to_layout);
        kelometer_layout = findViewById(R.id.kelometer_layout);
        from1 = findViewById(R.id.from_location1);
        to1 = findViewById(R.id.to_location1);
        vehicle_group1 = findViewById(R.id.vehicle_group1);
//        delivery_mode_switch = findViewById(R.id.delivery_mode);
        currentorders=findViewById(R.id.currentorders);
        completedorders=findViewById(R.id.completedorders);
//        delivery_mode_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
////                if(b)
////                {
//                Toast.makeText(getApplicationContext(), "Connector Online", Toast.LENGTH_SHORT).show();
//                //}
//            }
//        });
        vehicle_group2 = findViewById(R.id.vehicle_group2);
        vehicle_group1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId != -1 && isChecking) {
                    isChecking = false;
                    vehicle_group2.clearCheck();
                    mCheckedId = checkedId;
                }
                isChecking = true;
            }
        });
        vehicle_group2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId != -1 && isChecking) {
                    isChecking = false;
                    vehicle_group1.clearCheck();
                    mCheckedId = checkedId;
                }
                isChecking = true;
            }
        });
        user_id = Sharedpreference.getStringValue(getApplicationContext(), "User_id");
        Connectorid = Sharedpreference.getStringValue(getApplicationContext(), "Connectorid");
        boolean tokenupdate = Sharedpreference.getBoolen(getApplicationContext(), "updatetoken");
        String token = Sharedpreference.getStringValue(getApplicationContext(), "token");
        device_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        if (tokenupdate) {
            updateFCMToken(token, user_id, "KBC", device_id);
        }
        ForceUpdateChecker.with(DeliveryModeActivity.this).onUpdateNeeded(this).check();
        submit = findViewById(R.id.getdeliverylist);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (from.getText().length() == 0) {
                    from.setError("Select From Location");
                } else if (to.getText().length() == 0) {
                    to.setError("Select To Location");
                } else {
                    str_from_lat = String.valueOf(fromlatlong.latitude);
                    str_from_long = String.valueOf(fromlatlong.longitude);
                    str_to_lat = String.valueOf(tolatlong.latitude);
                    str_to_long = String.valueOf(tolatlong.longitude);
                    str_vehicle_type = "Two wheeler";
                    Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                    intent.putExtra("fromlat", str_from_lat);
                    intent.putExtra("fromlong", str_from_long);
                    intent.putExtra("tolat", str_to_lat);
                    intent.putExtra("tolong", str_to_long);
                    intent.putExtra("vehicletype", str_vehicle_type);
                    startActivity(intent);
                }
            }
        });
        location_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                radioButton = (RadioButton) location_group.findViewById(location_group.getCheckedRadioButtonId());
                switch (checkedId) {
                    case R.id.any:
                        radioButton.setChecked(true);
                        Location_type = "Any";
                        kelometer_layout.setVisibility(View.VISIBLE);
                        from_to_layout.setVisibility(View.GONE);
                        break;
                    case R.id.particular:
                        radioButton.setChecked(true);
                        Location_type = "Particular";
                        kelometer_layout.setVisibility(View.GONE);
                        from_to_layout.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });
        Places.initialize(getApplicationContext(), getString(R.string.apikey));
        location_layout = findViewById(R.id.location_layout);
        delivery_mode = findViewById(R.id.delivery_mode);
        delivery_mode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Deliverystatus("1");
                    location_layout.setVisibility(View.VISIBLE);
                } else {
                    Deliverystatus("0");
                    location_layout.setVisibility(View.GONE);
                }
            }
        });
        from.setFocusable(false);
        from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Place.Field> fieldList = Arrays.asList(Place.Field.ADDRESS, Place.Field.LAT_LNG, Place.Field.NAME);
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fieldList).build(DeliveryModeActivity.this);
                startActivityForResult(intent, 100);
            }
        });
        to.setFocusable(false);
        to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Place.Field> fieldList = Arrays.asList(Place.Field.ADDRESS, Place.Field.LAT_LNG, Place.Field.NAME);
                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fieldList).build(DeliveryModeActivity.this);
                startActivityForResult(intent, 200);
            }
        });
        Counts();
//        from1.setFocusable(false);
//        from1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                List<Place.Field> fieldList= Arrays.asList(Place.Field.ADDRESS,Place.Field.LAT_LNG,Place.Field.NAME);
//
//                Intent intent=new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,fieldList).build(DeliveryModeActivity.this);
//                startActivityForResult(intent,100);
//            }
//        });
    }

    @Override
    public void onBackPressed() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(this);
        alertDialog.setTitle("Confirm Exit..!!!");
        alertDialog.setMessage("Are you sure want to exit");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        android.app.AlertDialog alertDialog1 = alertDialog.create();
        alertDialog1.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK) {
            Place place = Autocomplete.getPlaceFromIntent(data);
            from.setText(place.getAddress());
            // to.setText(place.getAddress());
            // from1.setText(place.getAddress());
            txt1.setText(String.format("Locality Name : %s ", place.getName()));
            txt2.setText(String.valueOf(place.getLatLng()));
            fromlatlong = place.getLatLng();
        } else if (requestCode == 200 && resultCode == RESULT_OK) {
            Place place = Autocomplete.getPlaceFromIntent(data);
            to.setText(place.getAddress());
            tolatlong = place.getLatLng();
        } else if (requestCode == AutocompleteActivity.RESULT_ERROR) {
            Status status = Autocomplete.getStatusFromIntent(data);
            Toast.makeText(getApplicationContext(), status.getStatusMessage(), Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onUpdateNeeded(final String updateUrl, final Boolean forupdate) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please, update app to new version get new features.")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (forupdate) {
                                    finish();
                                } else {
                                    dialog.dismiss();
                                }
                            }
                        }).create();
        dialog.show();
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.dashboard, menu);
        Boolean login = Sharedpreference.getBoolen(getApplicationContext(), "Login");
        MenuItem item1 = menu.findItem(R.id.login);
        MenuItem item2 = menu.findItem(R.id.logout);
        if (login) {
            item1.setVisible(false);
            item2.setVisible(true);
        } else {
            item1.setVisible(true);
            item2.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.profile) {
            startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
            return true;
        } else if (item.getItemId() == R.id.scan) {

            startActivity(new Intent(getApplicationContext(), QrscancodeActivity.class));
            return true;
        } else if (item.getItemId() == R.id.logout) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DeliveryModeActivity.this);
            alertDialogBuilder.setMessage("Are you sure want to logout?");
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setPositiveButton("yes",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            Sharedpreference.ClearPreference(getApplicationContext());
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    });
            alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } else if (item.getItemId() == R.id.about) {
            String HTTPS = "https://";
            final String HTTP = "http://";
            String url = "https://www.kisanbandi.com/About_us";
            if (!url.startsWith(HTTP) && !url.startsWith(HTTPS)) {
                url = HTTP + url;
            }
            Intent intent4 = new Intent(getApplicationContext(), WebviewActivity.class);
            intent4.putExtra("url", url);
            startActivity(intent4);// Choose browser is arbitrary :)
        } else if (item.getItemId() == R.id.gallery) {
            String HTTPS = "https://";
            String HTTP = "http://";
            String url = "https://www.kisanbandi.com/Gallery";
            if (!url.startsWith(HTTP) && !url.startsWith(HTTPS)) {
                url = HTTP + url;
            }
            Intent intent4 = new Intent(getApplicationContext(), WebviewActivity.class);
            intent4.putExtra("url", url);
            startActivity(intent4);// Choose browser is arbitrary :)
        } else if (item.getItemId() == R.id.hubrequest) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DeliveryModeActivity.this);
            alertDialogBuilder.setMessage("Are you sure for  Requesting to Hub?");
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setPositiveButton("yes",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            Hubrequest();
                        }
                    });
            alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } else if (item.getItemId() == R.id.my_orders) {
            startActivity(new Intent(getApplicationContext(), MyorderActivity.class));
        }
        else if(item.getItemId()==R.id.notification)
        {
            startActivity(new Intent(getApplicationContext(),NotificationActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateFCMToken(String token, String session_id, String kbb, String s) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<StatusResponse> call = service.updateFcmToken(session_id, token, kbb, s);
        call.enqueue(new Callback<StatusResponse>() {
            @Override
            public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                if (response.body().getStatus().equals("success")) {

                }
            }
            @Override
            public void onFailure(Call<StatusResponse> call, Throwable t) {

            }
        });
    }

    private void Hubrequest() {
        dialog = new ProgressDialog(DeliveryModeActivity.this);
        dialog.setMessage("Loading....");
        dialog.setCancelable(false);
        dialog.show();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<StatusResponse> call = service.requestconnectortohub(Connectorid, user_id);
        call.enqueue(new Callback<StatusResponse>() {
            @Override
            public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                dialog.dismiss();
                if (response.body().getStatus().equals("success")) {


                }
            }

            @Override
            public void onFailure(Call<StatusResponse> call, Throwable t) {

            }
        });
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean connected = ConnectivityReceiver.isConnected();
        if (!connected) {
            startActivity(new Intent(getApplicationContext(), NointernetActivity.class));
        }
    }


    private void Deliverystatus(String status) {
        deliverydialog = new ProgressDialog(DeliveryModeActivity.this);
        deliverydialog.setMessage("Loading....");
        deliverydialog.setCancelable(false);
        deliverydialog.show();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<StatusResponse> call = service.onlinemode(user_id,status);
        call.enqueue(new Callback<StatusResponse>() {
            @Override
            public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                deliverydialog.dismiss();
                if (response.body().getStatus().equals("success")) {
                    Log.d("deliverysuccess","deliverystatus");
                }
            }
            @Override
            public void onFailure(Call<StatusResponse> call, Throwable t) {
            }
        });
    }
    private void Counts() {
        dialog = new ProgressDialog(DeliveryModeActivity.this);
        dialog.setMessage("Loading....");
        dialog.setCancelable(false);
        dialog.show();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<DashboardcountResponse> call = service.getdahboardcount(user_id);
        call.enqueue(new Callback<DashboardcountResponse>() {
            @Override
            public void onResponse(Call<DashboardcountResponse> call, Response<DashboardcountResponse> response) {
                dialog.dismiss();
                assert response.body() != null;
                if (response.body().getStatus().equals("success")) {
                    currentorders.setText(String.valueOf(response.body().getCurrentDeliveryCount()));
                    completedorders.setText(String.valueOf(response.body().getCompletedDeliveryCount()));
                    if(response.body().getLogin_status().equalsIgnoreCase("1"))
                    {
                        delivery_mode.setChecked(true);
                        location_layout.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        delivery_mode.setChecked(false);
                        location_layout.setVisibility(View.GONE);
                    }
                }
            }
            @Override
            public void onFailure(Call<DashboardcountResponse> call, Throwable t) {
            }
        });
    }
    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        Locale locale = new Locale(LocaleManager.getLanguagePref(this));
        Locale.setDefault(locale);
        overrideConfiguration.setLocale(locale);
        super.applyOverrideConfiguration(overrideConfiguration);
    }



}
