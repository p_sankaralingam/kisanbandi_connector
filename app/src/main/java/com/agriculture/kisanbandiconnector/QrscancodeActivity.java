package com.agriculture.kisanbandiconnector;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import static com.google.android.gms.common.util.CollectionUtils.listOf;
class QrscancodeActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    ZXingScannerView qrCodeScanner1;
//    CustomAlertDialog progressDialog,progressDialog1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrscancode);
        qrCodeScanner1 = findViewById(R.id.qrCodeScanner);
        setScannerProperties();
    }
    private void setScannerProperties() {
        qrCodeScanner1.setFormats(listOf(BarcodeFormat.QR_CODE));
        qrCodeScanner1.setAutoFocus(true);
        qrCodeScanner1.setLaserColor(R.color.colorAccent);
        qrCodeScanner1.setMaskColor(R.color.colorAccent);
        qrCodeScanner1.setAspectTolerance(0.5f);
    }
    @Override
    public void handleResult(final Result rawResult) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (rawResult.getText().length() == 0) {
                    Toast.makeText(QrscancodeActivity.this, "Scanned Qr code Wrong", Toast.LENGTH_SHORT).show();
                    qrCodeScanner1.stopCamera();
                } else {
//                    updateqrscan(rawResult.getText().toString());
                    ViewGroup viewGroup = findViewById(android.R.id.content);
                    View dialogView = LayoutInflater.from(QrscancodeActivity.this).inflate(R.layout.alert_completing_payment, viewGroup, false);
                    Button submit = (Button) dialogView.findViewById(R.id.ok);
                    ImageView close = (ImageView) dialogView.findViewById(R.id.close_alert);
                    TextView invoicenumber=(TextView)dialogView.findViewById(R.id.invoicenumber);
                    String number=rawResult.getText().toString();
                    invoicenumber.setText("Invoice Number " +number);
                    AlertDialog.Builder builder = new AlertDialog.Builder(QrscancodeActivity.this);
                    builder.setView(dialogView);
                    final AlertDialog alertDialog = builder.create();
                    alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    alertDialog.show();
                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Intent intent=new Intent(getApplicationContext(),DeliveryModeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            alertDialog.dismiss();
                        }
                    });
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent=new Intent(getApplicationContext(),DeliveryModeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            alertDialog.dismiss();
                        }
                    });
                }
            }
        }, 1000);
    }

    @Override
    public void onResume() {
        super.onResume();
        qrCodeScanner1.setResultHandler(this);
        qrCodeScanner1.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        qrCodeScanner1.stopCamera();
    }
}
