package com.agriculture.kisanbandiconnector.Services;

import com.agriculture.kisanbandiconnector.Models.Resbonse.AttachmentsResbonse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.BuyerlistResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.CategoryResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.ConnectorlistResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.DashboardcountResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.DeliveryDetailsResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.DistrictResbonse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.EmailMobileResbonse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.ForgetPasswordResbonse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.HoblyResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.HubDashboardResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.HublistResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.HuborderlistResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.ImageuploadResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.LangaugeResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.LoginResbonse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.MeasurementResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.NotificationlistResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.ParcelDataResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.ProductlistResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.ProfiledataResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.RegisterResbonse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.SelleridResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.SellerlistResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.StateResbonse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.StatusResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.SubcategoryResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.TalukResbonse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.VersionResbonse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.VideoDocumentResbonse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Api {
   // String BASE_URL = "https://www.kisanbandi.com/test/kisanbandiseller/KisanbandiConnector/";

        String BASE_URL = "https://www.kisanbandi.com/kisanbandiseller/KisanbandiConnector/";
    @GET("getstatedetail")
    Call<StateResbonse> getstate();

    @POST("getdistricts")
    @FormUrlEncoded
    Call<DistrictResbonse> getdistricts(@Field("state_id") String state_id);

    @Multipart
    @POST("updatevideo")
    Call<VideoDocumentResbonse> getVideo(@Part("user_id") RequestBody user_id,
                                         @Part("connector_id") RequestBody connector_id,
                                         @Part MultipartBody.Part video);
    @Multipart
    @POST("updatedocument")
    Call<VideoDocumentResbonse> getImage(@Part("user_id") RequestBody user_id,
                                         @Part("connector_id") RequestBody connector_id,
                                         @Part MultipartBody.Part image);

    @POST("gettaluks")
    @FormUrlEncoded
    Call<TalukResbonse> gettaluk(@Field("state_id") String state_id,
                                 @Field("district_id") String district_id);
    /*@Multipart
    @POST("Imageupload")
    Call<DocVideoResbonse> getDocVideo(@Part MultipartBody.Part image,
                                       @Part MultipartBody.Part video);
    */
   /* @Multipart
    @POST("InsertDocfile")
    Call<DocumentResbonse> getDocument(@Part MultipartBody.Part image);*/
    @Multipart
    @POST("InsertConnectorRegistration")
    Call<RegisterResbonse> getregister(@Part("first_name") RequestBody first_name,
                                       @Part("last_name") RequestBody last_name,
                                       @Part("email") RequestBody email,
                                       @Part("phonenumber") RequestBody phonenumber,
                                       @Part("pancard_no") RequestBody aadhar_no,
                                       @Part("address") RequestBody address,
                                       @Part("street_name") RequestBody street,
                                       @Part("building_name") RequestBody building_name,
                                       @Part("landmark") RequestBody landmark,
                                       @Part("state") RequestBody state,
                                       @Part("district") RequestBody district,
                                       @Part("taluk") RequestBody taluk,
                                       @Part("pincode") RequestBody pincode,
                                       @Part("country") RequestBody country,
                                       @Part("device_id") RequestBody device_id,
                                       @Part("vehicle_type") RequestBody vehicle_type,
                                       @Part("lat") RequestBody lat,
                                       @Part("lang") RequestBody lang,
                                       @Part("account_no") RequestBody account_no,
                                       @Part("ifsc_code") RequestBody ifsc_code,
                                       @Part("bank_name") RequestBody bank_name,
                                       @Part("app_version") RequestBody appversion,
                                       @Part("state_name") RequestBody state_name,
                                       @Part("district_name") RequestBody district_name,
                                       @Part("taluk_name") RequestBody taluk_name,
                                       @Part("hobli_id") RequestBody hobli_id);

    @POST("checkuserlogin")
    @FormUrlEncoded
    Call<LoginResbonse> getlogin(@Field("user_id") String user_id,
                                 @Field("password") String password,
                                 @Field("device_id") String device_id,
                                 @Field("user_type") String user_type);

    @GET("getversion_control")
    Call<VersionResbonse> getversion();


    @POST("connectorFcmToken")
    @FormUrlEncoded
    Call<StatusResponse> updateFcmToken(@Field("user_id") String user_id,
                                        @Field("fcm_token") String token,
                                        @Field("type") String type,
                                        @Field("device_id") String device_id);
    @POST("checkemailavaliable")
    @FormUrlEncoded
    Call<EmailMobileResbonse> getemail(@Field("email") String email);

    @POST("checkmobileavaliable")
    @FormUrlEncoded
    Call<EmailMobileResbonse> getmobile(@Field("mobile") String mobile);

    @POST("forgtepassword")
    @FormUrlEncoded
    Call<ForgetPasswordResbonse> getPassword(@Field("email") String email);

    @Multipart
    @POST("updateAttachementDetails")
    Call<AttachmentsResbonse> getAttachments(@Part("user_id") RequestBody user_id,
                                             @Part MultipartBody.Part image,
                                             @Part MultipartBody.Part video,
                                             @Part("account_no") RequestBody account_no,
                                             @Part("ifsc_code") RequestBody ifsc_code,
                                             @Part("bank_name") RequestBody bankname);
    @Headers({"Accept: application/json"})
    @GET("getLanguages")
    Call<LangaugeResponse> getalllang();

    @POST("getDeliverySellerHubList")
    @FormUrlEncoded
    Call<HublistResponse> getallhubdetails(@Field("from_lat") String from_lat,
                                           @Field("to_lat") String to_lat,
                                           @Field("from_long") String from_long,
                                           @Field("to_long") String to_long,
                                           @Field("vehicle_type") String vehicle_type);

    @POST("getDeliveryItems")
    @FormUrlEncoded
    Call<ParcelDataResponse> getparcellist(@Field("from_lat") String from_lat,
                                           @Field("to_lat") String to_lat,
                                           @Field("from_long") String from_long,
                                           @Field("to_long") String to_long,
                                           @Field("seller_id") String sellerid);

    @POST("connectorProfileDetails")
    @FormUrlEncoded
    Call<ProfiledataResponse> getprofiledata(@Field("connector_id") String connector_id,
                                             @Field("user_id") String user_id);

    //    @POST("getHubProductList")
//    @FormUrlEncoded
//    Call<>
    @Multipart
    @POST("addHubProduct")
    Call<StatusResponse> insertproduct(@Part("connector_id") RequestBody connector_id,
                                       @Part("mainc_id") RequestBody mainc_id,
                                       @Part("subc_id") RequestBody subc_id,
                                       @Part("prod_name") RequestBody prod_name,
                                       @Part("measurement_id") RequestBody measurement_id,
                                       @Part("prod_quantity") RequestBody prod_quantity,
                                       @Part("prod_price") RequestBody prod_price,
                                       @Part("prod_commission_price") RequestBody prod_commission_price,
                                       @Part("grade") RequestBody grade,
                                       @Part("prod_detail") RequestBody prod_detail,
                                       @Part("seller_id") Integer seller_id,
                                       @Part MultipartBody.Part image);
    @GET("getMainCategory")
    Call<CategoryResponse> getcategoruydetails();

    @POST("getSubCategory")
    @FormUrlEncoded
    Call<SubcategoryResponse> getsubcategory(@Field("mainc_id") String mainc_id);

    @POST("getMeasurement")
    @FormUrlEncoded
    Call<MeasurementResponse> getmeasurement(@Field("connector_id") String connectorid);

    @POST("connectorToHubRequest")
    @FormUrlEncoded
    Call<StatusResponse> requestconnectortohub(@Field("connector_id") String connector_id,
                                               @Field("user_id") String user_id);

    @POST("getHubProductList")
    @FormUrlEncoded
    Call<ProductlistResponse> getproductdetails(@Field("connector_id") String connector_id);

    @POST("getConnectorList")
    @FormUrlEncoded
    Call<ConnectorlistResponse> getconnectorslist(@Field("connector_id") String Connector_id);

    @POST("getHubOrderList")
    @FormUrlEncoded
    Call<HuborderlistResponse>gethuborderlist(@Field("connector_id") String connector_id);

    @POST("gethobly")
    @FormUrlEncoded
    Call<HoblyResponse> gethobly(@Field("auto_id") String auto_id);

    @POST("getSellerList")
    @FormUrlEncoded
    Call<SelleridResponse> getSellerid(@Field("connector_id") String connector_id);

    @POST("deleteHubProduct")
    @FormUrlEncoded
    Call<StatusResponse> deletehubproduct(@Field("connector_id") String connector_id,
                                          @Field("prod_id") String pro_id);

    @POST("getPendingDelivery")
    @FormUrlEncoded
    Call<DeliveryDetailsResponse> getcurrentdelivery(@Field("user_id") String user_id);

    @POST("getCompletedDelivery")
    @FormUrlEncoded
    Call<DeliveryDetailsResponse>getcompleteddelivery(@Field("user_id")String user_id);

    @POST("getRejectedDelivery")
    @FormUrlEncoded
    Call<DeliveryDetailsResponse>getrejecteddelivery(@Field("user_id")String user_id);

     @Multipart
    @POST("parcelConfirm")
    Call<StatusResponse> finaldeliveryupload(@Part("connector_user_id") RequestBody connectoid,
                                             @Part MultipartBody.Part prod_imageurl,
                                             @Part("order_date") RequestBody date,
                                             @Part("order_time") RequestBody time,
                                             @Part("order_id") RequestBody order_id);
    @POST("connectorloginstatus")
    @FormUrlEncoded
    Call<StatusResponse> onlinemode(@Field("user_id") String userid,
                                    @Field("login_status")String login_status);

    @POST("connectorDevliveryCounts")
    @FormUrlEncoded
    Call<DashboardcountResponse> getdahboardcount(@Field("user_id") String user_id);

    @POST("editHubProduct")
    @FormUrlEncoded
    Call<StatusResponse> edithubproduct(@Field("prod_id") String prod_id,
                                        @Field("seller_id") String seller_id,
                                        @Field("mainc_id") String mainc_id,
                                        @Field("subc_id") String subc_id,
                                        @Field("prod_name") String prod_name,
                                        @Field("prod_detail") String prod_detail,
                                        @Field("prod_price") String prod_price,
                                        @Field("prod_commission_price") String prod_commission_price,
                                        @Field("measurement_id") String measurement_id,
                                        @Field("prod_quantity") String prod_quantity,
                                        @Field("prod_imageurl") String prod_imageurl,
                                        @Field("grade") String grade);
    @POST("productImageUrl")
    @FormUrlEncoded
    Call<ImageuploadResponse> imageupload(@Part MultipartBody.Part prod_imageurl);

    @POST("sellerlist")
    @FormUrlEncoded
    Call<SellerlistResponse>getsellerlist(@Field("connector_id")String user_id);

    @POST("buyerlist")
    @FormUrlEncoded
    Call<BuyerlistResponse>getbuyerlist(@Field("connector_id")String user_id);

    @POST("getConnectorNotification")
    @FormUrlEncoded
    Call<NotificationlistResponse>getnotificationlist(@Field("connector_id")String user_id);

    @POST("parcelAssignKbc")
    @FormUrlEncoded
    Call<StatusResponse>addassignparcel(@Field("order_id")String order_id,
                                        @Field("connector_id")String connector_id,
                                        @Field("connector_user_id")String connector_user_id,
                                        @Field("connector_hub_id")String connector_hub_id );
    @POST("getHubDashboardCount")
    @FormUrlEncoded
    Call<HubDashboardResponse>getdashboardcount(@Field("connector_id")String connector_id);
}
