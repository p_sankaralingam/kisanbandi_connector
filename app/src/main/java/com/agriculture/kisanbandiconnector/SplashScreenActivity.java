package com.agriculture.kisanbandiconnector;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.agriculture.kisanbandiconnector.Hub.HubdashboardActivity;
import com.agriculture.kisanbandiconnector.Models.Resbonse.LoginResbonse;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.ConnectivityReceiver;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SplashScreenActivity extends AppCompatActivity {
    private static final String TAG =SplashScreenActivity.class.getSimpleName() ;
    /* Boolean login=false;
         Boolean register=false;
         Boolean attach=false;*/
    String Device_id = "", User_type = "", User_id, Password;
    boolean language=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        FirebaseMessaging.getInstance().subscribeToTopic("all");
        Device_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        User_type="KBC";
        User_id= Sharedpreference.getStringValue(SplashScreenActivity.this,"User_id");
        Password=Sharedpreference.getStringValue(SplashScreenActivity.this,"Password");
 /*       SharedPreferences sharedPreferences=getSharedPreferences("Log", Context.MODE_PRIVATE);
        login = sharedPreferences.getBoolean("Login", false);

        SharedPreferences sharedPreferences1=getSharedPreferences("Data", Context.MODE_PRIVATE);
        register = sharedPreferences1.getBoolean("Register", false);

        SharedPreferences sharedPreferences2=getSharedPreferences("Attach", Context.MODE_PRIVATE);
        attach = sharedPreferences2.getBoolean("Attachment", false);*/

        /*final boolean register= Sharedpreference.getBoolen(SplashScreenActivity.this,"Register");*/
        final boolean login = Sharedpreference.getBoolen(SplashScreenActivity.this, "Login");
        final boolean attach = Sharedpreference.getBoolen(SplashScreenActivity.this, "Attachment");
        final boolean approve = Sharedpreference.getBoolen(SplashScreenActivity.this, "Approval");
        final boolean success = Sharedpreference.getBoolen(SplashScreenActivity.this, "Success");
      //  final boolean language= Sharedpreference.getBoolen(SplashScreenActivity.this,"language");



        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        // Log and toast
                        Log.d("fcmtoken",token);
                    }
                });


//        if (isConnected()){
//        }
//        else {
//            AlertDialog.Builder builder =new AlertDialog.Builder(SplashScreenActivity.this);
//            builder.setTitle("No Internet Connection");
//            builder.setIcon(R.drawable.ic_no_internet);
//            builder.setMessage("You need to have mobile data or wifi");
//            builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    finish();
//                }
//            });
//            builder.show();
//        }
      /*  Log.d("toastdata", String.valueOf(attach));

        Log.d("toastdata", String.valueOf(approve));

        Log.d("toastdata", String.valueOf(login));*/

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (login) {
                    newlogin(User_id,Password,Device_id,User_type);
                   /* if (attach){
                        if (approve){
                            Intent intent=new Intent(SplashScreenActivity.this, ApprovalActivity.class);
                            startActivity(intent);
                        }else {
                            Intent intent1=new Intent(SplashScreenActivity.this, DeliveryModeActivity.class);
                            startActivity(intent1);
                        }
                    }else {
                        Intent intent=new Intent(SplashScreenActivity.this, AttachmentActivity.class);
                        startActivity(intent);
                    }*/
                } else {
                    if (language) {
                        Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                        startActivity(intent);
                    } else
                    {
                        Intent intent = new Intent(SplashScreenActivity.this, LanguageSelectionActivity.class);
                        intent.putExtra("profile",false);
                        startActivity(intent);
                    }
                }
            }

        }, 2000);
    }

    public void newlogin(final String User_id, String user_password, String device_id, String user_type) {
        final ProgressDialog progressDialog1 = new ProgressDialog(SplashScreenActivity.this);
        progressDialog1.setCancelable(false);
        progressDialog1.setMessage("Loading please wait...");
        progressDialog1.show();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);

        Call<LoginResbonse> call = service.getlogin(User_id, user_password, device_id, user_type);
        call.enqueue(new Callback<LoginResbonse>() {
            @Override
            public void onResponse(Call<LoginResbonse> call, Response<LoginResbonse> response) {
                progressDialog1.dismiss();
                if (response.body().getStatus().equals("success")) {
                    if (response.body().getConnectorstatus().equals("attachement")) {
                        Intent intent = new Intent(SplashScreenActivity.this, DeliveryModeActivity.class);
                        startActivity(intent);
                    } else if (response.body().getConnectorstatus().equals("approval")) {
                        Intent intent = new Intent(SplashScreenActivity.this, ApprovalActivity.class);
                        startActivity(intent);
                    } else if (response.body().getConnectorstatus().equals("success")) {

                        if(response.body().getHub_status().equalsIgnoreCase("1"))
                        {
                            Intent intent=new Intent(SplashScreenActivity.this, HubdashboardActivity.class);
                            Sharedpreference.storeBooleanValue(SplashScreenActivity.this,"Success",true);
                            /*   Sharedpreference.storeBooleanValue(LoginActivity.this,"Approved",true);*/
                            Sharedpreference.storeBooleanValue(SplashScreenActivity.this,"hubstatus",true);
                            startActivity(intent);
                        }
                        else
                        {
                            Intent intent = new Intent(SplashScreenActivity.this, DeliveryModeActivity.class);
                            startActivity(intent);
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResbonse> call, Throwable t) {

            }
        });
    }
    private boolean isConnected(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    @Override
    protected void onResume() {
        super.onResume();
        boolean connected = ConnectivityReceiver.isConnected();
        if (!connected) {
            startActivity(new Intent(getApplicationContext(), NointernetActivity.class));
        }
    }
}
