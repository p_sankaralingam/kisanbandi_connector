package com.agriculture.kisanbandiconnector;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agriculture.kisanbandiconnector.Adapter.LanguageAdapter;
import com.agriculture.kisanbandiconnector.Models.Resbonse.LangaugeResponse;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.LanguageCall;
import com.agriculture.kisanbandiconnector.Utils.LocaleManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LanguageSelectionActivity extends AppCompatActivity implements LanguageCall {

    Boolean profile=false;
    RecyclerView languagelist;
    TextView text1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_selection);
        languagelist=findViewById(R.id.languagelist);
        languagelist.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        profile=getIntent().getBooleanExtra("profile",false);
        getAlllanguages();

        text1=findViewById(R.id.text1);

//        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Sharedpreference.storeBooleanValue(LanguageSelectionActivity.this,"language",true);
//                startActivity(new Intent(getApplicationContext(),MainActivity.class));
//            }
//        });
    }
    public void getAlllanguages(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<LangaugeResponse> call=service.getalllang();
        call.enqueue(new Callback<LangaugeResponse>() {
            @Override
            public void onResponse(Call<LangaugeResponse> call, Response<LangaugeResponse> response) {
                if (response.body().getStatus().equals("success")){
                    LanguageAdapter adapter=new LanguageAdapter(LanguageSelectionActivity.this,response.body().getLanguageDetails(),profile);
                    languagelist.setAdapter(adapter);
            }
            }
            @Override
            public void onFailure(Call<LangaugeResponse> call, Throwable t) {

            }
        });

    }
    private void setNewLocale(AppCompatActivity mContext, @LocaleManager.LocaleDef String language) {
        LocaleManager.setNewLocale(this, language);
        Intent intent = mContext.getIntent();
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }
    @Override
    public void call(boolean dda) {
        if(dda)
        {
            setNewLocale(LanguageSelectionActivity.this, LocaleManager.KANNADA);
        }
    }

}
