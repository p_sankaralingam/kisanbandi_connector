package com.agriculture.kisanbandiconnector;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.agriculture.kisanbandiconnector.Models.Resbonse.DistrictResbonse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.EmailMobileResbonse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.HoblyResponse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.RegisterResbonse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.StateResbonse;
import com.agriculture.kisanbandiconnector.Models.Resbonse.TalukResbonse;
import com.agriculture.kisanbandiconnector.Services.Api;
import com.agriculture.kisanbandiconnector.Utils.Constants;
import com.agriculture.kisanbandiconnector.Utils.CryptoProvider;
import com.agriculture.kisanbandiconnector.Utils.SearchableSpinner;
import com.agriculture.kisanbandiconnector.Utils.Sharedpreference;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks{
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private static final int REQUEST_VIDEO_CAPTURE = 300;
    private static final int READ_REQUEST_CODE = 200;
    private static final int REQUEST_TAKE_GALLERY_VIDEO = 1;
    private VideoView displayRecordedVideo;
   /* FFmpeg ffmpeg;*/
    private Uri fileUri, uri;
    private Button btnRecordVideo, register;
    CheckBox  btn_gps;
    SearchableSpinner district_spinner, state_spinner,taluk_spinner,hobly_spinner;
    ArrayAdapter<String> state_adapter, district_adapter,taluk_adapter,hobly_adapter;
    ImageView attachment;
    ArrayList<String> region = new ArrayList<>();
    ArrayList<String> regoin_code = new ArrayList<>();
    List<StateResbonse.State> stateResponse;
    ArrayList<String> city = new ArrayList<>();
    ArrayList<String> city_id = new ArrayList<>();
    List<DistrictResbonse.District> districtResponse;
    ArrayList<String>taluk=new ArrayList<>();
    ArrayList<String>taluk_id=new ArrayList<>();

    ArrayList<String>hobly=new ArrayList<>();
    ArrayList<String>hobly_id=new ArrayList<>();

    ArrayList<String>taluk_auto_id=new ArrayList<>();
    List<TalukResbonse.Taluk> talukResponse;

    List<HoblyResponse.Hobly>HoblyResponse;

    TextView webview;
    CheckBox agree;
    String filepath1, pathToStoredVideo;
    ImageView kyc;
    String kyc_image = "", mCurrentPhotoPath="", _imageChooseMode, _video;
    RadioGroup vehicle_radio;
    RadioButton radioButton;
    TextView txt;
    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;
    double longitude,latitude;
    TextView location_error,terms_error;

    String Device_id = "", State_id = "", State_name = "", District_name = "",Taluk_name="",Taluk_id="", Video_Url = "", Doc_Url = "", Address = "", Country = "", Landmark = " ",
    Vehicle_type = "", Lat = "", Lang = "",Disstrict_id,versionRelease="",Account_Name="",Account_No="",Account_ifsc="";
    EditText first_name, last_name, mobile, pan_card, email, door_no, street, building, landmark, country, pincode, account_name, account_no, account_ifsc;
    int click = 0;
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();
    CheckBox ch1,ch2,ch3,ch4,ch5;

    private final static int UNIQUE_REQUESR_CODE = 1;
    private final static int UNIQUE_REQUESR_CODE2 = 2;
    LocationManager locationManager;
    private static  final int REQUEST_LOCATION=1;
    private final static int ALL_PERMISSIONS_RESULT = 101;
    public static final int RequestPermissionCode = 7;
    private static final int MY_PERMISSIONS_REQUEST_CODE = 123;
    public boolean check1=false;
    public boolean check2=false;
    private AwesomeValidation awesomeValidation;
    public static final String SEPERATOR =",";
    StringBuilder stringBuilder = new StringBuilder();

    String str_auto_id="";

    LinearLayout hobly_layout;
    String str_hobli_id="";

    boolean hobli_select=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        first_name = findViewById(R.id.connector_firstname);
        last_name = findViewById(R.id.connector_lastname);
        mobile = findViewById(R.id.connector_number);
        pan_card = findViewById(R.id.connector_aadhar);
        email = findViewById(R.id.connector_email);
        kyc = findViewById(R.id.connector_kyc);
        door_no = findViewById(R.id.connector_door_no);
        street = findViewById(R.id.connector_street);
        country = findViewById(R.id.connector_country);
        pincode = findViewById(R.id.connector_pincode);
        building = findViewById(R.id.connector_building);
        state_spinner = findViewById(R.id.state_spinner);
        district_spinner = findViewById(R.id.distrct_spinner);
        taluk_spinner=findViewById(R.id.taluk_spinner);
        hobly_spinner=findViewById(R.id.hobly_spinner);

        register = findViewById(R.id.new_register);
        btnRecordVideo = (Button) findViewById(R.id.btnRecordVideo);
        displayRecordedVideo = findViewById(R.id.video_display);
        webview = findViewById(R.id.web_view);
        account_name = findViewById(R.id.connector_Account_name);
        account_no = findViewById(R.id.connector_Account_number);
        account_ifsc = findViewById(R.id.connector_Account_ifsc);
        landmark = findViewById(R.id.connector_landmark);
        agree = findViewById(R.id.agree_checkbox);
        ch1=findViewById(R.id.ch_two);
        ch2=findViewById(R.id.ch_three);
        ch3=findViewById(R.id.ch_four);
        ch4=findViewById(R.id.ch_tembo);
        ch5=findViewById(R.id.ch_no);
        location_error=findViewById(R.id.location_error);
        terms_error=findViewById(R.id.terms_error);
        btn_gps = findViewById(R.id.enable_gps);
        hobly_layout=findViewById(R.id.hobly_layout);
        agree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    check1=true;
                    terms_error.setVisibility(View.GONE);
                }
                else {
                    check1=false;
                    terms_error.setVisibility(View.VISIBLE);
                }
            }
        });

        btn_gps.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                   check2=true;
                   location_error.setVisibility(View.GONE);
                }
                else {
                    check2=false;
                    location_error.setVisibility(View.VISIBLE);
                }
            }
        });
      /*  Button crashButton = new Button(this);
        crashButton.setText("Crash!");
        crashButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Crashlytics.getInstance().crash(); // Force a crash
            }
        });
        addContentView(crashButton, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));*/
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.connector_email, Patterns.EMAIL_ADDRESS, R.string.emailerror);
        awesomeValidation.addValidation(this, R.id.connector_number, "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[6789]\\d{9}$", R.string.mobileerror);
        awesomeValidation.addValidation(this, R.id.connector_pincode, "^[1-9]{1}[0-9]{2}\\s{0,1}[0-9]{3}$", R.string.pincode);

        try {
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", new CryptoProvider());
        } catch (NoSuchAlgorithmException e) {
        }
       ContextCompat.checkSelfPermission(RegisterActivity.this,Manifest.permission.ACCESS_FINE_LOCATION);
       ContextCompat.checkSelfPermission(RegisterActivity.this,Manifest.permission.READ_EXTERNAL_STORAGE);
        ContextCompat.checkSelfPermission(RegisterActivity.this,Manifest.permission.CAMERA);
            // Do something, when permissions not granted
            if(ActivityCompat.shouldShowRequestPermissionRationale((RegisterActivity.this),Manifest.permission.ACCESS_FINE_LOCATION)
                    || ActivityCompat.shouldShowRequestPermissionRationale((RegisterActivity.this),Manifest.permission.READ_EXTERNAL_STORAGE)||ActivityCompat.shouldShowRequestPermissionRationale((RegisterActivity.this),Manifest.permission.CAMERA)) {
                // If we should give explanation of requested permissions
                // Show an alert dialog here with request explanation
                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                builder.setMessage("Camera,Location and  Write External" + " Storage permissions are required to do the task.");
                builder.setTitle("Please grant those permissions");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ActivityCompat.requestPermissions(
                                RegisterActivity.this,
                                new String[]{
                                        Manifest.permission.ACCESS_FINE_LOCATION,
                                        Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.CAMERA
                                },
                                MY_PERMISSIONS_REQUEST_CODE
                        );
                    }
                });
                builder.setNeutralButton("Cancel",null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }else{
                // Directly request for required permissions, without explanation
                ActivityCompat.requestPermissions(
                       RegisterActivity.this,
                        new String[]{
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_CODE
                );
            }

       /* else {
            // Do something, when permissions are already granted
            Toast.makeText(RegisterActivity.this,"Permissions already granted",Toast.LENGTH_SHORT).show();
        }*/

        btn_gps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationManager=(LocationManager) RegisterActivity.this.getSystemService(Context.LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                {
                    //Write Function To enable gps
                    OnGPS();
                }
                else
                {
                    //GPS is already On then
                    getLocation();
                }
            }
        });
        if (isConnected()){
        }
        else {
            AlertDialog.Builder builder =new AlertDialog.Builder(RegisterActivity.this);
            builder.setTitle("No Internet Connection");
            builder.setIcon(R.drawable.ic_no_internet);
            builder.setMessage("You need to have mobile data or wifi");
            builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            builder.show();
        }
        Device_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        Country = "India";
        versionRelease = Build.VERSION.RELEASE;


        ch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                stringBuilder.append(ch1.getText().toString());
                ch5.setChecked(false);
            }
        });
        ch2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                stringBuilder.append(SEPERATOR + ch2.getText().toString());
                ch5.setChecked(false);
            }
        });
        ch3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                stringBuilder.append(SEPERATOR + ch3.getText().toString());
                ch5.setChecked(false);
            }
        });
        ch4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                stringBuilder.append(SEPERATOR + ch4.getText().toString());
                ch5.setChecked(false);
            }
        });
        ch5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                stringBuilder.append(SEPERATOR + ch5.getText().toString());
                ch1.setChecked(false);
                ch2.setChecked(false);
                ch3.setChecked(false);
                ch4.setChecked(false);
            }
        });

        webview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RegisterActivity.this, WebActivity.class);
                startActivity(intent);
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v == register) {
                    boolean error = false;

                    if (!validfirstName()) {
                        error = true;
                        first_name.setError("Please enter first name");
                    }
                    if (!awesomeValidation.validate()) {
                        error = true;
                    }
                    if (!validdoorno()) {
                        error = true;
                        door_no.setError("Please enter Door no");
                    }
                    if (!validstreet()) {
                        error = true;
                        street.setError("Please enter street");
                    }

                   /* if (!validbankname()) {
                        error = true;
                        account_name.setError("Please Enter account details");
                    }
                    if (!validifsc()) {
                        error = true;
                        account_ifsc.setError("please enter ifsc code");
                    }
                    if (!validbankno()) {
                        error = true;
                        account_no.setError("please enter account no");
                    }*/
                    if (agree.isChecked() == false) {
                        error = true;
                        terms_error.setVisibility(View.VISIBLE);
                    }
                    if (btn_gps.isChecked() == false) {
                        error = true;
                        location_error.setVisibility(View.VISIBLE);
                    }
                    if (!validstate()) {
                        error = true;
                        setSpinnerError(state_spinner, "Please Select state");
                    }
                    if (!validistrict()) {
                        error = true;
                        setSpinnerError(district_spinner, "Please Select district");
                    }
                    if (!valitaluk()) {
                        error = true;
                        setSpinnerError(taluk_spinner, "Please Select taluk");
                    }

                    if(hobli_select) {
                        if (!valihobli()) {
                            error = true;
                            setSpinnerError(hobly_spinner, "Please Select Hobli");
                        }
                    }

                    if (check1 == false) {
                        error = true;
                        terms_error.setVisibility(View.VISIBLE);
                    }
                    if (check2 = false) {
                        error = true;
                        location_error.setVisibility(View.VISIBLE);
                    }
                    if (!error) {
                    newRegister(first_name.getText().toString(), last_name.getText().toString(), email.getText().toString(), mobile.getText().toString(), pan_card.getText().toString(),
                        door_no.getText().toString(), street.getText().toString(), building.getText().toString(), landmark.getText().toString(),
                        State_id, Disstrict_id, Taluk_id, pincode.getText().toString(), Country, Device_id, stringBuilder.toString(), Lat, Lang,
                        Account_Name, Account_No,Account_ifsc, versionRelease,State_name,District_name,Taluk_name);
                    }

                }
            }
        });
        mobile.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){
                    getMobile(mobile.getText().toString());
                }
            }
        });
        email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){
                    getEmail(email.getText().toString());
                }
            }
        });
      /*  mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });*/
      /*  email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                getEmail(email.getText().toString());
            }
        });*/
        getState();
     /*   btnRecordVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayRecordedVideo.setVisibility(View.VISIBLE);
                Intent videoCaptureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                videoCaptureIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT,60);
                if(videoCaptureIntent.resolveActivity(getPackageManager()) != null){
                    startActivityForResult(videoCaptureIntent, REQUEST_VIDEO_CAPTURE);
                    _video="video".toLowerCase();
                }
            }
        });*/

       /* attach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                *//*_imageChooseMode = "image".toLowerCase();
                selectImage();*//*
                Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                openGalleryIntent.setType("image/*");
                startActivityForResult(openGalleryIntent, Constants.REQUEST_GALLERY_CODE);
            }
        });*/

        kyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                openGalleryIntent.setType("image/*");
                startActivityForResult(openGalleryIntent, Constants.REQUEST_GALLERY_CODE);
            }
        });
        if (!isDeviceSupportCamera()) {
            Toast.makeText(getApplicationContext(),"Sorry! Your device doesn't support camera", Toast.LENGTH_LONG).show();
            finish();
        }
    }



    private void setSpinnerError(Spinner spinner, String error){
        View selectedView = spinner.getSelectedView();
        if (selectedView != null && selectedView instanceof TextView) {
            spinner.requestFocus();
            TextView selectedTextView = (TextView) selectedView;
            selectedTextView.setError("error"); // any name of the error will do
            selectedTextView.setTextColor(Color.RED); //text color in which you want your error message to be displayed
            selectedTextView.setText(error); // actual error message
           /* spinner.performClick();*/ // to open the spinner list if error is found.
        }
    }
    public void getMobile(final String mobile){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);

        Call<EmailMobileResbonse> call=service.getmobile(mobile);
        call.enqueue(new Callback<EmailMobileResbonse>() {
            @Override
            public void onResponse(Call<EmailMobileResbonse> call, Response<EmailMobileResbonse> response) {
                if (response.body().getStatuscode().equals(400)){

                }
            }

            @Override
            public void onFailure(Call<EmailMobileResbonse> call, Throwable t) {

            }
        });
    }
    public void getEmail(final String email){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);

        Call<EmailMobileResbonse> call=service.getemail(email);
        call.enqueue(new Callback<EmailMobileResbonse>() {
            @Override
            public void onResponse(Call<EmailMobileResbonse> call, Response<EmailMobileResbonse> response) {
                if (response.body().getStatus().equals(400)){

                }
            }

            @Override
            public void onFailure(Call<EmailMobileResbonse> call, Throwable t) {

            }
        });
    }
    public void getState(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<StateResbonse> call=service.getstate();
        call.enqueue(new Callback<StateResbonse>() {
            @Override
            public void onResponse(Call<StateResbonse> call, Response<StateResbonse> response) {
                region.clear();
                regoin_code.clear();
                stateResponse=response.body().getState();
                for (int i=0;i<stateResponse.size();i++){
                    region.add(stateResponse.get(i).getStateName());
                    regoin_code.add(stateResponse.get(i).getStateId());
                }
                state_adapter=new ArrayAdapter<String>(RegisterActivity.this,R.layout.spinner_item,region);
                state_adapter.setDropDownViewResource(R.layout.spinner_item);
                state_spinner.setAdapter(state_adapter);
                state_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        State_name=region.get(parent.getSelectedItemPosition());
                        State_id=regoin_code.get(parent.getSelectedItemPosition());
                        district_spinner.resetAdapter(district_adapter);
                        getDisterct(State_id);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onFailure(Call<StateResbonse> call, Throwable t) {

            }
        });
    }

    public void getDisterct(String state_id){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);

        Call<DistrictResbonse> call=service.getdistricts(state_id);
        call.enqueue(new Callback<DistrictResbonse>() {
            @Override
            public void onResponse(Call<DistrictResbonse> call, Response<DistrictResbonse> response) {
                city.clear();
                city_id.clear();
                districtResponse=response.body().getDistricts();
                for (int i=0;i<districtResponse.size();i++)
                {
                    city.add(districtResponse.get(i).getDistrictName());
                    city_id.add(districtResponse.get(i).getDistrictId());
                }
                district_adapter=new ArrayAdapter<String>(RegisterActivity.this,R.layout.spinneritem2,city);
                district_adapter.setDropDownViewResource(R.layout.spinneritem2);
                district_spinner.setAdapter(district_adapter);

                district_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        District_name=city.get(parent.getSelectedItemPosition());
                        Disstrict_id=city_id.get(parent.getSelectedItemPosition());
                        taluk_spinner.resetAdapter(taluk_adapter);
                        getTaluk(State_id,Disstrict_id);

                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
            @Override
            public void onFailure(Call<DistrictResbonse> call, Throwable t) {

            }
        });
    }
    public void getTaluk(String state_id,String district_id){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<TalukResbonse> call=service.gettaluk(state_id,district_id);
        call.enqueue(new Callback<TalukResbonse>() {
            @Override
            public void onResponse(Call<TalukResbonse> call, Response<TalukResbonse> response) {
                taluk.clear();
                taluk_id.clear();
                taluk_auto_id.clear();
                if (response.body().getStatus().equals("success")){
                    talukResponse=response.body().getTaluks();
                    for (int i=0;i<talukResponse.size();i++){
                        taluk.add(talukResponse.get(i).getTalukName());
                        taluk_id.add(talukResponse.get(i).getTalukId());
                        taluk_auto_id.add(talukResponse.get(i).getAutoId());
                    }
                    taluk_adapter=new ArrayAdapter<String>(RegisterActivity.this,R.layout.spinneritem3,taluk);
                    taluk_adapter.setDropDownViewResource(R.layout.spinneritem3);
                    taluk_spinner.setAdapter(taluk_adapter);
                    taluk_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            Taluk_name=taluk.get(parent.getSelectedItemPosition());
                            Taluk_id=taluk_id.get(parent.getSelectedItemPosition());
                            str_auto_id=taluk_auto_id.get(parent.getSelectedItemPosition());
                            hobly_spinner.resetAdapter(taluk_adapter);
                            getHobly(str_auto_id);
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
            @Override
            public void onFailure(Call<TalukResbonse> call, Throwable t) {
            }
        });
    }
    public void getHobly(String str_auto_id){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);
        Call<HoblyResponse> call=service.gethobly(str_auto_id);
        call.enqueue(new Callback<HoblyResponse>() {
            @Override
            public void onResponse(Call<HoblyResponse> call, Response<HoblyResponse> response) {
                if (response.body().getStatus().equals("success")){
                    hobly.clear();
                    hobly_id.clear();
                    HoblyResponse=response.body().getHoblys();
                    for (int i=0;i<HoblyResponse.size();i++){
                        hobly.add(HoblyResponse.get(i).getHobliName());
                        hobly_id.add(HoblyResponse.get(i).getHobliId());
                    }
                    if(hobly.size()==0)
                    {
                            hobly_layout.setVisibility(View.GONE);
                            str_hobli_id="0";
                        hobli_select=false;

                    }
                    else
                    {
                        hobly_layout.setVisibility(View.VISIBLE);
                        hobli_select=true;
                    }
                    hobly_adapter=new ArrayAdapter<String>(RegisterActivity.this,R.layout.spinneritem3,hobly);
                    hobly_adapter.setDropDownViewResource(R.layout.spinneritem3);
                    hobly_spinner.setAdapter(hobly_adapter);
                    hobly_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            str_hobli_id=hobly_id.get(parent.getSelectedItemPosition());
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
            @Override
            public void onFailure(Call<HoblyResponse> call, Throwable t) {
            }
        });
    }
    private boolean isDeviceSupportCamera() {
        if (getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_VIDEO_CAPTURE) {
            fileUri = data.getData();
            if (EasyPermissions.hasPermissions(RegisterActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                displayRecordedVideo.setVideoURI(fileUri);
                displayRecordedVideo.start();

                pathToStoredVideo = getRealPathFromURIPath(fileUri, RegisterActivity.this);
                Log.d(TAG, "Recorded Video Path " + pathToStoredVideo);

             /*   uploadVideo(fileUri,video_Url);*/
                /*invokeUploadImageService(uri,filepath1,video_Url);*/
                //Store the video to your server
              /*  newRegister(name.getText().toString(),Last_name,email.getText().toString(),mobile.getText().toString(),aadhar.getText().toString(),Video_Url,Doc_Url,Address,
                        Landmark,State_name,District_name,pincode.getText().toString(),Country,Device_id);
*/
            } else {
                EasyPermissions.requestPermissions(RegisterActivity.this, getString(R.string.read_file), READ_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
            }
        }
        if (Constants.REQUEST_GALLERY_CODE == requestCode && resultCode == Activity.RESULT_OK && data != null) {
            uri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = RegisterActivity.this.getContentResolver().query(uri, filePathColumn, null, null, null);
            if (cursor == null)
                return;
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            filepath1 = cursor.getString(columnIndex);
//            progressBar.setVisibility(View.VISIBLE);
          /*  uploadVideo(fileUri,video_Url);*/
            /*invokeUploadImageService(uri,filepath1,video_Url);*/
            Glide.with(this).load(uri).into(kyc);
        }
      /*  if (requestCode == Constants.CAMERA_REQUEST) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");

                File photoFile = null;
                try {
                    photoFile = createImageFile(bitmap);
                } catch (IOException ex) {
                    // Error occurred while creating the File
                    ex.printStackTrace();
                }
                // Continue only if the File was successfully created
                if (photoFile != null)
                    uri = Uri.fromFile(photoFile);
              *//*  invokeUploadImageService(uri,filepath1,video_Url);*//*
                *//*invokeUploadImageService(uri,filepath1,video_Url);*//*
                Glide.with(this).load(uri).into(kyc);
              *//*  filepath1= String.valueOf(photoFile);*//*
            }
        }*/
    }


    private File createImageFile(Bitmap bitmap) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(imageFileName,  /* prefix */".jpg",/* suffix */RegisterActivity.this.getCacheDir() /* directory */);

       /* FileOutputStream fileOutputStream = new FileOutputStream(image);*/
        OutputStream outputStream = new FileOutputStream(image);
        /*bitmap = ((BitmapDrawable) kyc.getDrawable()).getBitmap();//newlly added*/
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        outputStream.flush();
        outputStream.close();

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.e("Getpath", "Cool" + mCurrentPhotoPath);
        return image;
    }
    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }
    private String getFileDestinationPath(){
        String generatedFilename = String.valueOf(System.currentTimeMillis());
        String filePathEnvironment = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
        File directoryFolder = new File(filePathEnvironment + "/video/");
        if(!directoryFolder.exists()){
            directoryFolder.mkdir();
        }
        Log.d(TAG, "Full path " + filePathEnvironment + "/video/" + generatedFilename + ".mp4");
        return filePathEnvironment + "/video/" + generatedFilename + ".mp4";
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if(fileUri != null){
            if(EasyPermissions.hasPermissions(RegisterActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)){
                displayRecordedVideo.setVideoURI(fileUri);
                displayRecordedVideo.start();

                pathToStoredVideo = getRealPathFromURIPath(fileUri, RegisterActivity.this);
                Log.d(TAG, "Recorded Video Path " + pathToStoredVideo);
                //Store the video to your server
              /*  uploadVideo(fileUri,videopath);*/

            }
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) { }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
       /* super.onRequestPermissionsResult(requestCode, permissions, grantResults);*/
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_CODE:{
                // When request is cancelled, the results array are empty
                if(
                        (grantResults.length >0) &&
                                (grantResults[0]
                                        + grantResults[1]
                                        == PackageManager.PERMISSION_GRANTED
                                )
                ){
                    // Permissions are granted
                    /*Toast.makeText(RegisterActivity.this,"Permissions granted.",Toast.LENGTH_SHORT).show();*/
                }else {
                    // Permissions are denied
                    Toast.makeText(RegisterActivity.this,"Permissions denied.",Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }



    public void newRegister(String first_name,String last_name,String email,String phonenumber,String pancard_no,String address,
                            String street_name,String building_name,String landmark,String state,String disrict,String taluk,String pincode,String country,String device_id,String vehicle_type,
                            String lat,String lang,String account_no,String ifsc_code,String bank_name,String version,String state_name,String district_name,String taluk_name){

        final ProgressDialog progressDialog1 = new ProgressDialog(RegisterActivity.this);
        progressDialog1.setCancelable(false);
        progressDialog1.setMessage("Loading please wait...");
        progressDialog1.show();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(Api.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Api service = retrofit.create(Api.class);

        RequestBody Firstname = RequestBody.create(MultipartBody.FORM,  first_name);
        RequestBody Lastname = RequestBody.create(MultipartBody.FORM, last_name);
        RequestBody Email = RequestBody.create(MultipartBody.FORM, email);
        RequestBody Phone_no = RequestBody.create(MultipartBody.FORM, phonenumber);
        RequestBody Pan = RequestBody.create(MultipartBody.FORM, pancard_no);

       /* MultipartBody.Part img=null;
                if (doc_url != null){
                    try {
                        File file=new File(String.valueOf(doc_url));
                        if (file.exists()){
                            RequestBody reqFile = RequestBody.create(MediaType.parse("images/*"), file);
                            img = MultipartBody.Part.createFormData("doc_url", file.getName(), reqFile);
                        }
                    }
                    catch (NullPointerException e){
                        e.printStackTrace();
                    }
                }else {
                    RequestBody attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "");
                    img = MultipartBody.Part.createFormData("doc_url", "", attachmentEmpty);
                }*/

       /* File file1=new File(String.valueOf(video_Url));
        RequestBody reqFile1 = RequestBody.create(MediaType.parse("video/*"), file1);
        MultipartBody.Part video = MultipartBody.Part.createFormData("video_url", file1.getName(), reqFile1);*/

        RequestBody Address = RequestBody.create(MultipartBody.FORM, address);
        RequestBody Street = RequestBody.create(MultipartBody.FORM, street_name);
        RequestBody Building = RequestBody.create(MultipartBody.FORM, building_name);
        RequestBody Landmark = RequestBody.create(MultipartBody.FORM, landmark);
        RequestBody State = RequestBody.create(MultipartBody.FORM, state);
        RequestBody District = RequestBody.create(MultipartBody.FORM, disrict);
        RequestBody Taluk=RequestBody.create(MultipartBody.FORM,taluk);
        RequestBody Pincode = RequestBody.create(MultipartBody.FORM, pincode);
        RequestBody Country = RequestBody.create(MultipartBody.FORM, country);
        RequestBody Device_id = RequestBody.create(MultipartBody.FORM, device_id);
        RequestBody Vehicle = RequestBody.create(MultipartBody.FORM, vehicle_type);
        RequestBody Lat = RequestBody.create(MultipartBody.FORM, lat);
        RequestBody Lang = RequestBody.create(MultipartBody.FORM, lang);
        RequestBody AccountNo=RequestBody.create(MultipartBody.FORM, account_no);
        RequestBody AccountIFSC=RequestBody.create(MultipartBody.FORM, ifsc_code);
        RequestBody AccountName=RequestBody.create(MultipartBody.FORM, bank_name);
        RequestBody Version=RequestBody.create(MultipartBody.FORM,version);
        RequestBody State_name=RequestBody.create(MultipartBody.FORM,state_name);
        RequestBody District_name=RequestBody.create(MultipartBody.FORM,district_name);
        RequestBody Taluk_name=RequestBody.create(MultipartBody.FORM,taluk_name);
        RequestBody Hobli_id=RequestBody.create(MultipartBody.FORM,str_hobli_id);

        Call<RegisterResbonse> call=service.getregister(Firstname,Lastname,Email,Phone_no,Pan,Address,Street,Building,Landmark,State,
                District,Taluk,Pincode,Country,Device_id,Vehicle,Lat,Lang,AccountNo,AccountIFSC,AccountName,Version,State_name,District_name,Taluk_name,Hobli_id);
        call.enqueue(new Callback<RegisterResbonse>() {
            @Override
            public void onResponse(Call<RegisterResbonse> call, Response<RegisterResbonse> response) {
                    progressDialog1.dismiss();
                if (response.body().getStatus().equals("success")){
                   // if (response.body().getMsg().equals("Register Successfully,Username and Pssword send to your mail")){
                        SharedPreferences sharedPreferences=getSharedPreferences("Data", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor=sharedPreferences.edit();
                        editor.commit();
                        editor.putBoolean("Register",true);
                        editor.apply();
                        showAlertDialog(R.layout.booking_success_popup);
                        Sharedpreference.storeBooleanValue(RegisterActivity.this,"Register",true);
                  //  }
                }
                if (response.body().getStatus().equals("fail")){
                    Toast.makeText(getApplicationContext(),"Already Register Email/Mobile please check",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RegisterResbonse> call, Throwable t) {

            }
        });
    }

    private void getLocation() {
        //Check Permissions again
        if (ActivityCompat.checkSelfPermission(RegisterActivity.this,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(RegisterActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) !=PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(RegisterActivity  .this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }
        else
        {
            Location LocationGps= locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location LocationNetwork=locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location LocationPassive=locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            if (LocationGps !=null)
            {
                double lat=LocationGps.getLatitude();
                double longi=LocationGps.getLongitude();

                Lat=String.valueOf(lat);
                Lang=String.valueOf(longi);
            }
            else if (LocationNetwork !=null)
            {
                double lat=LocationNetwork.getLatitude();
                double longi=LocationNetwork.getLongitude();

                Lat=String.valueOf(lat);
                Lang=String.valueOf(longi);

            }
            else if (LocationPassive !=null)
            {
                double lat=LocationPassive.getLatitude();
                double longi=LocationPassive.getLongitude();

                Lat=String.valueOf(lat);
                Lang=String.valueOf(longi);
            }
            else
            {
                Toast.makeText(RegisterActivity.this, "Can't Get Your Location", Toast.LENGTH_SHORT).show();
            }
            //Thats All Run Your App
        }
    }
    private void OnGPS() {
        final androidx.appcompat.app.AlertDialog.Builder builder= new androidx.appcompat.app.AlertDialog.Builder(RegisterActivity.this);
        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });
        final androidx.appcompat.app.AlertDialog alertDialog=builder.create();
        alertDialog.show();
    }

    public boolean validmatchemaill(){
        String pattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        String input = email.getText().toString().trim();
        if(input.length()==0){
            /*email.setError("Enter Email Address");*/
            email.setError("Enter Email Address");
            return false;
        }
        if (!input.matches(pattern)){
            email.setError("Please Enter Correct email");
            return false;
        }
        return true;
    }
    private boolean validatePhoneNumber() {
        String pattern = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[6789]\\d{9}$";
        String input = mobile.getText().toString().trim();
        if(input.isEmpty()){
//            ColorStateList colorStateList = ColorStateList.valueOf(getResources().getColor(R.color.colorAccent));
//            ViewCompat.setBackgroundTintList(phoneNumber, colorStateList);
            /*mobile.setError("Enter Mobile Number");*/
            mobile.setError("Please Enter Valid Mobile Number");
            return false;
        }if(!input.matches(pattern)){
//            ColorStateList colorStateList = ColorStateList.valueOf(getResources().getColor(R.color.colorAccent));
//            ViewCompat.setBackgroundTintList(phoneNumber, colorStateList);
            mobile.setError("Please Enter Correct Mobile Number");
            return false;
        } else {
//            ColorStateList colorStateList = ColorStateList.valueOf(getResources().getColor(R.color.colorGreen));
//            ViewCompat.setBackgroundTintList(phoneNumber, colorStateList);
            return true;
        }
    }

    public boolean validfirstName(){
        String input = first_name.getText().toString().trim();
        if (input.isEmpty()){
            first_name.setError("Please enter First name");
            return false;
        }
        else return true;
    }
    public boolean validgps(){
        if (!btn_gps.isChecked()){
            btn_gps.setError("Please click Enaple gps");
            return false;
        }
        else return false;
    }
    public boolean validterms(){
        if (!agree.isChecked()){
            agree.setError("Please Agree Terms & Condition");
            return false;
        }
        else return false;
    }
    public boolean validdoorno(){
        String input = door_no.getText().toString().trim();
        if (input.isEmpty()){
            door_no.setError("Please enter Door No");
            return false;
        }
        else return true;
    }
    public boolean validstreet(){
        String input = street.getText().toString().trim();
        if (input.isEmpty()){
            street.setError("Please enter First name");
            return false;
        }
        else return true;
    }
    public boolean validpincode(){
        String input = pincode.getText().toString().trim();
        if (input.isEmpty()){
            pincode.setError("Please enter First name");
            return false;
        }
        else return true;
    }

    public boolean validbankname(){
        String input = account_name.getText().toString().trim();
        if (input.isEmpty()){
            account_name.setError("Please enter First name");
            return false;
        }
        else return true;
    }
    public boolean validbankno(){
        String input = account_no.getText().toString().trim();
        if (input.isEmpty()){
            account_no.setError("Please enter First name");
            return false;
        }
        else return true;
    }
    public boolean validifsc(){
        String input = account_ifsc.getText().toString().trim();
        if (input.isEmpty()){
            account_ifsc.setError("Please enter First name");
            return false;
        }
        else return true;
    }
    public boolean validstate(){
        if (state_spinner.getSelectedItemPosition()==-1){
            setSpinnerError(state_spinner,"Select State");
            return false;
        }
        else return true;
    }
    public boolean validistrict(){
        if (district_spinner.getSelectedItemPosition()==-1){
            setSpinnerError(district_spinner,"Select district");
            return false;
        }
        else return true;
    }
    public boolean valitaluk(){
        if (taluk_spinner.getSelectedItemPosition()==-1){
            setSpinnerError(taluk_spinner,"Select taluk");
            return false;
        }
        else return true;
    }

    public boolean valihobli(){
        if (hobly_spinner.getSelectedItemPosition()==-1){
            setSpinnerError(hobly_spinner,"Select Hobli");
            return false;
        }
        else return true;
    }


    public void geterror(){
        LayoutInflater inflater = getLayoutInflater();
        View layout=inflater.inflate(R.layout.toast,(ViewGroup) findViewById(R.id.toast_layout));
        txt= layout.findViewById(R.id.error_toast_txt);
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.BOTTOM, 0, 100);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }
    public void showAlertDialog(int layout){
        dialogBuilder=new AlertDialog.Builder(RegisterActivity.this);
        View layoutview=getLayoutInflater().inflate(layout,null);
        Button btn=layoutview.findViewById(R.id.done);
        dialogBuilder.setView(layoutview);
        alertDialog=dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        alertDialog.setCancelable(false);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RegisterActivity.this,LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                String value = email.getText().toString().trim();
                SharedPreferences sharedPref = getSharedPreferences("myKey", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("value", value);
                editor.apply();
                startActivity(intent);
                alertDialog.dismiss();
            }
        });
    }
    private boolean isConnected(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


 }

