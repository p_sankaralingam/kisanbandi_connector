package com.agriculture.kisanbandiconnector;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.agriculture.kisanbandiconnector.Utils.ConnectivityReceiver;

public class WebviewActivity extends AppCompatActivity {

    WebView webview;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        webview=findViewById(R.id.webview);
        progressBar=findViewById(R.id.progress_bar);
        String url=getIntent().getStringExtra("url");
        webview.loadUrl(url);
        webview.setWebViewClient(new AppWebViewClients(progressBar));
    }
    @Override
    protected void onResume() {
        super.onResume();
        boolean connected= ConnectivityReceiver.isConnected();
        if(!connected)
        {
            startActivity(new Intent(getApplicationContext(), NointernetActivity.class));
        }
    }
}


class AppWebViewClients extends WebViewClient {
    private ProgressBar progressBar;
    public AppWebViewClients(ProgressBar progressBar) {
        this.progressBar=progressBar;
        progressBar.setVisibility(View.VISIBLE);
    }
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        // TODO Auto-generated method stub
        view.loadUrl(url);
        return true;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        // TODO Auto-generated method stub
        super.onPageFinished(view, url);
        progressBar.setVisibility(View.GONE);
    }
}