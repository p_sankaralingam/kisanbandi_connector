package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MeasurementResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statuscode")
    @Expose
    private String statuscode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("measurement_details")
    @Expose
    private List<MeasurementDetail> measurementDetails = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<MeasurementDetail> getMeasurementDetails() {
        return measurementDetails;
    }

    public void setMeasurementDetails(List<MeasurementDetail> measurementDetails) {
        this.measurementDetails = measurementDetails;
    }

    public class MeasurementDetail {

        @SerializedName("measurement_id")
        @Expose
        private String measurementId;
        @SerializedName("measurement_name")
        @Expose
        private String measurementName;

        public String getMeasurementId() {
            return measurementId;
        }

        public void setMeasurementId(String measurementId) {
            this.measurementId = measurementId;
        }

        public String getMeasurementName() {
            return measurementName;
        }

        public void setMeasurementName(String measurementName) {
            this.measurementName = measurementName;
        }
    }
}
