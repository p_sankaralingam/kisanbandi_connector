package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TalukResbonse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("taluks")
    @Expose
    private List<Taluk> taluks = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public TalukResbonse() {
    }

    /**
     *
     * @param taluks
     * @param status
     */
    public TalukResbonse(String status, List<Taluk> taluks) {
        super();
        this.status = status;
        this.taluks = taluks;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Taluk> getTaluks() {
        return taluks;
    }

    public void setTaluks(List<Taluk> taluks) {
        this.taluks = taluks;
    }
    public class Taluk {

        @SerializedName("auto_id")
        @Expose
        private String autoId;
        @SerializedName("state_id")
        @Expose
        private String stateId;
        @SerializedName("district_id")
        @Expose
        private String districtId;
        @SerializedName("taluk_id")
        @Expose
        private String talukId;
        @SerializedName("taluk_name")
        @Expose
        private String talukName;
        @SerializedName("flag")
        @Expose
        private String flag;
        @SerializedName("log")
        @Expose
        private String log;

        /**
         * No args constructor for use in serialization
         *
         */
        public Taluk() {
        }

        /**
         *
         * @param talukId
         * @param districtId
         * @param flag
         * @param talukName
         * @param log
         * @param autoId
         * @param stateId
         */
        public Taluk(String autoId, String stateId, String districtId, String talukId, String talukName, String flag, String log) {
            super();
            this.autoId = autoId;
            this.stateId = stateId;
            this.districtId = districtId;
            this.talukId = talukId;
            this.talukName = talukName;
            this.flag = flag;
            this.log = log;
        }

        public String getAutoId() {
            return autoId;
        }

        public void setAutoId(String autoId) {
            this.autoId = autoId;
        }

        public String getStateId() {
            return stateId;
        }

        public void setStateId(String stateId) {
            this.stateId = stateId;
        }

        public String getDistrictId() {
            return districtId;
        }

        public void setDistrictId(String districtId) {
            this.districtId = districtId;
        }

        public String getTalukId() {
            return talukId;
        }

        public void setTalukId(String talukId) {
            this.talukId = talukId;
        }

        public String getTalukName() {
            return talukName;
        }

        public void setTalukName(String talukName) {
            this.talukName = talukName;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getLog() {
            return log;
        }

        public void setLog(String log) {
            this.log = log;
        }

    }
}
