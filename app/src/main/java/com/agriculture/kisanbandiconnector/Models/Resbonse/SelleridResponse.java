package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SelleridResponse {



    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statuscode")
    @Expose
    private String statuscode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("product_list_details")
    @Expose
    private List<ProductListDetail> productListDetails = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ProductListDetail> getProductListDetails() {
        return productListDetails;
    }

    public void setProductListDetails(List<ProductListDetail> productListDetails) {
        this.productListDetails = productListDetails;
    }

    public class ProductListDetail {

        @SerializedName("sellers_id")
        @Expose
        private String sellersId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("connector_hub_id")
        @Expose
        private String connectorHubId;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private Object lastName;

        public String getSellersId() {
            return sellersId;
        }

        public void setSellersId(String sellersId) {
            this.sellersId = sellersId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getConnectorHubId() {
            return connectorHubId;
        }

        public void setConnectorHubId(String connectorHubId) {
            this.connectorHubId = connectorHubId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public Object getLastName() {
            return lastName;
        }

        public void setLastName(Object lastName) {
            this.lastName = lastName;
        }
    }
}
