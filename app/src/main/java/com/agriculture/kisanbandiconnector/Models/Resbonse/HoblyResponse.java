package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HoblyResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("hoblys")
    @Expose
    private List<Hobly> hoblys = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Hobly> getHoblys() {
        return hoblys;
    }

    public void setHoblys(List<Hobly> hoblys) {
        this.hoblys = hoblys;
    }

    public class Hobly {

        @SerializedName("hobli_id")
        @Expose
        private String hobliId;
        @SerializedName("taluk_auto_id")
        @Expose
        private String talukAutoId;
        @SerializedName("hobli_name")
        @Expose
        private String hobliName;
        @SerializedName("flag")
        @Expose
        private String flag;
        @SerializedName("log")
        @Expose
        private String log;

        public String getHobliId() {
            return hobliId;
        }

        public void setHobliId(String hobliId) {
            this.hobliId = hobliId;
        }

        public String getTalukAutoId() {
            return talukAutoId;
        }

        public void setTalukAutoId(String talukAutoId) {
            this.talukAutoId = talukAutoId;
        }

        public String getHobliName() {
            return hobliName;
        }

        public void setHobliName(String hobliName) {
            this.hobliName = hobliName;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getLog() {
            return log;
        }

        public void setLog(String log) {
            this.log = log;
        }
    }
}
