package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StateResbonse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("state")
    @Expose
    private List<State> state = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<State> getState() {
        return state;
    }

    public void setState(List<State> state) {
        this.state = state;
    }
    public class State {

        @SerializedName("state_id")
        @Expose
        private String stateId;
        @SerializedName("country_id")
        @Expose
        private String countryId;
        @SerializedName("state_name")
        @Expose
        private String stateName;
        @SerializedName("flag")
        @Expose
        private String flag;
        @SerializedName("log")
        @Expose
        private String log;

        public String getStateId() {
            return stateId;
        }

        public void setStateId(String stateId) {
            this.stateId = stateId;
        }

        public String getCountryId() {
            return countryId;
        }

        public void setCountryId(String countryId) {
            this.countryId = countryId;
        }

        public String getStateName() {
            return stateName;
        }

        public void setStateName(String stateName) {
            this.stateName = stateName;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getLog() {
            return log;
        }

        public void setLog(String log) {
            this.log = log;
        }
    }
}
