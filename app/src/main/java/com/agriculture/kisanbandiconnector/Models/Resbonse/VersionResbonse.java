package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VersionResbonse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("state")
    @Expose
    private List<State> state = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public VersionResbonse() {
    }

    /**
     *
     * @param state
     * @param status
     */
    public VersionResbonse(String status, List<State> state) {
        super();
        this.status = status;
        this.state = state;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<State> getState() {
        return state;
    }

    public void setState(List<State> state) {
        this.state = state;
    }


    public class State {

        @SerializedName("preference_id")
        @Expose
        private String preferenceId;
        @SerializedName("banner_status")
        @Expose
        private String bannerStatus;
        @SerializedName("version")
        @Expose
        private String version;
        @SerializedName("created_date")
        @Expose
        private String createdDate;

        /**
         * No args constructor for use in serialization
         *
         */
        public State() {
        }

        /**
         *
         * @param bannerStatus
         * @param createdDate
         * @param preferenceId
         * @param version
         */
        public State(String preferenceId, String bannerStatus, String version, String createdDate) {
            super();
            this.preferenceId = preferenceId;
            this.bannerStatus = bannerStatus;
            this.version = version;
            this.createdDate = createdDate;
        }

        public String getPreferenceId() {
            return preferenceId;
        }

        public void setPreferenceId(String preferenceId) {
            this.preferenceId = preferenceId;
        }

        public String getBannerStatus() {
            return bannerStatus;
        }

        public void setBannerStatus(String bannerStatus) {
            this.bannerStatus = bannerStatus;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

    }
}
