package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConnectorlistResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statuscode")
    @Expose
    private String statuscode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("connector_list")
    @Expose
    private List<ConnectorList> connectorList = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ConnectorList> getConnectorList() {
        return connectorList;
    }

    public void setConnectorList(List<ConnectorList> connectorList) {
        this.connectorList = connectorList;
    }


    public class ConnectorList {

        @SerializedName("connector_id")
        @Expose
        private String connectorId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("hub_status_code")
        @Expose
        private String hubStatusCode;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("phonenumber")
        @Expose
        private String phonenumber;
        @SerializedName("pancard_no")
        @Expose
        private String pancardNo;
        @SerializedName("user_type")
        @Expose
        private String userType;
        @SerializedName("video_url")
        @Expose
        private String videoUrl;
        @SerializedName("doc_url")
        @Expose
        private String docUrl;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("street_name")
        @Expose
        private String streetName;
        @SerializedName("building_name")
        @Expose
        private String buildingName;
        @SerializedName("landmark")
        @Expose
        private String landmark;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("city")
        @Expose
        private Object city;
        @SerializedName("district")
        @Expose
        private String district;
        @SerializedName("pincode")
        @Expose
        private String pincode;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("taluk")
        @Expose
        private String taluk;
        @SerializedName("device_id")
        @Expose
        private String deviceId;
        @SerializedName("vehicle_type")
        @Expose
        private String vehicleType;
        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("lang")
        @Expose
        private String lang;
        @SerializedName("account_no")
        @Expose
        private String accountNo;
        @SerializedName("ifsc_code")
        @Expose
        private String ifscCode;
        @SerializedName("bank_name")
        @Expose
        private String bankName;
        @SerializedName("login_id")
        @Expose
        private String loginId;
        @SerializedName("approval_flag")
        @Expose
        private String approvalFlag;
        @SerializedName("app_version")
        @Expose
        private String appVersion;
        @SerializedName("flag")
        @Expose
        private String flag;
        @SerializedName("log")
        @Expose
        private String log;
        @SerializedName("state_id")
        @Expose
        private String stateId;
        @SerializedName("country_id")
        @Expose
        private String countryId;
        @SerializedName("state_name")
        @Expose
        private String stateName;
        @SerializedName("auto_id")
        @Expose
        private String autoId;
        @SerializedName("district_id")
        @Expose
        private String districtId;
        @SerializedName("district_name")
        @Expose
        private String districtName;
        @SerializedName("taluk_id")
        @Expose
        private String talukId;
        @SerializedName("taluk_name")
        @Expose
        private String talukName;

        public String getConnectorId() {
            return connectorId;
        }

        public void setConnectorId(String connectorId) {
            this.connectorId = connectorId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getHubStatusCode() {
            return hubStatusCode;
        }

        public void setHubStatusCode(String hubStatusCode) {
            this.hubStatusCode = hubStatusCode;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPhonenumber() {
            return phonenumber;
        }

        public void setPhonenumber(String phonenumber) {
            this.phonenumber = phonenumber;
        }

        public String getPancardNo() {
            return pancardNo;
        }

        public void setPancardNo(String pancardNo) {
            this.pancardNo = pancardNo;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getVideoUrl() {
            return videoUrl;
        }

        public void setVideoUrl(String videoUrl) {
            this.videoUrl = videoUrl;
        }

        public String getDocUrl() {
            return docUrl;
        }

        public void setDocUrl(String docUrl) {
            this.docUrl = docUrl;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getStreetName() {
            return streetName;
        }

        public void setStreetName(String streetName) {
            this.streetName = streetName;
        }

        public String getBuildingName() {
            return buildingName;
        }

        public void setBuildingName(String buildingName) {
            this.buildingName = buildingName;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public Object getCity() {
            return city;
        }

        public void setCity(Object city) {
            this.city = city;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getTaluk() {
            return taluk;
        }

        public void setTaluk(String taluk) {
            this.taluk = taluk;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getVehicleType() {
            return vehicleType;
        }

        public void setVehicleType(String vehicleType) {
            this.vehicleType = vehicleType;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLang() {
            return lang;
        }

        public void setLang(String lang) {
            this.lang = lang;
        }

        public String getAccountNo() {
            return accountNo;
        }

        public void setAccountNo(String accountNo) {
            this.accountNo = accountNo;
        }

        public String getIfscCode() {
            return ifscCode;
        }

        public void setIfscCode(String ifscCode) {
            this.ifscCode = ifscCode;
        }

        public String getBankName() {
            return bankName;
        }

        public void setBankName(String bankName) {
            this.bankName = bankName;
        }

        public String getLoginId() {
            return loginId;
        }

        public void setLoginId(String loginId) {
            this.loginId = loginId;
        }

        public String getApprovalFlag() {
            return approvalFlag;
        }

        public void setApprovalFlag(String approvalFlag) {
            this.approvalFlag = approvalFlag;
        }

        public String getAppVersion() {
            return appVersion;
        }

        public void setAppVersion(String appVersion) {
            this.appVersion = appVersion;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getLog() {
            return log;
        }

        public void setLog(String log) {
            this.log = log;
        }

        public String getStateId() {
            return stateId;
        }

        public void setStateId(String stateId) {
            this.stateId = stateId;
        }

        public String getCountryId() {
            return countryId;
        }

        public void setCountryId(String countryId) {
            this.countryId = countryId;
        }

        public String getStateName() {
            return stateName;
        }

        public void setStateName(String stateName) {
            this.stateName = stateName;
        }

        public String getAutoId() {
            return autoId;
        }

        public void setAutoId(String autoId) {
            this.autoId = autoId;
        }

        public String getDistrictId() {
            return districtId;
        }

        public void setDistrictId(String districtId) {
            this.districtId = districtId;
        }

        public String getDistrictName() {
            return districtName;
        }

        public void setDistrictName(String districtName) {
            this.districtName = districtName;
        }

        public String getTalukId() {
            return talukId;
        }

        public void setTalukId(String talukId) {
            this.talukId = talukId;
        }

        public String getTalukName() {
            return talukName;
        }

        public void setTalukName(String talukName) {
            this.talukName = talukName;
        }

    }
}
