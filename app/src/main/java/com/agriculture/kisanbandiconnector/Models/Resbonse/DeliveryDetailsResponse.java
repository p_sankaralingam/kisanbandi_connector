package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeliveryDetailsResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statuscode")
    @Expose
    private String statuscode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("delivery_details")
    @Expose
    private List<DeliveryDetail> deliveryDetails = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DeliveryDetail> getDeliveryDetails() {
        return deliveryDetails;
    }

    public void setDeliveryDetails(List<DeliveryDetail> deliveryDetails) {
        this.deliveryDetails = deliveryDetails;
    }


    public class DeliveryDetail {

        @SerializedName("delivery_assign_id")
        @Expose
        private String deliveryAssignId;
        @SerializedName("order_id")
        @Expose
        private String orderId;
        @SerializedName("connector_id")
        @Expose
        private String connectorId;
        @SerializedName("connector_user_id")
        @Expose
        private String connectorUserId;
        @SerializedName("delivery_status")
        @Expose
        private String deliveryStatus;
        @SerializedName("delivery_status_code")
        @Expose
        private String deliveryStatusCode;
        @SerializedName("flag")
        @Expose
        private String flag;
        @SerializedName("log")
        @Expose
        private String log;
        @SerializedName("checkout_id")
        @Expose
        private Object checkoutId;
        @SerializedName("order_code")
        @Expose
        private String orderCode;
        @SerializedName("qr_code")
        @Expose
        private String qrCode;
        @SerializedName("trxn_pin")
        @Expose
        private String trxnPin;
        @SerializedName("buyer_user_id")
        @Expose
        private String buyerUserId;
        @SerializedName("buyer_address_id")
        @Expose
        private String buyerAddressId;
        @SerializedName("prod_actual_price")
        @Expose
        private String prodActualPrice;
        @SerializedName("prod_quantity")
        @Expose
        private String prodQuantity;
        @SerializedName("prod_grand_total")
        @Expose
        private String prodGrandTotal;
        @SerializedName("order_status")
        @Expose
        private String orderStatus;
        @SerializedName("paymentMode")
        @Expose
        private String paymentMode;
        @SerializedName("payment_status")
        @Expose
        private String paymentStatus;
        @SerializedName("transactionId")
        @Expose
        private String transactionId;
        @SerializedName("payment_id")
        @Expose
        private String paymentId;
        @SerializedName("receipt_order_id")
        @Expose
        private String receiptOrderId;
        @SerializedName("tracking_status")
        @Expose
        private String trackingStatus;
        @SerializedName("paidAmount")
        @Expose
        private String paidAmount;
        @SerializedName("order_delivery_status_code")
        @Expose
        private String orderDeliveryStatusCode;
        @SerializedName("order_delivery_status")
        @Expose
        private String orderDeliveryStatus;
        @SerializedName("buyers_address_id")
        @Expose
        private String buyersAddressId;
        @SerializedName("buyer_id")
        @Expose
        private String buyerId;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("street_name")
        @Expose
        private String streetName;
        @SerializedName("building_name")
        @Expose
        private String buildingName;
        @SerializedName("landmark")
        @Expose
        private String landmark;
        @SerializedName("city_id")
        @Expose
        private Object cityId;
        @SerializedName("state_id")
        @Expose
        private String stateId;
        @SerializedName("district_id")
        @Expose
        private String districtId;
        @SerializedName("country_id")
        @Expose
        private String countryId;
        @SerializedName("pincode")
        @Expose
        private String pincode;
        @SerializedName("addres_mode")
        @Expose
        private String addresMode;
        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("lang")
        @Expose
        private Object lang;

        public String getDeliveryAssignId() {
            return deliveryAssignId;
        }

        public void setDeliveryAssignId(String deliveryAssignId) {
            this.deliveryAssignId = deliveryAssignId;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getConnectorId() {
            return connectorId;
        }

        public void setConnectorId(String connectorId) {
            this.connectorId = connectorId;
        }

        public String getConnectorUserId() {
            return connectorUserId;
        }

        public void setConnectorUserId(String connectorUserId) {
            this.connectorUserId = connectorUserId;
        }

        public String getDeliveryStatus() {
            return deliveryStatus;
        }

        public void setDeliveryStatus(String deliveryStatus) {
            this.deliveryStatus = deliveryStatus;
        }

        public String getDeliveryStatusCode() {
            return deliveryStatusCode;
        }

        public void setDeliveryStatusCode(String deliveryStatusCode) {
            this.deliveryStatusCode = deliveryStatusCode;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getLog() {
            return log;
        }

        public void setLog(String log) {
            this.log = log;
        }

        public Object getCheckoutId() {
            return checkoutId;
        }

        public void setCheckoutId(Object checkoutId) {
            this.checkoutId = checkoutId;
        }

        public String getOrderCode() {
            return orderCode;
        }

        public void setOrderCode(String orderCode) {
            this.orderCode = orderCode;
        }

        public String getQrCode() {
            return qrCode;
        }

        public void setQrCode(String qrCode) {
            this.qrCode = qrCode;
        }

        public String getTrxnPin() {
            return trxnPin;
        }

        public void setTrxnPin(String trxnPin) {
            this.trxnPin = trxnPin;
        }

        public String getBuyerUserId() {
            return buyerUserId;
        }

        public void setBuyerUserId(String buyerUserId) {
            this.buyerUserId = buyerUserId;
        }

        public String getBuyerAddressId() {
            return buyerAddressId;
        }

        public void setBuyerAddressId(String buyerAddressId) {
            this.buyerAddressId = buyerAddressId;
        }

        public String getProdActualPrice() {
            return prodActualPrice;
        }

        public void setProdActualPrice(String prodActualPrice) {
            this.prodActualPrice = prodActualPrice;
        }

        public String getProdQuantity() {
            return prodQuantity;
        }

        public void setProdQuantity(String prodQuantity) {
            this.prodQuantity = prodQuantity;
        }

        public String getProdGrandTotal() {
            return prodGrandTotal;
        }

        public void setProdGrandTotal(String prodGrandTotal) {
            this.prodGrandTotal = prodGrandTotal;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public String getPaymentStatus() {
            return paymentStatus;
        }

        public void setPaymentStatus(String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

        public String getPaymentId() {
            return paymentId;
        }

        public void setPaymentId(String paymentId) {
            this.paymentId = paymentId;
        }

        public String getReceiptOrderId() {
            return receiptOrderId;
        }

        public void setReceiptOrderId(String receiptOrderId) {
            this.receiptOrderId = receiptOrderId;
        }

        public String getTrackingStatus() {
            return trackingStatus;
        }

        public void setTrackingStatus(String trackingStatus) {
            this.trackingStatus = trackingStatus;
        }

        public String getPaidAmount() {
            return paidAmount;
        }

        public void setPaidAmount(String paidAmount) {
            this.paidAmount = paidAmount;
        }

        public String getOrderDeliveryStatusCode() {
            return orderDeliveryStatusCode;
        }

        public void setOrderDeliveryStatusCode(String orderDeliveryStatusCode) {
            this.orderDeliveryStatusCode = orderDeliveryStatusCode;
        }

        public String getOrderDeliveryStatus() {
            return orderDeliveryStatus;
        }

        public void setOrderDeliveryStatus(String orderDeliveryStatus) {
            this.orderDeliveryStatus = orderDeliveryStatus;
        }

        public String getBuyersAddressId() {
            return buyersAddressId;
        }

        public void setBuyersAddressId(String buyersAddressId) {
            this.buyersAddressId = buyersAddressId;
        }

        public String getBuyerId() {
            return buyerId;
        }

        public void setBuyerId(String buyerId) {
            this.buyerId = buyerId;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getStreetName() {
            return streetName;
        }

        public void setStreetName(String streetName) {
            this.streetName = streetName;
        }

        public String getBuildingName() {
            return buildingName;
        }

        public void setBuildingName(String buildingName) {
            this.buildingName = buildingName;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public Object getCityId() {
            return cityId;
        }

        public void setCityId(Object cityId) {
            this.cityId = cityId;
        }

        public String getStateId() {
            return stateId;
        }

        public void setStateId(String stateId) {
            this.stateId = stateId;
        }

        public String getDistrictId() {
            return districtId;
        }

        public void setDistrictId(String districtId) {
            this.districtId = districtId;
        }

        public String getCountryId() {
            return countryId;
        }

        public void setCountryId(String countryId) {
            this.countryId = countryId;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getAddresMode() {
            return addresMode;
        }

        public void setAddresMode(String addresMode) {
            this.addresMode = addresMode;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public Object getLang() {
            return lang;
        }

        public void setLang(Object lang) {
            this.lang = lang;
        }

    }


}
