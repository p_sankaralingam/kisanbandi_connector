package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HubDashboardResponse {


    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statuscode")
    @Expose
    private String statuscode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("count")
    @Expose
    private Count count;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Count getCount() {
        return count;
    }

    public void setCount(Count count) {
        this.count = count;
    }


    public class Count {

        @SerializedName("connector_count")
        @Expose
        private Integer connectorCount;
        @SerializedName("hub_product")
        @Expose
        private Integer hubProduct;
        @SerializedName("hub_order")
        @Expose
        private Integer hubOrder;
        @SerializedName("seller_list")
        @Expose
        private Integer sellerList;
        @SerializedName("buyer_list")
        @Expose
        private Integer buyerList;
        @SerializedName("connector_notification")
        @Expose
        private Integer connectorNotification;

        public Integer getConnectorCount() {
            return connectorCount;
        }

        public void setConnectorCount(Integer connectorCount) {
            this.connectorCount = connectorCount;
        }

        public Integer getHubProduct() {
            return hubProduct;
        }

        public void setHubProduct(Integer hubProduct) {
            this.hubProduct = hubProduct;
        }

        public Integer getHubOrder() {
            return hubOrder;
        }

        public void setHubOrder(Integer hubOrder) {
            this.hubOrder = hubOrder;
        }

        public Integer getSellerList() {
            return sellerList;
        }

        public void setSellerList(Integer sellerList) {
            this.sellerList = sellerList;
        }

        public Integer getBuyerList() {
            return buyerList;
        }

        public void setBuyerList(Integer buyerList) {
            this.buyerList = buyerList;
        }

        public Integer getConnectorNotification() {
            return connectorNotification;
        }

        public void setConnectorNotification(Integer connectorNotification) {
            this.connectorNotification = connectorNotification;
        }

    }
}
