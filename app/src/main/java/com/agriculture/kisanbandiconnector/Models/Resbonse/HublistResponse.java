package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HublistResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private String statusCode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("delivery_details")
    @Expose
    private List<DeliveryDetail> deliveryDetails = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DeliveryDetail> getDeliveryDetails() {
        return deliveryDetails;
    }

    public void setDeliveryDetails(List<DeliveryDetail> deliveryDetails) {
        this.deliveryDetails = deliveryDetails;
    }


    public class DeliveryDetail {

        @SerializedName("order_id")
        @Expose
        private String orderId;
        @SerializedName("order_code")
        @Expose
        private String orderCode;
        @SerializedName("prod_id")
        @Expose
        private String prodId;
        @SerializedName("seller_id")
        @Expose
        private String sellerId;
        @SerializedName("seller_user_id")
        @Expose
        private String sellerUserId;
        @SerializedName("buyer_id")
        @Expose
        private String buyerId;
        @SerializedName("buyers_address_id")
        @Expose
        private String buyersAddressId;
        @SerializedName("order_lat")
        @Expose
        private String orderLat;
        @SerializedName("order_long")
        @Expose
        private String orderLong;
        @SerializedName("product_quantity")
        @Expose
        private String productQuantity;
        @SerializedName("product_actual_price")
        @Expose
        private String productActualPrice;
        @SerializedName("product_subtotal")
        @Expose
        private String productSubtotal;
        @SerializedName("shipping_type")
        @Expose
        private String shippingType;
        @SerializedName("shipping_amount")
        @Expose
        private String shippingAmount;
        @SerializedName("grand_total")
        @Expose
        private String grandTotal;
        @SerializedName("order_status")
        @Expose
        private String orderStatus;
        @SerializedName("payment_mode")
        @Expose
        private String paymentMode;
        @SerializedName("transaction_id")
        @Expose
        private String transactionId;
        @SerializedName("tracking_status")
        @Expose
        private String trackingStatus;
        @SerializedName("flag")
        @Expose
        private String flag;
        @SerializedName("order_delivery_status")
        @Expose
        private String orderDeliveryStatus;
        @SerializedName("log")
        @Expose
        private String log;
        @SerializedName("sellers_id")
        @Expose
        private String sellersId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("phonenumber")
        @Expose
        private String phonenumber;
        @SerializedName("pancard_no")
        @Expose
        private String pancardNo;
        @SerializedName("seller_type")
        @Expose
        private String sellerType;
        @SerializedName("free_delivery")
        @Expose
        private String freeDelivery;
        @SerializedName("kilometer")
        @Expose
        private String kilometer;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("street_name")
        @Expose
        private String streetName;
        @SerializedName("building_name")
        @Expose
        private String buildingName;
        @SerializedName("landmark")
        @Expose
        private String landmark;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("city")
        @Expose
        private Object city;
        @SerializedName("taluk")
        @Expose
        private Object taluk;
        @SerializedName("district")
        @Expose
        private String district;
        @SerializedName("pincode")
        @Expose
        private String pincode;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("account_no")
        @Expose
        private String accountNo;
        @SerializedName("ifsc_code")
        @Expose
        private String ifscCode;
        @SerializedName("bank_name")
        @Expose
        private String bankName;
        @SerializedName("ipaddress")
        @Expose
        private String ipaddress;
        @SerializedName("lat")
        @Expose
        private Object lat;
        @SerializedName("lang")
        @Expose
        private Object lang;
        @SerializedName("login_id")
        @Expose
        private String loginId;
        @SerializedName("order_count")
        @Expose
        private String orderCount;

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getOrderCode() {
            return orderCode;
        }

        public void setOrderCode(String orderCode) {
            this.orderCode = orderCode;
        }

        public String getProdId() {
            return prodId;
        }

        public void setProdId(String prodId) {
            this.prodId = prodId;
        }

        public String getSellerId() {
            return sellerId;
        }

        public void setSellerId(String sellerId) {
            this.sellerId = sellerId;
        }

        public String getSellerUserId() {
            return sellerUserId;
        }

        public void setSellerUserId(String sellerUserId) {
            this.sellerUserId = sellerUserId;
        }

        public String getBuyerId() {
            return buyerId;
        }

        public void setBuyerId(String buyerId) {
            this.buyerId = buyerId;
        }

        public String getBuyersAddressId() {
            return buyersAddressId;
        }

        public void setBuyersAddressId(String buyersAddressId) {
            this.buyersAddressId = buyersAddressId;
        }

        public String getOrderLat() {
            return orderLat;
        }

        public void setOrderLat(String orderLat) {
            this.orderLat = orderLat;
        }

        public String getOrderLong() {
            return orderLong;
        }

        public void setOrderLong(String orderLong) {
            this.orderLong = orderLong;
        }

        public String getProductQuantity() {
            return productQuantity;
        }

        public void setProductQuantity(String productQuantity) {
            this.productQuantity = productQuantity;
        }

        public String getProductActualPrice() {
            return productActualPrice;
        }

        public void setProductActualPrice(String productActualPrice) {
            this.productActualPrice = productActualPrice;
        }

        public String getProductSubtotal() {
            return productSubtotal;
        }

        public void setProductSubtotal(String productSubtotal) {
            this.productSubtotal = productSubtotal;
        }

        public String getShippingType() {
            return shippingType;
        }

        public void setShippingType(String shippingType) {
            this.shippingType = shippingType;
        }

        public String getShippingAmount() {
            return shippingAmount;
        }

        public void setShippingAmount(String shippingAmount) {
            this.shippingAmount = shippingAmount;
        }

        public String getGrandTotal() {
            return grandTotal;
        }

        public void setGrandTotal(String grandTotal) {
            this.grandTotal = grandTotal;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

        public String getTrackingStatus() {
            return trackingStatus;
        }

        public void setTrackingStatus(String trackingStatus) {
            this.trackingStatus = trackingStatus;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getOrderDeliveryStatus() {
            return orderDeliveryStatus;
        }

        public void setOrderDeliveryStatus(String orderDeliveryStatus) {
            this.orderDeliveryStatus = orderDeliveryStatus;
        }

        public String getLog() {
            return log;
        }

        public void setLog(String log) {
            this.log = log;
        }

        public String getSellersId() {
            return sellersId;
        }

        public void setSellersId(String sellersId) {
            this.sellersId = sellersId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPhonenumber() {
            return phonenumber;
        }

        public void setPhonenumber(String phonenumber) {
            this.phonenumber = phonenumber;
        }

        public String getPancardNo() {
            return pancardNo;
        }

        public void setPancardNo(String pancardNo) {
            this.pancardNo = pancardNo;
        }

        public String getSellerType() {
            return sellerType;
        }

        public void setSellerType(String sellerType) {
            this.sellerType = sellerType;
        }

        public String getFreeDelivery() {
            return freeDelivery;
        }

        public void setFreeDelivery(String freeDelivery) {
            this.freeDelivery = freeDelivery;
        }

        public String getKilometer() {
            return kilometer;
        }

        public void setKilometer(String kilometer) {
            this.kilometer = kilometer;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getStreetName() {
            return streetName;
        }

        public void setStreetName(String streetName) {
            this.streetName = streetName;
        }

        public String getBuildingName() {
            return buildingName;
        }

        public void setBuildingName(String buildingName) {
            this.buildingName = buildingName;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public Object getCity() {
            return city;
        }

        public void setCity(Object city) {
            this.city = city;
        }

        public Object getTaluk() {
            return taluk;
        }

        public void setTaluk(Object taluk) {
            this.taluk = taluk;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getAccountNo() {
            return accountNo;
        }

        public void setAccountNo(String accountNo) {
            this.accountNo = accountNo;
        }

        public String getIfscCode() {
            return ifscCode;
        }

        public void setIfscCode(String ifscCode) {
            this.ifscCode = ifscCode;
        }

        public String getBankName() {
            return bankName;
        }

        public void setBankName(String bankName) {
            this.bankName = bankName;
        }

        public String getIpaddress() {
            return ipaddress;
        }

        public void setIpaddress(String ipaddress) {
            this.ipaddress = ipaddress;
        }

        public Object getLat() {
            return lat;
        }

        public void setLat(Object lat) {
            this.lat = lat;
        }

        public Object getLang() {
            return lang;
        }

        public void setLang(Object lang) {
            this.lang = lang;
        }

        public String getLoginId() {
            return loginId;
        }

        public void setLoginId(String loginId) {
            this.loginId = loginId;
        }

        public String getOrderCount() {
            return orderCount;
        }

        public void setOrderCount(String orderCount) {
            this.orderCount = orderCount;
        }

    }
}
