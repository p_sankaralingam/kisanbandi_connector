package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationlistResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statuscode")
    @Expose
    private String statuscode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("connector_notification")
    @Expose
    private List<ConnectorNotification> connectorNotification = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ConnectorNotification> getConnectorNotification() {
        return connectorNotification;
    }

    public void setConnectorNotification(List<ConnectorNotification> connectorNotification) {
        this.connectorNotification = connectorNotification;
    }


    public class ConnectorNotification {

        @SerializedName("connector_notification_id")
        @Expose
        private String connectorNotificationId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("content")
        @Expose
        private String content;
        @SerializedName("read_status")
        @Expose
        private String readStatus;
        @SerializedName("flag")
        @Expose
        private String flag;
        @SerializedName("log")
        @Expose
        private String log;

        public String getConnectorNotificationId() {
            return connectorNotificationId;
        }

        public void setConnectorNotificationId(String connectorNotificationId) {
            this.connectorNotificationId = connectorNotificationId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getReadStatus() {
            return readStatus;
        }

        public void setReadStatus(String readStatus) {
            this.readStatus = readStatus;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getLog() {
            return log;
        }

        public void setLog(String log) {
            this.log = log;
        }

    }
}
