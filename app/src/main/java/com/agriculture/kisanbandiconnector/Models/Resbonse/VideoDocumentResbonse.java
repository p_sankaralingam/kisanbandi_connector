package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoDocumentResbonse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statuscode")
    @Expose
    private String statuscode;

    /**
     * No args constructor for use in serialization
     *
     */
    public VideoDocumentResbonse() {
    }

    /**
     *
     * @param statuscode
     * @param status
     */
    public VideoDocumentResbonse(String status, String statuscode) {
        super();
        this.status = status;
        this.statuscode = statuscode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }
}
