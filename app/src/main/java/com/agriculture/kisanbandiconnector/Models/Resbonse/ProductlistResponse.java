package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductlistResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statuscode")
    @Expose
    private String statuscode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("product_list_details")
    @Expose
    private List<ProductListDetail> productListDetails = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<ProductListDetail> getProductListDetails() {
        return productListDetails;
    }

    public void setProductListDetails(List<ProductListDetail> productListDetails) {
        this.productListDetails = productListDetails;
    }

    public class ProductListDetail {

        @SerializedName("prod_id")
        @Expose
        private String prodId;

        @SerializedName("seller_id")
        @Expose
        private String seller_id;

        @SerializedName("prod_code")
        @Expose
        private String prodCode;
        @SerializedName("user_type")
        @Expose
        private String userType;
        @SerializedName("connector_id")
        @Expose
        private String connectorId;
        @SerializedName("mainc_id")
        @Expose
        private String maincId;
        @SerializedName("subc_id")
        @Expose
        private String subcId;
        @SerializedName("prod_name")
        @Expose
        private String prodName;
        @SerializedName("prod_detail")
        @Expose
        private String prodDetail;
        @SerializedName("prod_imageurl")
        @Expose
        private String prodImageurl;
        @SerializedName("prod_price")
        @Expose
        private String prodPrice;
        @SerializedName("selling_price")
        @Expose
        private String sellingPrice;
        @SerializedName("measurement_id")
        @Expose
        private String measurementId;
        @SerializedName("prod_quantity")
        @Expose
        private String prodQuantity;
        @SerializedName("grade")
        @Expose
        private String grade;
        @SerializedName("flag")
        @Expose
        private String flag;
        @SerializedName("mainc_name")
        @Expose
        private String maincName;
        @SerializedName("Commision_amount")
        @Expose
        private String commisionAmount;
        @SerializedName("catgr_img_url")
        @Expose
        private String catgrImgUrl;
        @SerializedName("subc_name")
        @Expose
        private String subcName;
        @SerializedName("measurement_name")
        @Expose
        private String measurementName;

        public String getProdId() {
            return prodId;
        }

        public void setProdId(String prodId) {
            this.prodId = prodId;
        }

        public String getProdCode() {
            return prodCode;
        }

        public void setProdCode(String prodCode) {
            this.prodCode = prodCode;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getConnectorId() {
            return connectorId;
        }

        public void setConnectorId(String connectorId) {
            this.connectorId = connectorId;
        }

        public String getMaincId() {
            return maincId;
        }

        public void setMaincId(String maincId) {
            this.maincId = maincId;
        }

        public String getSubcId() {
            return subcId;
        }

        public void setSubcId(String subcId) {
            this.subcId = subcId;
        }

        public String getProdName() {
            return prodName;
        }

        public void setProdName(String prodName) {
            this.prodName = prodName;
        }

        public String getProdDetail() {
            return prodDetail;
        }

        public void setProdDetail(String prodDetail) {
            this.prodDetail = prodDetail;
        }

        public String getProdImageurl() {
            return prodImageurl;
        }

        public void setProdImageurl(String prodImageurl) {
            this.prodImageurl = prodImageurl;
        }

        public String getProdPrice() {
            return prodPrice;
        }

        public void setProdPrice(String prodPrice) {
            this.prodPrice = prodPrice;
        }

        public String getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(String sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public String getMeasurementId() {
            return measurementId;
        }

        public void setMeasurementId(String measurementId) {
            this.measurementId = measurementId;
        }

        public String getProdQuantity() {
            return prodQuantity;
        }

        public void setProdQuantity(String prodQuantity) {
            this.prodQuantity = prodQuantity;
        }

        public String getGrade() {
            return grade;
        }

        public void setGrade(String grade) {
            this.grade = grade;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getMaincName() {
            return maincName;
        }

        public void setMaincName(String maincName) {
            this.maincName = maincName;
        }

        public String getCommisionAmount() {
            return commisionAmount;
        }

        public void setCommisionAmount(String commisionAmount) {
            this.commisionAmount = commisionAmount;
        }

        public String getCatgrImgUrl() {
            return catgrImgUrl;
        }

        public void setCatgrImgUrl(String catgrImgUrl) {
            this.catgrImgUrl = catgrImgUrl;
        }

        public String getSubcName() {
            return subcName;
        }

        public void setSubcName(String subcName) {
            this.subcName = subcName;
        }

        public String getMeasurementName() {
            return measurementName;
        }

        public void setMeasurementName(String measurementName) {
            this.measurementName = measurementName;
        }

        public String getSeller_id() {
            return seller_id;
        }

        public void setSeller_id(String seller_id) {
            this.seller_id = seller_id;
        }
    }
    }
