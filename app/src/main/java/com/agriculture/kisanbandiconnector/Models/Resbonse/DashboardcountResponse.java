package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashboardcountResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statuscode")
    @Expose
    private String statuscode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("completed_delivery_count")
    @Expose
    private String completedDeliveryCount;
    @SerializedName("current_delivery_count")
    @Expose
    private String currentDeliveryCount;

    @SerializedName("login_status")
    @Expose
    private String login_status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCompletedDeliveryCount() {
        return completedDeliveryCount;
    }

    public void setCompletedDeliveryCount(String completedDeliveryCount) {
        this.completedDeliveryCount = completedDeliveryCount;
    }

    public String getCurrentDeliveryCount() {
        return currentDeliveryCount;
    }

    public void setCurrentDeliveryCount(String currentDeliveryCount) {
        this.currentDeliveryCount = currentDeliveryCount;
    }


    public String getLogin_status() {
        return login_status;
    }

    public void setLogin_status(String login_status) {
        this.login_status = login_status;
    }
}
