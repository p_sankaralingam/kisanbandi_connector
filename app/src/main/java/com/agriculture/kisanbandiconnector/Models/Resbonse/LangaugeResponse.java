package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LangaugeResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusCode")
    @Expose
    private String statusCode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("language_details")
    @Expose
    private List<LanguageDetail> languageDetails = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<LanguageDetail> getLanguageDetails() {
        return languageDetails;
    }

    public void setLanguageDetails(List<LanguageDetail> languageDetails) {
        this.languageDetails = languageDetails;
    }


    public class LanguageDetail {

        @SerializedName("lang_id")
        @Expose
        private String langId;
        @SerializedName("language")
        @Expose
        private String language;
        @SerializedName("language_id")
        @Expose
        private String languageId;
        @SerializedName("language_name")
        @Expose
        private String languageName;

        public String getLangId() {
            return langId;
        }

        public void setLangId(String langId) {
            this.langId = langId;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getLanguageId() {
            return languageId;
        }

        public void setLanguageId(String languageId) {
            this.languageId = languageId;
        }

        public String getLanguageName() {
            return languageName;
        }

        public void setLanguageName(String languageName) {
            this.languageName = languageName;
        }

    }
}