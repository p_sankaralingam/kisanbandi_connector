package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubcategoryResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statuscode")
    @Expose
    private String statuscode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("sub_category_details")
    @Expose
    private List<SubCategoryDetail> subCategoryDetails = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<SubCategoryDetail> getSubCategoryDetails() {
        return subCategoryDetails;
    }

    public void setSubCategoryDetails(List<SubCategoryDetail> subCategoryDetails) {
        this.subCategoryDetails = subCategoryDetails;
    }

    public class SubCategoryDetail {

        @SerializedName("subc_id")
        @Expose
        private String subcId;
        @SerializedName("mainc_id")
        @Expose
        private String maincId;
        @SerializedName("subc_name")
        @Expose
        private String subcName;

        public String getSubcId() {
            return subcId;
        }

        public void setSubcId(String subcId) {
            this.subcId = subcId;
        }

        public String getMaincId() {
            return maincId;
        }

        public void setMaincId(String maincId) {
            this.maincId = maincId;
        }

        public String getSubcName() {
            return subcName;
        }

        public void setSubcName(String subcName) {
            this.subcName = subcName;
        }

    }

}
