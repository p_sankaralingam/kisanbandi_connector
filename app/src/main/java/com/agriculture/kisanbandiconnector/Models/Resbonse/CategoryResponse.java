package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statuscode")
    @Expose
    private String statuscode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("category_details")
    @Expose
    private List<CategoryDetail> categoryDetails = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<CategoryDetail> getCategoryDetails() {
        return categoryDetails;
    }

    public void setCategoryDetails(List<CategoryDetail> categoryDetails) {
        this.categoryDetails = categoryDetails;
    }


    public class CategoryDetail {

        @SerializedName("mainc_id")
        @Expose
        private String maincId;
        @SerializedName("mainc_name")
        @Expose
        private String maincName;
        @SerializedName("Commision_amount")
        @Expose
        private String commisionAmount;

        public String getMaincId() {
            return maincId;
        }

        public void setMaincId(String maincId) {
            this.maincId = maincId;
        }

        public String getMaincName() {
            return maincName;
        }

        public void setMaincName(String maincName) {
            this.maincName = maincName;
        }

        public String getCommisionAmount() {
            return commisionAmount;
        }

        public void setCommisionAmount(String commisionAmount) {
            this.commisionAmount = commisionAmount;
        }

    }
}
