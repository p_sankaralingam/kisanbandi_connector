package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HuborderlistResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statuscode")
    @Expose
    private String statuscode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("order_list")
    @Expose
    private List<OrderList> orderList = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<OrderList> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<OrderList> orderList) {
        this.orderList = orderList;
    }

    public class OrderList {
        private boolean isChecked = false;
        @SerializedName("order_id")
        @Expose
        private String orderId;
        @SerializedName("order_code")
        @Expose
        private String orderCode;
        @SerializedName("prod_id")
        @Expose
        private String prodId;
        @SerializedName("seller_id")
        @Expose
        private String sellerId;
        @SerializedName("seller_user_id")
        @Expose
        private String sellerUserId;
        @SerializedName("buyer_id")
        @Expose
        private String buyerId;
        @SerializedName("buyers_address_id")
        @Expose
        private String buyersAddressId;
        @SerializedName("order_lat")
        @Expose
        private String orderLat;
        @SerializedName("order_long")
        @Expose
        private String orderLong;
        @SerializedName("product_quantity")
        @Expose
        private String productQuantity;
        @SerializedName("product_actual_price")
        @Expose
        private String productActualPrice;
        @SerializedName("product_subtotal")
        @Expose
        private String productSubtotal;
        @SerializedName("shipping_type")
        @Expose
        private String shippingType;
        @SerializedName("shipping_amount")
        @Expose
        private String shippingAmount;
        @SerializedName("grand_total")
        @Expose
        private String grandTotal;
        @SerializedName("order_status")
        @Expose
        private String orderStatus;
        @SerializedName("payment_mode")
        @Expose
        private String paymentMode;
        @SerializedName("transaction_id")
        @Expose
        private String transactionId;
        @SerializedName("tracking_status")
        @Expose
        private String trackingStatus;
        @SerializedName("flag")
        @Expose
        private String flag;
        @SerializedName("order_delivery_status")
        @Expose
        private String orderDeliveryStatus;
        @SerializedName("log")
        @Expose
        private String log;
        @SerializedName("product_name")
        @Expose
        private String productName;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("street_name")
        @Expose
        private String streetName;
        @SerializedName("building_name")
        @Expose
        private String buildingName;
        @SerializedName("landmark")
        @Expose
        private String landmark;
        @SerializedName("country_name")
        @Expose
        private String countryName;
        @SerializedName("pincode")
        @Expose
        private String pincode;
        @SerializedName("addres_mode")
        @Expose
        private String addresMode;
        @SerializedName("state_name")
        @Expose
        private String stateName;
        @SerializedName("district_name")
        @Expose
        private String districtName;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("full_name")
        @Expose
        private String fullName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("phonenumber")
        @Expose
        private String phonenumber;


        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getOrderCode() {
            return orderCode;
        }

        public void setOrderCode(String orderCode) {
            this.orderCode = orderCode;
        }

        public String getProdId() {
            return prodId;
        }

        public void setProdId(String prodId) {
            this.prodId = prodId;
        }

        public String getSellerId() {
            return sellerId;
        }

        public void setSellerId(String sellerId) {
            this.sellerId = sellerId;
        }

        public String getSellerUserId() {
            return sellerUserId;
        }

        public void setSellerUserId(String sellerUserId) {
            this.sellerUserId = sellerUserId;
        }

        public String getBuyerId() {
            return buyerId;
        }

        public void setBuyerId(String buyerId) {
            this.buyerId = buyerId;
        }

        public String getBuyersAddressId() {
            return buyersAddressId;
        }

        public void setBuyersAddressId(String buyersAddressId) {
            this.buyersAddressId = buyersAddressId;
        }

        public String getOrderLat() {
            return orderLat;
        }

        public void setOrderLat(String orderLat) {
            this.orderLat = orderLat;
        }

        public String getOrderLong() {
            return orderLong;
        }

        public void setOrderLong(String orderLong) {
            this.orderLong = orderLong;
        }

        public String getProductQuantity() {
            return productQuantity;
        }

        public void setProductQuantity(String productQuantity) {
            this.productQuantity = productQuantity;
        }

        public String getProductActualPrice() {
            return productActualPrice;
        }

        public void setProductActualPrice(String productActualPrice) {
            this.productActualPrice = productActualPrice;
        }

        public String getProductSubtotal() {
            return productSubtotal;
        }

        public void setProductSubtotal(String productSubtotal) {
            this.productSubtotal = productSubtotal;
        }

        public String getShippingType() {
            return shippingType;
        }

        public void setShippingType(String shippingType) {
            this.shippingType = shippingType;
        }

        public String getShippingAmount() {
            return shippingAmount;
        }

        public void setShippingAmount(String shippingAmount) {
            this.shippingAmount = shippingAmount;
        }

        public String getGrandTotal() {
            return grandTotal;
        }

        public void setGrandTotal(String grandTotal) {
            this.grandTotal = grandTotal;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

        public String getTrackingStatus() {
            return trackingStatus;
        }

        public void setTrackingStatus(String trackingStatus) {
            this.trackingStatus = trackingStatus;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getOrderDeliveryStatus() {
            return orderDeliveryStatus;
        }

        public void setOrderDeliveryStatus(String orderDeliveryStatus) {
            this.orderDeliveryStatus = orderDeliveryStatus;
        }

        public String getLog() {
            return log;
        }

        public void setLog(String log) {
            this.log = log;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getStreetName() {
            return streetName;
        }

        public void setStreetName(String streetName) {
            this.streetName = streetName;
        }

        public String getBuildingName() {
            return buildingName;
        }

        public void setBuildingName(String buildingName) {
            this.buildingName = buildingName;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public String getCountryName() {
            return countryName;
        }

        public void setCountryName(String countryName) {
            this.countryName = countryName;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getAddresMode() {
            return addresMode;
        }

        public void setAddresMode(String addresMode) {
            this.addresMode = addresMode;
        }

        public String getStateName() {
            return stateName;
        }

        public void setStateName(String stateName) {
            this.stateName = stateName;
        }

        public String getDistrictName() {
            return districtName;
        }

        public void setDistrictName(String districtName) {
            this.districtName = districtName;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhonenumber() {
            return phonenumber;
        }

        public void setPhonenumber(String phonenumber) {
            this.phonenumber = phonenumber;
        }

    }
}
