package com.agriculture.kisanbandiconnector.Models.Resbonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResbonse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("connector_id")
    @Expose
    private String connectorId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("connectorstatus")
    @Expose
    private String connectorstatus;
    @SerializedName("statuscode")
    @Expose
    private String statuscode;

    @SerializedName("hub_status")
    @Expose
    private String hub_status;

    /**
     * No args constructor for use in serialization
     *
     */
    public LoginResbonse() {
    }

    /**
     *
     * @param statuscode
     * @param connectorId
     * @param userId
     * @param status
     * @param connectorstatus
     */
    public LoginResbonse(String status, String connectorId, String userId, String connectorstatus, String statuscode) {
        super();
        this.status = status;
        this.connectorId = connectorId;
        this.userId = userId;
        this.connectorstatus = connectorstatus;
        this.statuscode = statuscode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getConnectorId() {
        return connectorId;
    }

    public void setConnectorId(String connectorId) {
        this.connectorId = connectorId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getConnectorstatus() {
        return connectorstatus;
    }

    public void setConnectorstatus(String connectorstatus) {
        this.connectorstatus = connectorstatus;
    }

    public String getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(String statuscode) {
        this.statuscode = statuscode;
    }

    public String getHub_status() {
        return hub_status;
    }

    public void setHub_status(String hub_status) {
        this.hub_status = hub_status;
    }
}
